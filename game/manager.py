
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from util import some, random_string
from random import random, randint, choice

from data.db import acft_cat
from game.env import env


# ---------- Constants ----------

teacher_callsign = 'Teacher'
student_callsign = 'Student'

aircraft_registration_prefixes = ['F', 'G', 'OO', 'SU', 'XA', 'D', 'JA', 'XV', 'VT', 'TU', 'VH']
commercial_prob = {'heavy': 1, 'jets': 1, 'turboprops': .2, 'props': 0, 'helos': 0}

# -------------------------------


class GameType:
	enum = DUMMY, SOLO, FLIGHTGEAR_MP, TEACHER, STUDENT = range(5)


class GameManager:
	'''
	Subclasses should redefine "game_type" attr, and the following silent methods:
	- start
	- stop
	- pauseGame
	- resumeGame
	- isRunning
	- myCallsign
	- getAircraft
	- postTextChat (raise ValueError if message should not appear as posted)
	- instructAircraft (instructs the selected callsign, or a specified callsign---direct aircraft control if TEACHER mode)
	- handOverStrip (returns None if OK, or an error msg if handover must be aborted and strip not sent)
	- getWeather (weather for given argument station name)
	- onlineFplEditAvailable (returns True iff a system exists to file/amend online FPLs)
	- changeFplStatus (if only None FPL statuses can exist in the game: this will never be called)
	'''
	
	def __init__(self, gui):
		self.gui = gui
		self.game_type = GameType.DUMMY

	def generateCallsign(self, acft_type, available_airlines):
		cs = None
		while cs == None or cs in env.ATCs.knownATCs() + [acft.identifier for acft in self.getAircraft()]:
			airline = None
			cat = acft_cat(acft_type)
			if cat != None and random() < commercial_prob.get(cat, 0):
				if len(available_airlines) > 0:
					airline = choice(available_airlines)
			if airline == None:
				prefix = choice(aircraft_registration_prefixes)
				cs = '%s-%s' % (prefix, random_string(5 - len(prefix)))
			else:
				cs = '%s%04d' % (airline, randint(1, 9999))
		return cs
	
	
	## Methods to override below ##
	
	def start(self):
		pass
	
	def stop(self):
		pass
	
	def pauseGame(self):
		pass
	
	def resumeGame(self):
		pass
	
	def isRunning(self):
		return False
	
	def myCallsign(self):
		return 'Dummy'
	
	def getAircraft(self):
		return []
	
	def getWeather(self, station):
		return None
	
	def postTextChat(self, msg):
		pass
	
	def instructAircraft(self, instr, callsign=None):
		pass
	
	def handOverStrip(self, strip, atc):
		return None
	
	def onlineFplEditAvailable(self):
		return False
	
	def changeFplStatus(self):
		pass

