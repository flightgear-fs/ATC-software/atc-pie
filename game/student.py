
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtNetwork import QTcpSocket
from PyQt5.QtWidgets import QMessageBox

from util import pop_all, some
from settings import settings
from GUI.misc import signals, selection

from data.chatMsg import ChatMessage
from data.FPL import FPL
from data.UTC import now
from data.weather import Weather
from data.strip import Strip, received_from_detail

from game.env import env
from game.manager import GameManager, GameType, student_callsign, teacher_callsign
from game.teacher import TeachingMsg, TeachingSessionWire, handover_details

from ext.fgfs import send_packet_to_views
from ext.fgms import update_FgmsAircraft_list


# ---------- Constants ----------

game_tick_interval = 100 # ms

# -------------------------------



class StudentSessionGameManager(GameManager):
	def __init__(self, gui):
		GameManager.__init__(self, gui)
		self.game_type = GameType.STUDENT
		self.gui = gui
		self.running = False
		self.teacher_socket = QTcpSocket() # this socket connects to the teacher
		self.teacher_paused_at = None # pause time if game is paused; None otherwise
		self.traffic = [] # FgmsAircraft list
		self.known_METAR = None
	
	def start(self):
		self.teacher_socket.connectToHost(settings.teaching_service_host, settings.teaching_service_port)
		if self.teacher_socket.waitForConnected():
			env.ATCs.updateATC(teacher_callsign, True, None, 'Your teacher', None)
			self.teacher = TeachingSessionWire(self.teacher_socket)
			self.teacher_socket.disconnected.connect(self.disconnected)
			self.teacher.messageArrived.connect(self.receiveMsgFromTeacher)
			print('Connected to teacher.')
			self.traffic.clear()
			self.running = True
			signals.gameStarted.emit()
		else:
			QMessageBox.critical(self.gui, 'Connection error', 'Connection to teacher has failed. Check service host and port.')
	
	def stop(self):
		if self.isRunning():
			self.running = False
			self.teacher.messageArrived.disconnect(self.receiveMsgFromTeacher)
			self.teacher_socket.disconnected.disconnect(self.disconnected)
			self.teacher_socket.disconnectFromHost()
			print('Disconnected.')
			self.traffic.clear()
			signals.gameStopped.emit()
	
	def disconnected(self):
		QMessageBox.critical(self.gui, 'Disconnected', 'Connection error or rejected by teacher.')
		self.stop()
	
	def isRunning(self):
		return self.running
	
	def myCallsign(self):
		return student_callsign
	
	def getAircraft(self):
		return self.traffic[:]
	
	def getWeather(self, station):
		if station == settings.primary_METAR_station and self.known_METAR != None:
			return Weather(self.known_METAR)
		else:
			return None
	
	def postTextChat(self, msg):
		self.teacher.sendMessage(TeachingMsg(TeachingMsg.TEXT_CHAT, data=msg.txtMsg()))
	
	def instructAircraft(self, instr, callsign=None):
		dest = some(callsign, some(selection.selectedCallsign(), ''))
		msg = instr.suggestTextChatInstruction(selection.acft if dest == selection.selectedCallsign() else None)
		signals.chatInstructionSuggestion.emit(dest, msg, False)
		selection.writeStripAssignment(instr)
	
	
	## COMMUNICATION WITH TEACHER
	
	def receiveMsgFromTeacher(self, msg):
		if msg.type == TeachingMsg.ACFT_KILLED:
			for acft in pop_all(self.traffic, lambda a: a.identifier == msg.strData()):
				signals.aircraftKilled.emit(acft)
		elif msg.type == TeachingMsg.TRAFFIC: # traffic update; contains FGMS packet
			fgms_packet = msg.binData()
			update_FgmsAircraft_list(self.traffic, fgms_packet)
			send_packet_to_views(fgms_packet)
			self.teacher.sendMessage(TeachingMsg(TeachingMsg.TRAFFIC))
		elif msg.type == TeachingMsg.SIM_PAUSED:
			self.teacher_paused_at = now()
			signals.gamePaused.emit()
		elif msg.type == TeachingMsg.SIM_RESUMED:
			pause_delay = now() - self.teacher_paused_at
			for acft in self.traffic:
				acft.moveHistoryTimesForward(pause_delay)
			self.teacher_paused_at = None
			signals.gameResumed.emit()
		elif msg.type == TeachingMsg.TEXT_CHAT:
			signals.incomingTextChat.emit(ChatMessage(teacher_callsign, msg.strData(), dest=student_callsign, private=True))
		elif msg.type == TeachingMsg.STRIP_EXCHANGE:
			fromATC, details = msg.decodeDetails()
			strip = Strip()
			for d, v in details.items():
				strip.writeDetail(d, v)
			strip.writeDetail(received_from_detail, fromATC)
			signals.receiveStrip.emit(strip)
		elif msg.type == TeachingMsg.SX_LIST:
			to_remove = set(env.ATCs.knownATCs())
			to_remove.discard(teacher_callsign)
			for line in msg.strData().split('\n'):
				if line != '': # last line is empty
					lst = line.rsplit('\t', maxsplit=1)
					atc = lst[0]
					frq = lst[-1] if len(lst) > 1 else None
					env.ATCs.updateATC(atc, True, None, None, frq)
					to_remove.discard(atc)
			for atc in to_remove:
				env.ATCs.removeATC(atc)
		elif msg.type == TeachingMsg.WEATHER:
			metar = msg.strData()
			station = metar.split(' ', maxsplit=1)[0]
			if station == settings.primary_METAR_station and metar != self.known_METAR:
				self.known_METAR = metar
				signals.newWeather.emit(station, Weather(metar))
		else:
			print('Unhandled message type from teacher: %s' % msg.type)
	
	
	## STRIP EXCHANGE
	
	def handOverStrip(self, strip, sendto):
		details = { d:strip.lookup(d) for d in handover_details if strip.lookup(d) != None }
		msg = TeachingMsg.detailExchangeMsg(TeachingMsg.STRIP_EXCHANGE, sendto, details)
		self.teacher.sendMessage(msg)
		if sendto != teacher_callsign:
			self.instructAircraft(env.ATCs.handoverInstructionTo(sendto))
	


