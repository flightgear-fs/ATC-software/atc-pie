
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta
from socket import socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_REUSEADDR
from PyQt5.QtCore import QMutex, QThread
from PyQt5.QtWidgets import QMessageBox

from util import pop_all, some
from settings import settings
from GUI.misc import signals, selection, Ticker
from game.env import env
from game.manager import GameManager, GameType
from data.weather import Weather
from data.FPL import FPL
from data.UTC import now
from data.strip import HandoverError

from ext.fgms import FGMShandshaker, FGMSlistener, known_ATC_models, FgmsAircraft, update_FgmsAircraft_list
from ext.wwsx import StripExchanger
from ext.fgfs import send_packet_to_views
from ext.noaa import get_METAR
from ext.lenny64 import download_FPLs, Lenny64Error, set_FPL_status


# ---------- Constants ----------

minimum_chat_message_send_count = 8
FGMS_handshake_interval = 500 # ms

# -------------------------------



# =============================================== #

#                 ONLINE CHECKERS                 #

# =============================================== #


class WeatherUpdater(QThread):
	def __init__(self, parent):
		QThread.__init__(self, parent)
		self.known_info = {} # station -> Weather
	
	def lookupOrUpdate(self, station):
		try:
			return self.known_info[station]
		except KeyError:
			self.start() # This trick works only if station is the primary or in the additional ones
		
	def run(self):
		wanted = [settings.primary_METAR_station] + settings.additional_METAR_stations
		for key in self.known_info:
			if key not in wanted:
				del self.known_info[key]
		for station in wanted:
			new_metar = get_METAR(station)
			if new_metar != None:
				prev = self.known_info.get(station, None)
				self.known_info[station] = w = Weather(new_metar)
				if prev == None or w.isNewerThan(prev):
					signals.newWeather.emit(station, w)




class FPLchecker(QThread):
	def __init__(self, parent):
		QThread.__init__(self, parent)
		
	def run(self):
		try:
			day = now().date()
			online_IDs = set()
			for i in range(-4, 5):
				for online_fpl in download_FPLs(day + i * timedelta(days=1)):
					online_IDs.add(online_fpl.online_id)
					try:
						got_FPL = env.FPLs.findFPL(lambda fpl: fpl.online_id == online_fpl.online_id)[0]
					except StopIteration:
						env.FPLs.addFPL(online_fpl)
						signals.newFPL.emit(online_fpl)
					else:
						got_FPL.setStatus(online_fpl.status())
						got_FPL.setOnlineComments(online_fpl.onlineComments())
						for d in FPL.details:
							if d not in got_FPL.modified_details and got_FPL[d] != online_fpl[d]:
								got_FPL.details[d] = online_fpl[d]
			env.FPLs.clearFPLs(pred=(lambda fpl: fpl.existsOnline() and fpl.online_id not in online_IDs))
		except Lenny64Error as err:
			print('Could not check for online flight plans. %s' % err)






# ============================================== #

#                  GAME MANAGER                  #

# ============================================== #


class FlightGearMPgameManager(GameManager):
	def __init__(self, gui, callsign):
		GameManager.__init__(self, gui)
		self.game_type = GameType.FLIGHTGEAR_MP
		self.MP_callsign = callsign
		self.socket = None # None here when game NOT running
		self.FGMS_handshake_ticker = Ticker(self.FGMShandshake, parent=self.gui)
		self.strip_exchanger = StripExchanger(self.gui, self.update_SX_ATCs)
		self.connection_list_mutex = QMutex() # Critical concurrency: handshaker clearing zombies vs. listener adding traffic
		self.weather_updater = WeatherUpdater(self.gui)
		self.FPL_checker = FPLchecker(self.gui)
		self.FPL_checker.finished.connect(env.FPLs.refreshViews)
		self.FPL_ticker = Ticker(self.FPL_checker.start, parent=self.gui)
		self.METAR_ticker = Ticker(self.weather_updater.start, parent=self.gui)
		self.FGMS_connections = [] # FgmsAircraft list of "connected" FGMS callsigns
	
	def start(self):
		try:
			self.socket = socket(AF_INET, SOCK_DGRAM)
			self.socket.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
			self.socket.bind(('', settings.FGMS_client_port))
			self.server_address = settings.FGMS_server_name, settings.FGMS_server_port
		except OSError as error:
			self.socket = None
			print('Connection error: %s' % error)
		else:
			self.chat_msg_send_count = 0
			self.chat_msg_queue = [] # pop first, enqueue at end
			self.FGMS_handshaker = FGMShandshaker(self.gui, self.socket, self.server_address, self.myCallsign())
			self.FGMS_handshake_ticker.start(FGMS_handshake_interval)
			self.FGMS_listener = FGMSlistener(self.gui, self.socket, self.receiveFgmsData)
			self.FGMS_listener.start()
			if settings.FGSX_enabled:
				self.strip_exchanger.start()
			self.restartOnlineLookupTickers()
			signals.systemSettingsChanged.connect(self.restartOnlineLookupTickers)
			signals.FPLupdateRequest.connect(self.FPL_checker.start)
			signals.weatherUpdateRequest.connect(self.weather_updater.start)
			signals.gameStarted.emit()
			print('Connected to %s port %d.' % (settings.FGMS_server_name, settings.FGMS_server_port))
	
	def stop(self):
		if self.isRunning():
			signals.systemSettingsChanged.disconnect(self.restartOnlineLookupTickers)
			signals.FPLupdateRequest.disconnect(self.FPL_checker.start)
			signals.weatherUpdateRequest.disconnect(self.weather_updater.start)
			# stop tickers and threads in a clean way
			self.FGMS_listener.stop() # looping thread
			self.FGMS_handshake_ticker.stop() # ticker triggering a one shot thread
			self.FPL_ticker.stop() # ticker triggering a one shot thread
			self.METAR_ticker.stop() # ticker triggering a one shot thread
			self.strip_exchanger.stopAndWait() # contains a thread and includes a clean wait
			for thread in self.FGMS_listener, self.FGMS_handshaker, self.FPL_checker, self.weather_updater:
				thread.wait()
			del self.FGMS_handshaker
			del self.FGMS_listener
			# finish up
			self.socket = None
			self.FGMS_connections.clear()
			signals.gameStopped.emit()
	
	def restartOnlineLookupTickers(self):
		self.FPL_ticker.start_stopOnZero(settings.FPL_update_interval)
		self.METAR_ticker.start_stopOnZero(settings.METAR_update_interval)
	
	def isRunning(self):
		return self.socket != None
	
	def myCallsign(self):
		return self.MP_callsign
	
	def getAircraft(self):
		self.connection_list_mutex.lock()
		result = [acft for acft in self.FGMS_connections if acft.aircraft_type not in known_ATC_models]
		self.connection_list_mutex.unlock()
		return result
	
	def getWeather(self, station):
		# Returns the weather straight away if known, otherwise answers None but triggers the updater
		# which will signal the new weather if station is the primary or a registered additional station
		return self.weather_updater.lookupOrUpdate(station)
	
	def postTextChat(self, msg):
		txt = msg.txtMsg()
		if txt == (self.FGMS_handshaker.currentChatMessage() if self.chat_msg_queue == [] else self.chat_msg_queue[-1]):
			raise ValueError('FGMS ignores a text message if it is identical to the previous.')
		else:
			self.chat_msg_queue.append(txt)
	
	def instructAircraft(self, instr, callsign=None):
		dest = some(callsign, some(selection.selectedCallsign(), ''))
		msg = instr.suggestTextChatInstruction(selection.acft if dest == selection.selectedCallsign() else None)
		signals.chatInstructionSuggestion.emit(dest, msg, False)
		selection.writeStripAssignment(instr)
	
	
	## FPL STUFF
	
	def onlineFplEditAvailable(self):
		return self.isRunning() and settings.lenny64_account_email != ''
	
	def changeFplStatus(self, fpl, new_status):
		try:
			set_FPL_status(fpl, new_status)
		except Lenny64Error as err:
			msg = 'Error in setting FPL online status (ID = %s): %s' % (fpl.online_id, err)
			if err.srvResponse() != None:
				msg += '\nServer response was: %s' % err.srvResponse()
			print(msg)
		else:
			env.FPLs.refreshViews()
	
	
	## ATCs and STRIP EXCHANGE
	
	def handOverStrip(self, strip, atc_callsign):
		if self.strip_exchanger.isRunning():
			self.strip_exchanger.handOver(strip, atc_callsign) # raises HandoverError if not allowed/possible
			self.instructAircraft(env.ATCs.handoverInstructionTo(atc_callsign))
		else:
			raise HandoverError('Strip exchange feature turned off')
	
	
	## FGMS
	
	def FGMShandshake(self):
		if self.chat_msg_send_count >= minimum_chat_message_send_count and self.chat_msg_queue != []:
			self.chat_msg_send_count = 0
			self.FGMS_handshaker.setChatMessage(self.chat_msg_queue.pop(0))
		self.FGMS_handshaker.start()
		self.chat_msg_send_count += 1
		self.connection_list_mutex.lock()
		pop_all(self.FGMS_connections, FgmsAircraft.isZombie)
		sx = env.ATCs.knownATCs(sx=True)
		lost = set(env.ATCs.knownATCs(sx=False))
		for c in self.FGMS_connections:
			if c.aircraft_type in known_ATC_models:
				lost.discard(c.identifier)
				if c.identifier not in sx:
					env.ATCs.updateATC(c.identifier, False, c.coords(realTime=True), None, c.radio_comm_frequency)
		for atc in lost:
			env.ATCs.removeATC(atc)
		self.connection_list_mutex.unlock()
		env.ATCs.refreshViews()
	
	def receiveFgmsData(self, udp_packet):
		self.connection_list_mutex.lock()
		update_FgmsAircraft_list(self.FGMS_connections, udp_packet)
		self.connection_list_mutex.unlock()
		send_packet_to_views(udp_packet)
	
	def update_SX_ATCs(self, update_dict):
		self.connection_list_mutex.lock()
		lost_SX = set(env.ATCs.knownATCs(sx=True))
		for atc, (pos, name, frq) in update_dict.items():
			env.ATCs.updateATC(atc, True, pos, name, frq)
			lost_SX.discard(atc)
		for atc in lost_SX:
			env.ATCs.updateATC(atc, False, None, None, None)
		self.connection_list_mutex.unlock()

