
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta
from random import random, choice

from settings import settings
from util import pop_all, some

from data.coords import EarthCoords
from data.UTC import now
from data.nav import Navpoint, NavpointError
from data.chatMsg import ChatMessage
from data.params import Speed, Heading, StdPressureAlt, distance_flown, wind_effect
from data.aircraft import Aircraft
from data.db import take_off_speed, touch_down_speed, stall_speed, maximum_speed, cruise_speed
from data.instruction import Instruction

from game.env import env
from game.manager import GameType
from game.aiStatus import Status
from GUI.misc import signals
from ext.fgfs import FGFS_model, FGFS_liveries
from ext.fgms import make_fgms_packet, position_data, position_message_type_code, FGMS_prop_code_by_name, \
		FGMS_prop_XPDR_code, FGMS_prop_XPDR_alt, FGMS_prop_XPDR_ident, FGMS_prop_XPDR_mode


# ---------- Constants ----------

pilot_turn_speed = 3 # degrees per second
pilot_vert_speed = 1800 # ft / min
pilot_accel = 3 # kt / s
pilot_hdg_precision = 2 # degrees
pilot_alt_precision = 20 # ft
pilot_spd_precision = 5 # kt
pilot_nav_precision = 1 # NM
pilot_taxi_precision = .002 # NM
pilot_sight_range = 7.5 # NM
pilot_sight_ceiling = StdPressureAlt.fromFL(100)

fast_turn_factor = 2.75
fast_climb_descend_factor = 1.75
fast_accel_decel_factor = 1.75

touch_down_distance_tolerance = .03 # NM
touch_down_height_tolerance = 50 # ft
touch_down_heading_tolerance = 5 # degrees
touch_down_speed_tolerance = 5 # kt
min_clearToLand_height = 50 # ft

taxi_speed = Speed(15)
ldg_roll_speed = Speed(25)
MISAP_climb_reading = '5000 ft'

default_turn_off_angle = -60 # degrees
turn_off_choice_prob = .5
approach_angle = 30 # degrees
navpoint_intercept_angle = 7 # degrees on each side
init_hldg_turn = 120 # degrees
hldg_leg_fly_time = timedelta(minutes=1)
ready_max_dist_to_threshold = .1 # NM
park_max_dist_to_gate_node = .1 # NM

RPM_low = 100
RPM_high = 1000
right_turn_roll = 12 # degrees roll
pitch_factor = 30 / 3000 # degrees / (ft/min)
final_pitch = 2
gear_compression_low = 0
gear_compression_high = .5

FGMS_prop_XPDR_mode_encoding = { c:i for i, c in enumerate('0GACS') }
FGMS_prop_text_chat = FGMS_prop_code_by_name('sim/multiplay/chat')
FGMS_prop_livery_file = FGMS_prop_code_by_name('sim/model/livery/file')
FGMS_props_gear_position = [FGMS_prop_code_by_name('gear/gear[%d]/position-norm' % i) for i in range(4)]
FGMS_props_gear_compression = [FGMS_prop_code_by_name('gear/gear[%d]/compression-norm' % i) for i in range(4)]
FGMS_props_engine_RPM = [FGMS_prop_code_by_name('engines/engine[%d]/rpm' % i) for i in range(4)]

# -------------------------------



def GS_alt(thr_elev, fpa, dist):
	return StdPressureAlt.fromAMSL(thr_elev + 60.761 * fpa * dist, qnh=env.QNH())





class AI_Aircraft(Aircraft):
	'''
	This class represents an solo mode AI contact
	'''
	
	def __init__(self, callsign, init_params, acft_type, goal):
		'''
		goal parameter is:
		 - bool if and only if landing at base airport (True=ILS; False=visual)
		 - str if and only if requesting a parking position
		 - (Navpoint, cruise alt/lvl) if must be brought to a certain location/altitude (either can be None if don't matter)
		 - Airfield (destination) if it is transiting through airspace
		 - None if none of the above
		'''
		Aircraft.__init__(self, callsign, acft_type, init_params.position, init_params.geometricAltitude())
		if cruise_speed(acft_type) == None:
			raise ValueError('Aborting AI_Aircraft construction: unknown cruise speed for %s' % acft_type)
		try:
			airline_code = callsign[:next(i for i in range(len(callsign)) if not callsign[i].isalpha())]
			self.livery = FGFS_liveries[acft_type][airline_code]
		except (StopIteration, KeyError):
			self.livery = None
		self.params = init_params
		self.goal = goal
		self.frozen = False
		self.released = False
		self.instructions = []
		self.last_turn_direction = 0
	
	
	## SNAPSHOTS
	
	def fromStatusSnapshot(snapshot): # STATIC constructor
		cs, t, params, goal, spawned, frozen, instr = snapshot
		acft = AI_Aircraft(cs, params.dup(), t, goal)
		acft.instructions = [i.dup() for i in instr]
		acft._spawned = spawned # STYLE! improve inelegant placement? (makes sense only in teacher game type)
		acft.frozen = frozen
		return acft
	
	def statusSnapshot(self):
		return self.identifier, self.aircraft_type, self.params.dup(), self.goal, \
				self._spawned, self.frozen, [i.dup() for i in self.instructions]
	
	
	## GENERAL ACCESS METHODS
	
	def statusType(self):
		return self.params.status.type
	
	def isInboundGoal(self):
		return isinstance(self.goal, bool) or isinstance(self.goal, str)
	
	def isOutboundGoal(self):
		return isinstance(self.goal, tuple)
	
	def wantsToPark(self):
		return isinstance(self.goal, str)
	
	def canPark(self):
		if self.wantsToPark() and env.airport_data != None: # wants a gate/pkpos
			pkg_pos = env.airport_data.ground_net.parkingPosition(self.goal)
			return self.params.position.distanceTo(pkg_pos) <= park_max_dist_to_gate_node
		else:
			return False
	
	def isReadyForDeparture(self):
		return self.statusType() in [Status.READY, Status.LINED_UP]
	
	def isGroundStatus(self):
		return self.statusType() in [Status.TAXIING, Status.READY, Status.LINED_UP, Status.RWY_TKOF, Status.RWY_LDG]
	
	def instrOfType(self, t):
		'''
		returns the instruction of given type, or None
		'''
		return next((i for i in self.instructions if i.type == t), None)
	
	def groundPointInSight(self, point):
		return self.params.altitude.diff(pilot_sight_ceiling) <= 0 \
			and self.params.position.distanceTo(point) <= pilot_sight_range
	
	def maxTurn(self, timedelta):
		return pilot_turn_speed * timedelta.total_seconds()
	
	def maxClimb(self, timedelta):
		return pilot_vert_speed * timedelta.total_seconds() / 60
	
	def maxSpdIncr(self, timedelta):
		return pilot_accel * timedelta.total_seconds()
	
	
	# # # # # # # # # # # # #
	#        TICKING        #
	# # # # # # # # # # # # #
	
	def tickOnce(self):
		if not self.frozen:
			self.tick_interval = now() - self.lastLiveUpdateTime()
			for instr in self.instructions:
				self.followInstruction(instr)
			if not self.isGroundStatus(): # control horiz. displacement
				if settings.solo_enforce_speed_restriction and self.params.altitude.diff(StdPressureAlt.fromFL(100)) <= 0:
					self.accelDecelTowards(Speed(250), accelOK=False)
				elif self.params.altitude.diff(StdPressureAlt.fromFL(100)) > 0 and self.instrOfType(Instruction.VECTOR_SPD) == None:
					self.accelDecelTowards(cruise_speed(self.aircraft_type))
				tas = self.params.ias.ias2tas(self.params.altitude)
				#print('IAS %s at %d ft --> TAS %s' % (self.params.ias, self.params.altitude.ft1013(), tas)) # DEBUG
				w = env.primaryWeather()
				wind_info = None if w == None else w.mainWind()
				if self.statusType() == Status.LANDING or wind_info == None or wind_info[0] == None:
					course = self.params.heading
					ground_speed = tas
				else: # assumed here: no gusts (OK) # FIXME unit here is assumed kt
					course, ground_speed = wind_effect(self.params.heading, tas, wind_info[0], Speed(wind_info[1]))
				self.params.position = self.params.position.moved(course, distance_flown(self.tick_interval, ground_speed))
			pop_all(self.instructions, self.instructionDone)
		self.updateLiveStatus(self.params.position, self.params.geometricAltitude(), *self.params.XPDRstatus())
	
	
	# # # # # # # # # # # # #
	#     INSTRUCTIONS      #
	# # # # # # # # # # # # #
	
	## This is where the conditions are given for getting rid of instructions in ACFT instr. lists
	def instructionDone(self, instr):
		if instr.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_ALT, Instruction.VECTOR_SPD, Instruction.FOLLOW_ROUTE]:
			return False
		elif instr.type == Instruction.VECTOR_DCT: # navpoint already resolved
			return self.params.position.distanceTo(instr.arg.coordinates) <= pilot_nav_precision
		elif instr.type == Instruction.HOLD:
			return False
		elif instr.type == Instruction.SQUAWK:
			return self.params.XPDR_code == instr.arg
		elif instr.type == Instruction.CANCEL_APP:
			return self.statusType() == Status.AIRBORNE and self.instrOfType(Instruction.CLEARED_TO_LAND) == None \
				and self.instrOfType(Instruction.CLEARED_APP) == None
		elif instr.type == Instruction.INTERCEPT_NAV:
			return False
		elif instr.type == Instruction.INTERCEPT_LOC:
			return self.instrOfType(Instruction.CLEARED_APP) != None
		elif instr.type == Instruction.EXPECT_RWY:
			return self.statusType() in [Status.RWY_LDG, Status.READY]
		elif instr.type == Instruction.TAXI:
			return instr.arg[0] == [] and instr.arg[1] == None
		elif instr.type == Instruction.HOLD_POSITION:
			return self.instrOfType(Instruction.TAXI) == None
		elif instr.type == Instruction.CLEARED_APP:
			return self.statusType() == Status.LANDING
		elif instr.type == Instruction.CLEARED_TO_LAND:
			return self.statusType() == Status.TAXIING
		elif instr.type == Instruction.LINE_UP:
			return self.statusType() == Status.LINED_UP
		elif instr.type == Instruction.CLEARED_TKOF:
			return self.statusType() == Status.AIRBORNE
		elif instr.type == Instruction.HAND_OVER:
			return self.released
		else:
			assert False, 'instructionDone: unknown instruction %s' % instr
	
	## This is where instruction is given, possibly rejected by ACFT if makes no sense
	def instruct(self, instr):
		'''
		given instruction might replace some in place, but ALWAYS ends up in the list, except for SAY_* instructions
		'''
		self.ckResolveNavpointInstr(instr)
		
		if instr.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_DCT, Instruction.FOLLOW_ROUTE]:
			self.ckInstr(self.instrOfType(Instruction.CLEARED_APP) == None, 'Already cleared for approach. Should I cancel clearance?')
			self.ckInstr(self.statusType() in [Status.AIRBORNE, Status.HLDG], 'Sorry, not a time for vectors.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_DCT, \
					Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC, Instruction.FOLLOW_ROUTE, Instruction.HOLD])
			if self.statusType() == Status.HLDG:
				self.params.status = Status(Status.AIRBORNE)
			
		elif instr.type == Instruction.VECTOR_ALT:
			if self.isGroundStatus(): # instr = initial climb
				self.ckInstr(not self.isInboundGoal(), 'Not outbound.')
			else: # instr = climb/descend
				self.ckInstr(self.instrOfType(Instruction.CLEARED_APP) == None, 'Already cleared for approach. Should I cancel clearance?')
			pop_all(self.instructions, lambda i: i.type == Instruction.VECTOR_ALT)
			
		elif instr.type == Instruction.VECTOR_SPD:
			self.ckInstr(self.statusType() == Status.AIRBORNE, 'Sorry, not a time for speed changes.')
			self.ckInstr(instr.arg.diff(stall_speed(self.aircraft_type)) >= 0, 'Speed too low.')
			self.ckInstr(instr.arg.diff(maximum_speed(self.aircraft_type)) <= 0, 'Cannot reach such speed.')
			pop_all(self.instructions, lambda i: i.type == Instruction.VECTOR_SPD)
			
		elif instr.type == Instruction.INTERCEPT_NAV:
			self.ckInstr(self.statusType() == Status.AIRBORNE and self.instrOfType(Instruction.CLEARED_APP) == None, \
					'Sorry, not a time for vectors.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC])
			
		elif instr.type == Instruction.INTERCEPT_LOC:
			self.ckInstr(not self.isGroundStatus(), 'Not airborne!')
			self.ckInstr(self.statusType() != Status.LANDING, 'Already landing.')
			self.ckInstr(self.statusType() != Status.HLDG, 'Still on hold.')
			self.ckVoiceInstrAndEnsureRwy(instr)
			self.ckInstr(self.instrOfType(Instruction.CLEARED_APP) == None, 'Already cleared for approach. Should I cancel clearance?')
			self.ckInstr(self.goal != False, 'Requesting visual approach.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC])
			
		elif instr.type == Instruction.HOLD:
			self.ckInstr(self.statusType() in [Status.AIRBORNE, Status.HLDG], 'Sorry, cannot hold.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_DCT, \
					Instruction.INTERCEPT_NAV, Instruction.INTERCEPT_LOC, Instruction.FOLLOW_ROUTE, Instruction.HOLD])
			
		elif instr.type == Instruction.SQUAWK:
			pop_all(self.instructions, lambda i: i.type == Instruction.SQUAWK)
			
		elif instr.type == Instruction.CANCEL_APP:
			self.ckInstr(self.statusType() == Status.LANDING or self.instrOfType(Instruction.CLEARED_APP) != None, 'Not on approach.')
			pop_all(self.instructions, lambda i: i.type == Instruction.CANCEL_APP)
			
		elif instr.type == Instruction.LINE_UP:
			self.ckInstr(self.statusType() == Status.READY, 'Not ready.')
			if instr.isVoiceRecognised():
				str_voice_rwys = instr.voice_data['rwy']
				if str_voice_rwys != '':
					self.ckInstr(str_voice_rwys == self.params.status.arg, 'Ready for departure from %s. Wrong runway?' % self.params.status.arg)
			pop_all(self.instructions, lambda i: i.type == Instruction.LINE_UP)
			
		elif instr.type == Instruction.CLEARED_TKOF:
			self.ckInstr(self.statusType() in [Status.READY, Status.LINED_UP], 'Not waiting for departure.')
			if instr.isVoiceRecognised():
				str_voice_rwys = instr.voice_data['rwy']
				if str_voice_rwys != '':
					self.ckInstr(str_voice_rwys == self.params.status.arg, 'Ready for departure from %s. Wrong runway?' % self.params.status.arg)
			if self.statusType() == Status.READY:
				self.instructions.append(Instruction(Instruction.LINE_UP, arg=self.params.status.arg))
			pop_all(self.instructions, lambda i: i.type == Instruction.CLEARED_TKOF)
			
		elif instr.type == Instruction.EXPECT_RWY:
			self.ckInstr(env.airport_data != None and instr.arg in env.airport_data.runwayNames(), 'Which runway??')
			if self.isGroundStatus():
				self.ckInstr(self.statusType() != Status.RWY_TKOF, 'Already taking off.')
				self.ckInstr(not self.isInboundGoal(), 'Not requesting departure.')
			else:
				self.ckInstr(not self.isOutboundGoal(), 'Outbound.')
				self.ckInstr(self.statusType() != Status.LANDING, 'Already landing.')
				self.ckInstr(self.instrOfType(Instruction.CLEARED_APP) == None, 'Already cleared for approach. Should I cancel clearance?')
				if settings.game_manager.game_type == GameType.TEACHER:
					self.goal = True # ILS approach to base airport (for easier intercept)
				if instr.isVoiceRecognised():
					app = instr.voice_data['app']
					self.ckInstr(app == None or app == self.goal, 'Requesting %s approach.' % ('ILS' if self.goal else 'visual'))
				if self.goal: # requires an ILS-capable runway
					self.ckInstr(env.airport_data.runway(instr.arg).param_ILS, 'RWY %s has no ILS.' % instr.arg) # RWY name exists
			pop_all(self.instructions, lambda i: i.type in [Instruction.EXPECT_RWY, Instruction.INTERCEPT_LOC])
			self.params.runway_reported_in_sight = False
		
		elif instr.type == Instruction.TAXI:
			self.ckInstr(self.isGroundStatus(), 'Currently airborne!')
			self.ckInstr(self.statusType() != Status.RWY_TKOF, 'Already taking off.')
			pop_all(self.instructions, lambda i: i.type in [Instruction.TAXI, Instruction.HOLD_POSITION])
		
		elif instr.type == Instruction.HOLD_POSITION:
			self.ckInstr(self.instrOfType(Instruction.TAXI) != None, 'Not taxiing.')
			pop_all(self.instructions, lambda i: i.type == Instruction.HOLD_POSITION)
			
		elif instr.type == Instruction.CLEARED_APP:
			self.ckInstr(not self.isGroundStatus(), 'Not airborne!')
			self.ckInstr(self.statusType() != Status.LANDING, 'Already landing.')
			self.ckInstr(self.statusType() != Status.HLDG, 'Still on hold.')
			self.ckVoiceInstrAndEnsureRwy(instr)
			if instr.isVoiceRecognised():
				app = instr.voice_data['app'] # in this case normally never None
				self.ckInstr(app == None or app == self.goal, 'Requesting %s approach.' % ('ILS' if self.goal else 'visual'))
			self.ckInstr(self.goal == True or self.params.runway_reported_in_sight, 'Runway not in sight yet.')
			pop_all(self.instructions, lambda i: i.type == Instruction.CLEARED_APP)
			
		elif instr.type == Instruction.CLEARED_TO_LAND:
			self.ckInstr((settings.solo_role_TWR or settings.game_manager.game_type == GameType.TEACHER), 'Only TWR can issue this instruction.')
			self.ckInstr(self.statusType() == Status.LANDING, 'Not correctly on final.')
			got_expect_runway = self.instrOfType(Instruction.EXPECT_RWY)
			self.ckInstr(got_expect_runway != None, 'REPORT BUG! Landing without a runway :-(')
			if instr.isVoiceRecognised():
				str_voice_rwys = instr.voice_data['rwy']
				if str_voice_rwys != '':
					self.ckInstr(str_voice_rwys == got_expect_runway.arg, 'Established on final for RWY %s.' % got_expect_runway.arg)
			pop_all(self.instructions, lambda i: i.type == Instruction.CLEARED_TO_LAND)
			
		elif instr.type == Instruction.HAND_OVER:
			pop_all(self.instructions, lambda i: i.type == Instruction.HAND_OVER)
		
		# ADD instruction to list (except if type is SAY_*)
		if instr.type != Instruction.SAY_INTENTIONS:
			self.instructions.append(instr)
	
	## This is where AI pilot does something about an instruction of his list that he must follow
	def followInstruction(self, instr):
		if instr.type == Instruction.VECTOR_HDG:
			self.turnTowards(instr.arg, tol=pilot_hdg_precision)
			
		elif instr.type == Instruction.VECTOR_DCT:
			self.flyTowards(instr.arg.coordinates)
			
		elif instr.type == Instruction.VECTOR_ALT:
			if not self.isGroundStatus():
				self.climbDescendTowards(env.stdPressureAlt(instr.arg))
				
		elif instr.type == Instruction.VECTOR_SPD:
			self.accelDecelTowards(instr.arg)
			
		elif instr.type == Instruction.FOLLOW_ROUTE:
			self.flyTowards(instr.arg.currentWaypoint(self.params.position).coordinates)
				
		elif instr.type == Instruction.HOLD:
			hldg_fix, std_turns = instr.arg
			if self.statusType() != Status.HLDG:
				self.params.status = Status(Status.HLDG, arg=None)
			if self.params.status.arg == None: # going for fix
				if self.params.position.distanceTo(hldg_fix.coordinates) <= pilot_nav_precision: # got there
					hldg_hdg = self.params.heading + (init_hldg_turn if std_turns else -init_hldg_turn)
					self.params.status = Status(Status.HLDG, arg=(hldg_hdg, hldg_leg_fly_time))
				else:
					self.flyTowards(hldg_fix.coordinates)
			else: # in the loop
				hldg_hdg, outbound_ttf = self.params.status.arg
				if outbound_ttf > timedelta(0): # flying outbound leg
					if self.params.heading.diff(hldg_hdg, tolerance=pilot_hdg_precision) == 0:
						self.params.status.arg = hldg_hdg, self.params.status.arg[1] - self.tick_interval
					else:
						self.turnTowards(hldg_hdg, pilot_hdg_precision)
				else: # flying inbound leg
					self.flyTowards(hldg_fix.coordinates)
					if self.params.position.distanceTo(hldg_fix.coordinates) <= pilot_nav_precision:
						self.params.status.arg = hldg_hdg, hldg_leg_fly_time
			
		elif instr.type == Instruction.SQUAWK:
			self.params.XPDR_code = instr.arg
			
		elif instr.type == Instruction.CANCEL_APP:
			self.MISAP()
			if settings.game_manager.game_type == GameType.TEACHER:
				self.goal = None
				
		elif instr.type == Instruction.INTERCEPT_NAV:
			self.intercept(instr.arg[0].coordinates, instr.arg[1], navpoint_intercept_angle, tolerance=pilot_hdg_precision)
				
		elif instr.type == Instruction.INTERCEPT_LOC:
			rwy = env.airport_data.runway(self.instrOfType(Instruction.EXPECT_RWY).arg)
			# TODO limit to/from intercept
			self.intercept(rwy.threshold(dthr=True), rwy.orientation(), rwy.param_LOCangle, tolerance=pilot_hdg_precision)
			
		elif instr.type == Instruction.EXPECT_RWY:
			rwy = env.airport_data.runway(instr.arg)
			if self.isGroundStatus():
				if self.instrOfType(Instruction.TAXI) == None \
						and self.params.position.distanceTo(rwy.threshold()) <= ready_max_dist_to_threshold:
					self.params.status = Status(Status.READY, arg=instr.arg)
					self.say('Short of %s, ready for departure.' % rwy.name)
			else:
				td_point = rwy.threshold(dthr=True)
				if self.statusType() == Status.LANDING:
					self.intercept(td_point, rwy.orientation(), rwy.param_LOCangle, tolerance=0, force=True)
					self.approachDescent(rwy)
				elif self.goal == False and not self.params.runway_reported_in_sight and self.groundPointInSight(td_point):
					# Start visual approach
					self.say('Runway %s in sight.' % rwy.name)
					self.params.runway_reported_in_sight = True
		
		elif instr.type == Instruction.TAXI:
			if self.statusType() != Status.RWY_LDG: # Other on-ground status with RWY argument
				self.params.status = Status(Status.TAXIING)
			if instr.arg[0] != []: # still got nodes to taxi
				next_target = env.airport_data.ground_net.nodePosition(instr.arg[0][0])
			elif instr.arg[1] != None: # final pkg pos
				next_target = env.airport_data.ground_net.parkingPosition(instr.arg[1])
			else: # no taxi goal left
				next_target = None
			if next_target != None:
				if self.params.position.distanceTo(next_target) <= pilot_taxi_precision:
					if instr.arg[0] != []: # still got nodes to taxi
						del instr.arg[0][0]
						if self.canPark() and instr.arg[0] == []:
							self.say('Request contact with ramp.')
					else:
						instr.arg = [], None
				else:
					hdg = self.params.position.headingTo(next_target)
					if self.params.heading.diff(hdg, tolerance=.01) != 0: # not facing goal
						self.turnTowards(hdg, tol=0, fastOK=True)
					else:
						self.params.ias = taxi_speed
						self.taxiTowards(next_target)
			
		elif instr.type == Instruction.HOLD_POSITION:
			pop_all(self.instructions, lambda i: i.type == Instruction.TAXI)
			
		elif instr.type == Instruction.CLEARED_APP:
			rwy = env.airport_data.runway(self.instrOfType(Instruction.EXPECT_RWY).arg)
			if self.params.runway_reported_in_sight or rwy.threshold().distanceTo(self.params.position) <= rwy.param_LOCrange \
					and self.intercept(rwy.threshold(dthr=True), rwy.orientation(), rwy.param_LOCangle, tolerance=0):
				pop_all(self.instructions, lambda i: i.type in [Instruction.VECTOR_ALT, Instruction.VECTOR_SPD])
				self.params.status = Status(Status.LANDING, arg=rwy.name)
			
		elif instr.type == Instruction.LINE_UP:
			if self.statusType() == Status.READY:
				rwy = env.airport_data.runway(self.params.status.arg)
				target = rwy.threshold().moved(rwy.orientation(), .03)
				if self.params.position.distanceTo(target) > pilot_taxi_precision:
					self.params.ias = taxi_speed
					self.taxiTowards(target)
				else:
					self.params.ias = Speed(0)
					hdg = rwy.orientation()
					if self.params.heading.diff(hdg, tolerance=.01) != 0:
						self.turnTowards(hdg, tol=0, fastOK=True)
					else:
						pop_all(self.instructions, lambda i: i.type == Instruction.LINE_UP)
						self.params.status.type = Status.LINED_UP
			
		elif instr.type == Instruction.CLEARED_TKOF:
			if self.statusType() == Status.LINED_UP:
				self.params.status.type = Status.RWY_TKOF
			elif self.statusType() == Status.RWY_TKOF:
				self.taxiForward()
				self.params.ias = self.params.ias + self.maxSpdIncr(self.tick_interval)
				if self.params.ias.diff(take_off_speed(self.aircraft_type)) >= 0:
					self.params.status = Status(Status.AIRBORNE)
					if self.instrOfType(Instruction.VECTOR_ALT) == None:
						self.instructions.append(Instruction(Instruction.VECTOR_ALT, arg=settings.solo_initial_climb_reading))
					self.instructions.append(Instruction(Instruction.VECTOR_SPD, arg=cruise_speed(self.aircraft_type)))
			
		elif instr.type == Instruction.CLEARED_TO_LAND and self.statusType() == Status.RWY_LDG:
			# 3 stages after touch down:
			#   1. slow down (speed > ldg_roll_speed)
			#   2. LDG roll until turn-off point ahead if any (speed == ldg_roll_speed)
			#   3. turn/taxi off RWY (speed == taxi_speed)
			#      (3a) following taxi routes if possible (a TAXI instruction is present)
			#      (3b) "into the wild" if no ground net (no TAXI instruction present)
			if self.instrOfType(Instruction.TAXI) == None: # A taxi instruction (from a stage 3b) will take care of forward move
				self.taxiForward()
			if self.params.ias.diff(ldg_roll_speed) > .1: # In stage 1
				self.accelDecelTowards(ldg_roll_speed, fast=True, tol=0)
			else:
				turning_off = False # init
				rwy = env.airport_data.runway(self.params.status.arg)
				roll_dist = rwy.threshold().distanceTo(self.params.position)
				l1, l2, l3, l4 = env.airport_data.ground_net.runwayTurnOffs(rwy, minroll=roll_dist)
				fwd_turn_offs_available = l1 if l1 != [] else l2
				if self.instrOfType(Instruction.TAXI) != None: # In stage 3a
					turning_off = True
					into_the_wild = False
				elif fwd_turn_offs_available == []:
					if l3 == []: # No turn-off ahead + no possible backtrack. Going to stage (3b).
						turning_off = into_the_wild = True
					else: # Must stop, ACFT will need a backtrack
						self.accelDecelTowards(Speed(0), fast=(roll_dist > rwy.length(dthr=True) * 3 / 4), tol=0)
						if self.params.ias.diff(Speed(0)) == 0:
							self.params.status = Status(Status.TAXIING) # This finishes the instruction
							self.say('Request backtrack runway %s' % rwy.name)
				else: # In stage 2
					next_turn_off_point = env.airport_data.ground_net.nodePosition(fwd_turn_offs_available[0][0])
					if self.params.position.distanceTo(next_turn_off_point) < 2 * pilot_taxi_precision: # chance to turn off
						if len(fwd_turn_offs_available) == 1 or random() < turn_off_choice_prob: # turn off RWY! going to stage 3a
							self.instructions.append(Instruction(Instruction.TAXI, arg=([fwd_turn_offs_available[0][1]], None)))
				if turning_off: # In stage 3
					if into_the_wild:
						if self.params.ias.diff(taxi_speed) > .1: # slow down first
							self.accelDecelTowards(taxi_speed, fast=True, tol=0)
						else:
							finish_heading = rwy.orientation() + default_turn_off_angle
							self.turnTowards(finish_heading, tol=0, fastOK=True)
							turning_off = self.params.heading.diff(finish_heading, tolerance=.1) != 0
					else:
						turning_off = self.instrOfType(Instruction.TAXI) == None # reached RWY cleared point
					if not turning_off: # turn-off finished
						self.params.ias = Speed(0)
						if settings.game_manager.game_type == GameType.SOLO and settings.solo_role_GND:
							self.goal = choice(env.airport_data.ground_net.parkingPositions(acftType=self.aircraft_type))
							pkinfo = env.airport_data.ground_net.parkingPosInfo(self.goal)
							self.say('Runway %s clear for %s %s.' % (rwy.name, pkinfo[2], self.goal))
						else:
							self.say('Runway %s clear.' % rwy.name)
						self.params.status = Status(Status.TAXIING) # This finishes the instruction
			
		elif instr.type == Instruction.HAND_OVER:
			self.released = True
	
	
	## AUXILIARY METHODS FOR INSTRUCTION FOLLOWING
	
	def ckInstr(self, accept_condition, msg_if_rejected):
		if not accept_condition:
			raise Instruction.Error(msg_if_rejected)
	
	def ckVoiceInstrAndEnsureRwy(self, instr):
		if instr.isVoiceRecognised():
			str_voice_rwys = instr.voice_data['rwy']
			if str_voice_rwys != '':
				expect_instr = self.instrOfType(Instruction.EXPECT_RWY)
				if expect_instr == None: # was not expecting a runway; is one included in this voice instruction?
					self.instruct(Instruction(Instruction.EXPECT_RWY, arg=str_voice_rwys)) # if more than one listed here, name will be rejected
				else: # already expecting a runway; if one is included here, it should match
					self.ckInstr(str_voice_rwys == expect_instr.arg, 'Expecting RWY %s. Cancel this approach?' % expect_instr.arg)
		self.ckInstr(self.instrOfType(Instruction.EXPECT_RWY) != None, 'No runway given.')
	
	def ckResolveNavpointInstr(self, instr):
		try:
			if instr.type == Instruction.VECTOR_DCT: # arg is single navpoint
				instr.arg = env.navpoints.findClosest(env.radarPos(), code=instr.arg)
			elif instr.type in [Instruction.HOLD, Instruction.INTERCEPT_NAV]: # arg is pair with navpoint first
				navpoint = env.navpoints.findClosest(env.radarPos(), code=instr.arg[0], types=[Navpoint.VOR, Navpoint.NDB])
				instr.arg = navpoint, instr.arg[1]
		except NavpointError as err:
			raise Instruction.Error('Where is %s??' % err)
	
	def intercept(self, point, hdg, intercept_range, tolerance, force=False):
		dct = self.params.position.toRadarCoords().headingTo(point.toRadarCoords())
		opp = dct.opposite()
		interception = force or abs(hdg.diff(opp)) <= intercept_range or abs(hdg.opposite().diff(opp)) <= intercept_range
		if interception: # in LOC/QDM/QDR cone
			pop_all(self.instructions, lambda i: i.type in [Instruction.VECTOR_HDG, Instruction.VECTOR_DCT, Instruction.FOLLOW_ROUTE])
			diff = dct.diff(hdg)
			delta = diff if abs(diff) < 90 else opp.diff(hdg)
			self.turnTowards(hdg + delta * approach_angle / intercept_range, tolerance, fastOK=True)
		return interception
		
	def approachDescent(self, runway):
		touch_down_point = runway.threshold(dthr=True)
		touch_down_dist = self.params.position.distanceTo(touch_down_point)
		gs_diff = self.params.altitude.diff(GS_alt(env.elevation(touch_down_point), runway.param_FPA, touch_down_dist), tolerance=0)
		if gs_diff > 0: # must descend
			drop = min(fast_climb_descend_factor * self.maxClimb(self.tick_interval), gs_diff)
			self.params.altitude = StdPressureAlt.fromAMSL(self.params.altitude.ft1013() - drop, qnh=1013.25)
		self.accelDecelTowards(touch_down_speed(self.aircraft_type), accelOK=False)
		rwy_ori = runway.orientation()
		if abs(self.params.position.headingTo(touch_down_point).diff(rwy_ori)) >= 90:
			self.say('Facing wrong direction! Executing missed approach.')
			self.MISAP()
			return
		height = self.params.altitude.diff(env.groundStdPressureAlt(self.params.position))
		if height < min_clearToLand_height and self.instrOfType(Instruction.CLEARED_TO_LAND) == None:
			self.say('Going around; not cleared to land.')
			self.MISAP()
		elif touch_down_dist <= touch_down_distance_tolerance: # Attempt touch down!
			alt_check = height <= touch_down_height_tolerance
			hdg_check = self.params.heading.diff(rwy_ori, tolerance=touch_down_heading_tolerance) == 0
			speed_check = self.params.ias.diff(touch_down_speed(self.aircraft_type), tolerance=touch_down_speed_tolerance) <= 0
			if alt_check and hdg_check and speed_check: # TOUCH DOWN!
				self.params.status.type = Status.RWY_LDG
				self.params.heading = rwy_ori
				if settings.game_manager.game_type == GameType.TEACHER:
					self.goal = None # was set to enable ILS, but no real goal for teacher's traffic
			else: # Missed approach
				reason = ('not lined up' if speed_check else 'too fast') if alt_check else 'too high'
				self.say('Missed touch down: %s, going around.' % reason)
				self.MISAP()
	
	def taxiTowards(self, coords):
		self.params.heading = self.params.position.headingTo(coords)
		self.taxiForward(maxdist=self.params.position.distanceTo(coords))
	
	def taxiForward(self, maxdist=None):
		dist = distance_flown(self.tick_interval, self.params.ias)
		if maxdist != None and dist > maxdist:
			dist = maxdist
		self.params.position = self.params.position.moved(self.params.heading, dist)
		self.params.altitude = env.groundStdPressureAlt(self.params.position)
	
	def turnTowards(self, hdg, tol, fastOK=False):
		diff = hdg.diff(self.params.heading, tolerance=tol)
		if diff == 0:
			self.last_turn_direction = 0
		else:
			abs_turn = min((fast_turn_factor if fastOK else 1) * self.maxTurn(self.tick_interval), abs(diff))
			self.last_turn_direction = 1 if diff > 0 else -1
			self.params.heading += self.last_turn_direction * abs_turn
	
	def climbDescendTowards(self, alt, climbOK=True, descendOK=True):
		diff = alt.diff(self.params.altitude, tolerance=pilot_alt_precision)
		if diff < 0 and descendOK or diff > 0 and climbOK:
			vert = min(self.maxClimb(self.tick_interval), abs(diff))
			self.params.altitude = StdPressureAlt.fromAMSL(self.params.altitude.ft1013() + (vert if diff > 0 else -vert), qnh=1013.25)

	def accelDecelTowards(self, spd, accelOK=True, decelOK=True, fast=False, tol=pilot_spd_precision):
		diff = spd.diff(self.params.ias, tolerance=tol)
		if diff < 0 and decelOK or diff > 0 and accelOK:
			spdincr = min((fast_accel_decel_factor if fast else 1) * self.maxSpdIncr(self.tick_interval), abs(diff))
			self.params.ias = self.params.ias + (spdincr if diff > 0 else -spdincr)
	
	def flyTowards(self, coords):
		self.turnTowards(self.params.position.headingTo(coords), tol=pilot_hdg_precision)
	
	def MISAP(self):
		self.params.status = Status(Status.AIRBORNE)
		pop_all(self.instructions, lambda i: i.type in [Instruction.CLEARED_TO_LAND, Instruction.CLEARED_APP, Instruction.INTERCEPT_LOC])
		if self.params.altitude.diff(env.stdPressureAlt(MISAP_climb_reading)) < 0:
			self.instructions.append(Instruction(Instruction.VECTOR_ALT, arg=MISAP_climb_reading))
	
	
	## RADIO
	
	def say(self, message): # FUTURE[tts] add param with speech synth tokenised input
		signals.incomingTextChat.emit(ChatMessage(self.identifier, message))
	
	def makeInitialContact(self):
		msg = 'Hello, '
		
		if self.statusType() == Status.READY:
			msg += 'short of runway %s, ready for departure.' % self.params.status.arg
			
		elif self.statusType() == Status.LANDING:
			if self.goal: # ILS
				msg += 'established ILS runway %s' % self.params.status.arg
			else: # visual approach
				msg += 'on visual for runway %s' % self.params.status.arg
			
		elif self.statusType() == Status.TAXIING:
			if self.wantsToPark():
				msg += 'runway cleared, for parking at %s' % self.goal
			else:
				if env.airport_data != None:
					pk = env.airport_data.ground_net.closestParkingPosition(self.params.position, maxdist=.1)
					if pk != None:
						msg += 'standing at %s, ' % pk
				msg += 'ready to taxi'
		
		elif self.isInboundGoal():
			msg += 'with you, %s' % self.params.altitude.read(qnh=env.QNH())
			msg += ', inbound for %s approach' % ('ILS' if self.goal else 'visual')
			instr = self.instrOfType(Instruction.EXPECT_RWY)
			if instr != None:
				msg += ' runway %s' % instr.arg
			
		elif self.isOutboundGoal(): # Normally a departure received from TWR
			msg += self.params.altitude.read(qnh=env.QNH())
			instr = self.instrOfType(Instruction.VECTOR_ALT)
			if instr != None:
				msg += ' for %s' % instr.arg
		
		else: # Transit for CTR
			msg += 'with you, %s' % self.params.altitude.read(qnh=env.QNH())
		## Now SAY IT!
		self.say(msg)
	
	def readBack(self, instr):
		if instr.type == Instruction.VECTOR_HDG:
			msg = 'Heading %s' % instr.arg.read()
		elif instr.type == Instruction.VECTOR_ALT:
			msg = instr.arg
		elif instr.type == Instruction.VECTOR_SPD:
			msg = str(instr.arg)
		elif instr.type == Instruction.VECTOR_DCT:
			msg = 'Direct %s' % instr.arg.code
		elif instr.type == Instruction.FOLLOW_ROUTE:
			msg = 'Route copied, now proceeding direct %s' % instr.arg.currentWaypoint(self.params.position)
		elif instr.type == Instruction.HOLD:
			fix, turns = instr.arg
			msg = 'Hold at %s, %s turns' % (fix.code, ('right' if turns else 'left'))
		elif instr.type == Instruction.SQUAWK:
			msg = '%04d' % instr.arg
		elif instr.type == Instruction.CANCEL_APP:
			msg = 'Cancel approach'
		elif instr.type == Instruction.HAND_OVER:
			if instr.arg[1] == None:
				msg = 'With %s, thank you, good bye.' % instr.arg[0]
			else:
				msg = '%s on %s, good bye.' % instr.arg
		elif instr.type == Instruction.LINE_UP:
			msg = 'Line up and wait'
		elif instr.type == Instruction.INTERCEPT_NAV:
			msg = 'Intercept %s %s' % (instr.arg[0].code, instr.arg[1].read())
		elif instr.type == Instruction.INTERCEPT_LOC:
			msg = 'Intercept LOC RWY %s' % self.instrOfType(Instruction.EXPECT_RWY).arg
		elif instr.type == Instruction.EXPECT_RWY:
			if self.isGroundStatus():
				msg = 'Runway %s, will report ready for departure.' % instr.arg
			else:
				msg = '%s runway %s' % (('ILS' if self.goal else 'Visual'), instr.arg)
		elif instr.type == Instruction.TAXI:
			if env.airport_data == None:
				msg = 'Unable to taxi'
			else:
				msg = env.airport_data.ground_net.taxiInstrStr(*instr.arg)
		elif instr.type == Instruction.HOLD_POSITION:
			msg = 'Hold position'
		elif instr.type == Instruction.CLEARED_APP:
			msg = 'Cleared %s %s' % (('ILS' if self.goal else 'visual'), self.instrOfType(Instruction.EXPECT_RWY).arg)
		elif instr.type == Instruction.CLEARED_TKOF:
			msg = 'Cleared for take-off runway %s' % self.params.status.arg
		elif instr.type == Instruction.CLEARED_TO_LAND:
			msg = 'Clear to land runway %s' % self.instrOfType(Instruction.EXPECT_RWY).arg
		elif instr.type == Instruction.SAY_INTENTIONS:
			if self.wantsToPark():
				msg = 'Park at %s' % self.goal
			elif self.isInboundGoal():
				msg = '%s approach' % ('ILS' if self.goal else 'Visual')
				instr2 = self.instrOfType(Instruction.EXPECT_RWY)
				if instr2 != None:
					msg += ', expecting runway %s' % instr2.arg
			elif self.isOutboundGoal():
				msg = 'Departing'
				if self.goal[0] != None:
					msg += ' via %s' % self.goal[0].code
				if self.goal[1] != None:
					msg += ', cruise %s' % self.goal[1]
			elif isinstance(self.goal, Airfield): # Transiting with destination
				msg = 'En-route to %s' % self.goal
			else:
				msg = 'No intentions'
		## Now SAY IT!
		self.say(msg)
	
	
	def FGMSlivePositionPacket(self):
		coords, amsl = self.live_position
		amsl += settings.AI_traffic_FG_AMSL_correction
		model = FGFS_model(self.aircraft_type)
		if self.statusType() in [Status.AIRBORNE, Status.HLDG]:
			deg_roll = self.last_turn_direction * right_turn_roll
		else:
			deg_roll = 0
		if self.statusType() == Status.LANDING:
			deg_pitch = final_pitch
		elif self.isGroundStatus():
			deg_pitch = 0
		else:
			deg_pitch = pitch_factor * some(self.verticalSpeed(), 0)
		data = position_data(model, coords, amsl, hdg=self.params.heading.trueAngle(), pitch=deg_pitch, roll=deg_roll)
		# XPDR prop's
		data.pack_int(FGMS_prop_XPDR_mode)
		data.pack_int(FGMS_prop_XPDR_mode_encoding[self.params.XPDR_mode])
		if self.params.XPDR_mode not in '0G':
			data.pack_int(FGMS_prop_XPDR_code)
			data.pack_int(self.params.XPDR_code)
			if self.params.XPDR_mode in 'CS':
				data.pack_int(FGMS_prop_XPDR_alt)
				data.pack_int(int(self.params.altitude.ft1013()))
			data.pack_int(FGMS_prop_XPDR_ident)
			data.pack_bool(self.params.XPDR_idents)
		# engines
		for prop in FGMS_props_engine_RPM:
			data.pack_int(prop)
			data.pack_float(RPM_low if self.params.ias.kt < 1 else RPM_high)
		# landing gear
		for prop in FGMS_props_gear_position: # FLOAT: 0=retracted; 1=extended
			data.pack_int(prop)
			data.pack_float(float(self.isGroundStatus() or self.statusType() == Status.LANDING))
		# gear compression
		for prop in FGMS_props_gear_compression: # FLOAT: 0=free; 1=compressed
			data.pack_int(prop)
			data.pack_float(gear_compression_high if self.isGroundStatus() else gear_compression_low)
		# livery
		if self.livery != None:
			data.pack_int(FGMS_prop_livery_file)
			data.pack_FGFS_buggy_string(self.livery)
		# finish up
		data.pack_int(FGMS_prop_text_chat)
		data.pack_FGFS_buggy_string('')
		return make_fgms_packet(self.identifier, position_message_type_code, data).allData()


