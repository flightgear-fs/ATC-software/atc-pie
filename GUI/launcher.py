
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QWidget, QMessageBox
from ui.launcher import Ui_launcher

from util import some
from settings import settings, default_map_range_AD, default_map_range_CTR, version_string
from game.env import env

from data.coords import EarthCoords
from data.nav import world_nav_data, Navpoint, NavpointError
from data.radar import Radar
from data.params import Heading

from models.gameStrips import StripModel
from models.FPLs import FlightPlanModel
from models.ATCs import AtcNeighboursModel
from models.discardedStrips import DiscardedStripModel

from ext.noaa import get_declination
from ext.xplane import get_airport_data, get_frequencies
from ext.resources import read_bg_img, read_point_spec, get_ground_elevation_map
from ext.sr import prepare_SR

from GUI.main import MainWindow


# ---------- Constants ----------

# -------------------------------


def valid_location_code(code):
	return code.isalnum()



class ATCpieLauncher(QWidget, Ui_launcher):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.version_info.setText('Version: %s' % version_string)
		self.last_selected_ICAO = None
		self.updateCtrLocationsList()
		self.ctrLocationCode_edit.lineEdit().textChanged.connect(self.updateStartButton)
		self.ctrLocationCode_edit.activated.connect(self.selectListedCtrLocation)
		self.AD_radioButton.toggled.connect(self.switchMode)
		self.airport_select.recognised.connect(self.recogniseAD)
		self.airport_select.unrecognised.connect(self.unrecogniseAD)
		self.airport_select.airport_edit.returnPressed.connect(self.start_button.click)
		self.start_button.clicked.connect(self.launchWithWindowInput)
		self.exit_button.clicked.connect(self.close)
		self.AD_radioButton.setChecked(True)
	
	def switchMode(self, ad_mode):
		self.mapRange_edit.setValue(default_map_range_AD if ad_mode else default_map_range_CTR)
		self.updateStartButton()
	
	def selectListedCtrLocation(self, index):
		self.ctrRadarPos_edit.setText(settings.CTR_radar_positions[self.ctrLocationCode_edit.itemText(index)])
	
	def updateStartButton(self):
		if self.AD_radioButton.isChecked():
			self.start_button.setEnabled(self.last_selected_ICAO != None)
		else:
			self.start_button.setEnabled(valid_location_code(self.ctrLocationCode_edit.currentText()))
	
	def updateCtrLocationsList(self):
		self.ctrLocationCode_edit.clear()
		self.ctrLocationCode_edit.addItems(sorted(settings.CTR_radar_positions.keys()))
		self.ctrLocationCode_edit.clearEditText()
	
	def recogniseAD(self, ad):
		self.last_selected_ICAO = ad.code
		self.selectedAD_info.setText(ad.long_name)
		self.start_button.setEnabled(True)
	
	def unrecogniseAD(self):
		self.last_selected_ICAO = None
		self.selectedAD_info.clear()
		self.start_button.setEnabled(False)
	
	def launchWithWindowInput(self):
		try:
			if self.AD_radioButton.isChecked(): # Airport mode
				self.launch(self.last_selected_ICAO, mapRange=self.mapRange_edit.value())
			else: # CTR mode
				self.launch(self.ctrLocationCode_edit.currentText(), ctrPos=self.ctrRadarPos_edit.text(), mapRange=self.mapRange_edit.value())
		except ValueError as err:
			QMessageBox.critical(self, 'Start-up error', str(err))
	
	def launch(self, location_code, mapRange=None, ctrPos=None):
		'''
		Raise ValueError with error message if launch fails.
		'''
		settings.map_range = some(mapRange, (default_map_range_AD if ctrPos == None else default_map_range_CTR))
		print('Setting up session %s in %s mode at location %s.' % (settings.sessionID(), ['AD', 'CTR'][ctrPos != None], location_code))
		try:
			if ctrPos == None: # Airport mode
				env.airport_data = get_airport_data(location_code)
				EarthCoords.setRadarPos(env.airport_data.navpoint.coordinates)
				env.frequencies = get_frequencies(env.airport_data.navpoint.code)
				try:
					settings.restoreLocalSettings_AD(env.airport_data)
				except FileNotFoundError:
					print('No airport settings file found; using defaults.')
				else:
					settings.first_time_at_location = False
				try:
					env.elevation_map = get_ground_elevation_map(location_code)
					print('Loaded ground elevation map.')
				except FileNotFoundError:
					print('No elevation map found; using field elevation.')
			else: # CTR mode
				radar_position = read_point_spec(ctrPos, world_nav_data)
				env.CTR_closest_airport = world_nav_data.findClosest(radar_position, types=[Navpoint.AD])
				EarthCoords.setRadarPos(radar_position)
				env.frequencies = get_frequencies(env.CTR_closest_airport.code)
				try:
					settings.restoreLocalSettings_CTR(location_code)
				except FileNotFoundError:
					print('No CTR settings file found; using defaults.')
				else:
					settings.first_time_at_location = False
				if location_code in settings.CTR_radar_positions:
					print('Overriding previously saved radar position.')
				settings.CTR_radar_positions[location_code] = ctrPos
				self.updateCtrLocationsList()
		except NavpointError as err:
			raise ValueError('Navpoint error: %s' % err)
		else:
			print('Radar position is: %s' % env.radarPos())
			Heading.declination = get_declination(env.radarPos())
			env.navpoints = world_nav_data.subDB(lambda p: env.pointOnMap(p.coordinates))
			env.radar = Radar(self) # CAUTION: uses airport data; make sure it is already in env
			env.strips = StripModel(self)
			env.FPLs = FlightPlanModel(self)
			env.ATCs = AtcNeighboursModel(self)
			env.discarded_strips = DiscardedStripModel(self)
			try:
				settings.restoreGeneralAndSystemSettings()
			except FileNotFoundError:
				print('No general settings file found; using defaults.')
			settings.radar_background_images, settings.loose_strip_bay_backgrounds = read_bg_img(location_code, env.navpoints)
			prepare_SR(location_code)
			w = MainWindow(self)
			w.show()
			self.close()
			if settings.first_time_at_location:
				msg = 'This is your first time at %s.\nCheck/configure local settings!' % location_code
				if env.airport_data == None:
					QMessageBox.information(w, 'New radar centre location', msg)
				else:
					missing = []
					if len(env.airport_data.viewpoints) == 0:
						missing.append('viewpoint -- tower view will position randomly')
					if len(env.airport_data.ground_net.taxiways()) == 0:
						missing.append('ground route network -- solo ground control disabled')
					if len(env.airport_data.ground_net.parkingPositions()) == 0:
						missing.append('parking positions -- solo ground control disabled')
					if missing != []:
						msg += '\n\nAirport data missing (though this could be normal):\n'
						msg += '\n'.join(' - ' + s for s in missing)
					QMessageBox.information(w, 'New airport location', msg)



