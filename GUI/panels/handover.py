
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QWidget
from ui.handover import Ui_handOverPane

from settings import settings
from game.env import env
from GUI.misc import signals


# ---------- Constants ----------

# -------------------------------


class HandoverPane(QWidget, Ui_handOverPane):
	
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.ATC_view.setModel(env.ATCs)
		self.publicFrequency_edit.addFrequencies([(f, d) for f, d, t in env.frequencies if t != 'recorded'])
		self.publiciseFrequency_tickBox.toggled.connect(self.publiciseFrequency)
		self.publicFrequency_edit.frequencyChanged.connect(self.setPublicFrequency)
		signals.gameStopped.connect(env.ATCs.clear)
		
	def publiciseFrequency(self, toggle):
		settings.publicised_frequency = self.publicFrequency_edit.getFrequency() if toggle else None
	
	def setPublicFrequency(self, frq):
		settings.publicised_frequency = frq
	

