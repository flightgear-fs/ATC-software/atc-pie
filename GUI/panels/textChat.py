
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re
from PyQt5.QtCore import Qt, QObject, QEvent, QAbstractTableModel, QSortFilterProxyModel, QModelIndex
from PyQt5.QtWidgets import QWidget, QInputDialog, QMenu, QAction, QMessageBox, QCompleter

from ui.textChat import Ui_textChatFrame

from util import some
from settings import settings

from data.strip import rack_detail, runway_box_detail, parsed_route_detail, \
		assigned_SQ_detail, assigned_altitude_detail, assigned_heading_detail, assigned_speed_detail
from data.coords import dist_str
from game.env import env
from game.manager import GameType
from data.FPL import FPL
from data.UTC import timestr
from data.params import transition_level, Heading, hPa2inHg
from data.chatMsg import ChatMessage

from GUI.misc import selection, signals
from GUI.dialog.miscDialogs import yesNo_question


# ---------- Constants ----------

text_alias_prefix = '$'
text_snip_separator = '|'

# -------------------------------



## ALIASES AND REPLACEMENTS ##


alias_regexp = re.compile('%s(\w+)' % re.escape(text_alias_prefix))




def noNone(value, failmsg=None):
	if value == None:
		raise ValueError(some(failmsg, 'Unaccepted None value'))
	return value


def custom_alias_search(alias, text):
	try:
		lines = [line.strip() for line in text.split('\n')]
		return next(line.split('=', maxsplit=1)[1] for line in lines if line.startswith('%s=' % alias))
	except StopIteration:
		raise ValueError('Alias not found: %s%s' % (text_alias_prefix, alias))


def alias_match_replacement(alias_match):
	alias = alias_match.group(1).lower()
	weather = env.primaryWeather()
	## Check for general alias
	if alias == 'ad':
		return 'N/A' if env.airport_data == None else env.locationName()
	elif alias == 'decl':
		return some(env.readDeclination(), 'N/A')
	elif alias == 'elev':
		return 'N/A' if env.airport_data == None else '%d ft' % env.airport_data.navpoint.altitude
	elif alias == 'frq':
		return noNone(settings.publicised_frequency)
	elif alias == 'icao':
		return settings.location_code
	elif alias == 'me':
		return settings.game_manager.myCallsign()
	elif alias == 'metar':
		return 'N/A' if weather == None else weather.METAR()
	elif alias == 'qfe':
		qnh = env.QNH(noneSafe=False)
		return 'N/A' if env.airport_data == None or qnh == None else '%d' % env.QFE(qnh)
	elif alias == 'qnh':
		qnh = env.QNH(noneSafe=False)
		return 'N/A' if qnh == None else '%d' % qnh
	elif alias == 'qnhg':
		qnh = env.QNH(noneSafe=False)
		return 'N/A' if qnh == None else '%.2f' % (hPa2inHg * qnh)
	elif alias == 'runways':
		return env.readRunwaysInUse()
	elif alias == 'rwyarr':
		if env.airport_data == None:
			return 'N//A'
		else:
			rwys = [rwy.name for rwy in env.airport_data.allRunways() if rwy.use_for_arrivals]
			return 'N/A' if rwys == [] else ', '.join(rwys)
	elif alias == 'rwydep':
		if env.airport_data == None:
			return 'N//A'
		else:
			rwys = [rwy.name for rwy in env.airport_data.allRunways() if rwy.use_for_departures]
			return 'N/A' if rwys == [] else ', '.join(rwys)
	elif alias == 'ta':
		return '%d ft' % settings.transition_altitude
	elif alias == 'tl':
		qnh = env.QNH(noneSafe=False)
		return 'N/A' if qnh == None else 'FL%03d' % transition_level(qnh)
	elif alias == 'utc':
		return timestr()
	elif alias == 'wind':
		return 'N/A' if weather == None else weather.readWind()
	else: # Check for selection-dependant alias
		strip = selection.strip
		acft = selection.acft
		if alias == 'dest':
			return noNone(noNone(strip).lookup(FPL.ICAO_ARR))
		elif alias == 'dist':
			coords = noNone(acft).coords()
			return 'N/A' if env.airport_data == None else dist_str(env.airport_data.navpoint.coordinates.distanceTo(coords))
		elif alias == 'nseq':
			return str(env.strips.rackSequenceNumber(noNone(strip))) # rightly fails with ValueError if strip is loose
		elif alias == 'qdm':
			coords = noNone(acft).coords()
			return 'N/A' if env.airport_data == None else coords.headingTo(env.airport_data.navpoint.coordinates).read()
		elif alias == 'rack':
			return noNone(noNone(strip).lookup(rack_detail))
		elif alias == 'route':
			return noNone(noNone(strip).lookup(FPL.ROUTE))
		elif alias == 'rwy':
			box = noNone(noNone(strip).lookup(runway_box_detail))
			return env.airport_data.physicalRunwayNameFromUse(box) # code unreachable if env.airport_data == None
		elif alias == 'sq':
			sq = noNone(strip).lookup(assigned_SQ_detail)
			return str(noNone(sq))
		elif alias == 'valt':
			valt = noNone(strip).lookup(assigned_altitude_detail)
			return noNone(valt) # valt is a "reading"
		elif alias == 'vhdg':
			vhdg = noNone(strip).lookup(assigned_heading_detail)
			return noNone(vhdg).read()
		elif alias == 'vspd':
			vspd = noNone(strip).lookup(assigned_speed_detail)
			return str(noNone(vspd))
		elif alias == 'wpnext':
			coords = noNone(acft).coords()
			route = noNone(strip).lookup(parsed_route_detail)
			return str(noNone(route).currentWaypoint(coords))
		elif alias == 'wpsid':
			route = noNone(strip).lookup(parsed_route_detail)
			return noNone(noNone(route).SID())
		elif alias == 'wpstar':
			route = noNone(strip).lookup(parsed_route_detail)
			return noNone(noNone(route).STAR())
		else:
			## Check for custom alias, in order: general notes, location-specific notes, selected strip comments
			try:
				return custom_alias_search(alias, settings.general_notes)
			except ValueError:
				try:
					return custom_alias_search(alias, settings.local_notes)
				except ValueError:
					comments = noNone(noNone(strip).lookup(FPL.COMMENTS))
					return custom_alias_search(alias, comments)


def alias_match_replacement_nofail(alias_match):
	try:
		return alias_match_replacement(alias_match)
	except ValueError:
		return alias_match.group(0)


def process_text_chat_line(full_line, nofail=False):
	message = full_line.split(text_snip_separator, maxsplit=1)[-1]
	return alias_regexp.sub((alias_match_replacement_nofail if nofail else alias_match_replacement), message)





# =============================================== #

#                     MODELS                      #

# =============================================== #




class FullTextChatHistoryModel(QAbstractTableModel):

	columns = ['Time', 'From', 'Message']

	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.msg_list = []

	def rowCount(self, parent=None):
		return len(self.msg_list)

	def columnCount(self, parent):
		return len(FullTextChatHistoryModel.columns)

	def data(self, index, role):
		if role == Qt.DisplayRole:
			chat_line = self.messageOnRow(index.row())
			col = index.column()
			if col == 0:
				return timestr(chat_line.timeStamp(), seconds=True)
			if col == 1:
				return chat_line.sender()
			if col == 2:
				return chat_line.txtMsg()

	def headerData(self, section, orientation, role):
		if role == Qt.DisplayRole:
			if orientation == Qt.Horizontal:
				return FullTextChatHistoryModel.columns[section]
	
	def messageOnRow(self, index):
		return self.msg_list[index]
	
	def addChatMessage(self, msg):
		position = self.rowCount()
		self.beginInsertRows(QModelIndex(), position, position)
		self.msg_list.insert(position, msg)
		self.endInsertRows()
		return True







class FilteredTextChatHistoryModel(QSortFilterProxyModel):
	def __init__(self, base_model, parent=None):
		QSortFilterProxyModel.__init__(self, parent)
		self.setSourceModel(base_model)
		self.hidden_senders = []
	
	def messageOnRow(self, filtered_list_row):
		source_index = self.mapToSource(self.index(filtered_list_row, 0)).row()
		return self.sourceModel().messageOnRow(source_index)
	
	def filterAcceptsRow(self, sourceRow, sourceParent):
		return self.sourceModel().messageOnRow(sourceRow).sender() not in self.hidden_senders
	
	def blacklistSender(self, callsign):
		self.hidden_senders.append(callsign)
		self.setFilterFixedString('') # triggers a refilter
	
	def blacklist(self):
		return self.hidden_senders
	
	def clearBlacklist(self):
		self.hidden_senders.clear()
		self.setFilterFixedString('') # triggers a refilter





# ================================================ #

#                     WIDGETS                      #

# ================================================ #



class ChatCompleterPopupEventFilter(QObject):
	def __init__(self, on_return_pressed, parent=None):
		QObject.__init__(self, parent)
		self.on_return_pressed = on_return_pressed

	def eventFilter(self, popup_menu, event): # reimplementing
		if event.type() == QEvent.KeyPress and event.key() in [Qt.Key_Return, Qt.Key_Enter]:
			self.on_return_pressed()
			popup_menu.hide()
			return True
		return False



class TextChatFrame(QWidget, Ui_textChatFrame):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		msg_menu = QMenu()
		checkMsgReplacements_action = QAction('Check message', self)
		msg_menu.addAction(checkMsgReplacements_action)
		self.resetMessage_button.setMenu(msg_menu)
		self.chatHistory_baseModel = FullTextChatHistoryModel(parent=self)
		self.chatHistory_filteredModel = FilteredTextChatHistoryModel(self.chatHistory_baseModel, parent=self)
		self.chatHistory_view.setModel(self.chatHistory_filteredModel)
		self.updatePresetMessages()
		dest_menu = QMenu()
		self.blacklistAsSender_action = QAction('Blacklist sender', self)
		self.showBlacklistedSenders_action = QAction('Show blacklisted senders', self)
		self.clearBlacklist_action = QAction('Clear blacklist', self)
		self.blacklistAsSender_action.setEnabled(False)
		self.clearBlacklist_action.setEnabled(False)
		dest_menu.addAction(self.blacklistAsSender_action)
		dest_menu.addSeparator()
		dest_menu.addAction(self.showBlacklistedSenders_action)
		dest_menu.addAction(self.clearBlacklist_action)
		self.resetDest_button.setMenu(dest_menu)
		self.chatLine_input.completer().setCompletionMode(QCompleter.PopupCompletion)
		self.chatLine_input.completer().setFilterMode(Qt.MatchContains)
		self.chatLine_input.completer().popup().installEventFilter(ChatCompleterPopupEventFilter(self.sendChatLine, parent=self))
		# Signal connections
		checkMsgReplacements_action.triggered.connect(lambda: self.checkMsgReplacements('Check/edit message'))
		self.blacklistAsSender_action.triggered.connect(self.blacklistDest)
		self.showBlacklistedSenders_action.triggered.connect(self.showSendersBlacklist)
		self.clearBlacklist_action.triggered.connect(self.chatHistory_filteredModel.clearBlacklist)
		self.clearBlacklist_action.triggered.connect(lambda: self.clearBlacklist_action.setEnabled(False))
		self.dest_combo.editTextChanged.connect(lambda cs: self.blacklistAsSender_action.setEnabled(cs != ''))
		self.send_button.clicked.connect(self.sendChatLine)
		self.chatLine_input.lineEdit().returnPressed.connect(self.sendChatLine)
		self.dest_combo.lineEdit().returnPressed.connect(self.sendChatLine)
		self.chatHistory_view.clicked.connect(self.recallMessage)
		self.resetMessage_button.clicked.connect(self.chatLine_input.clearEditText)
		self.resetMessage_button.clicked.connect(self.chatLine_input.setFocus)
		self.resetDest_button.clicked.connect(self.resetDest)
		signals.selectionChanged.connect(self.suggestChatDestFromNewSelection)
		signals.chatInstructionSuggestion.connect(self.fillInstruction)
		signals.incomingTextChat.connect(self.appendChatMessageToTable)
	
	def focusInEvent(self, event):
		QWidget.focusInEvent(self, event)
		self.chatLine_input.setFocus()
		self.chatLine_input.lineEdit().selectAll()
	
	def appendChatMessageToTable(self, msg):
		self.chatHistory_baseModel.addChatMessage(msg)
		self.chatHistory_view.scrollToBottom()
	
	def _postChatLine(self, txt):
		if txt == '':
			return # Do not send empty lines
		dest = self.dest_combo.currentText()
		msg = ChatMessage(settings.game_manager.myCallsign(), txt, dest)
		if settings.game_manager.isRunning():
			try:
				settings.game_manager.postTextChat(msg)
				self.appendChatMessageToTable(msg)
				self.chatLine_input.setCurrentIndex(-1)
				self.chatLine_input.clearEditText()
			except ValueError as error:
				QMessageBox.critical(self, 'Text chat error', str(error))
		else:
			QMessageBox.critical(self, 'Text chat error', 'No game started.')
		self.chatLine_input.setFocus()
	
	def sendChatLine(self):
		try:
			self._postChatLine(process_text_chat_line(self.chatLine_input.currentText()))
		except ValueError:
			self.checkMsgReplacements('Alias replacements failed!')
	
	def checkMsgReplacements(self, box_title):
		dest = self.dest_combo.currentText()
		txt, ok = QInputDialog.getText(self, box_title, ('Send:' if dest == '' else 'Send to %s:' % dest), \
			text=process_text_chat_line(self.chatLine_input.currentText(), nofail=True))
		if ok:
			self._postChatLine(txt)
		else:
			self.chatLine_input.setFocus()
	
	def fillInstruction(self, dest, msg, send):
		self.dest_combo.setEditText(dest)
		self.chatLine_input.setEditText(msg)
		if send:
			self.sendChatLine()
		else:
			self.chatLine_input.setFocus()
	
	def resetDest(self):
		self.dest_combo.clear()
		self.dest_combo.addItems(['All traffic'] + sorted(list(env.knownCallsigns())))
		self.dest_combo.clearEditText()
		self.dest_combo.setFocus()
	
	def suggestChatDestFromNewSelection(self):
		cs = selection.selectedCallsign()
		if cs != None:
			self.dest_combo.setEditText(cs)
		
	def updatePresetMessages(self):
		self.chatLine_input.clear()
		self.chatLine_input.addItems(settings.preset_chat_messages)
		self.chatLine_input.clearEditText()
	
	def recallMessage(self, index):
		msg = self.chatHistory_filteredModel.messageOnRow(index.row())
		if msg.isFromMe():
			self.dest_combo.setEditText(msg.dest) # not None, but potentially empty string
			self.chatLine_input.setEditText(msg.text)
		else:
			self.dest_combo.setEditText(msg.sender())
		self.chatLine_input.setFocus()
	
	def blacklistDest(self):
		cs = self.dest_combo.currentText()
		if cs != '' and yesNo_question(self, 'Blacklisting from chat', \
					'This will hide past and future messages from %s.' % cs, 'OK?'):
			self.chatHistory_filteredModel.blacklistSender(cs)
			self.clearBlacklist_action.setEnabled(True)
	
	def showSendersBlacklist(self):
		lst = self.chatHistory_filteredModel.blacklist()
		if lst == []:
			txt = 'No blacklisted senders.'
		else:
			txt = 'Blacklisted senders: %s.' % ', '.join(lst)
		QMessageBox.information(self, 'Senders blacklist', txt)




