
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex
from PyQt5.QtWidgets import QWidget
from ui.teachingConsole import Ui_teachingConsole

from util import some
from settings import settings
from data.params import Heading
from data.UTC import timestr, now
from data.strip import assigned_SQ_detail
from data.weather import generateMETAR_clearWeather, gust_diff_threshold
from game.env import env
from game.manager import GameType, student_callsign, teacher_callsign
from GUI.misc import signals, selection
from GUI.dialog.miscDialogs import yesNo_question


# ---------- Constants ----------

# -------------------------------



# =============================================== #

#                     MODELS                      #

# =============================================== #


class SituationSnapshotModel(QAbstractTableModel):
	columns = ['Situation', 'Traffic']
	
	def __init__(self, parent):
		QAbstractTableModel.__init__(self, parent)
		self.snapshots = [] # (situation, time, name) list

	def rowCount(self, parent=None):
		return len(self.snapshots)

	def columnCount(self, parent):
		return len(SituationSnapshotModel.columns)

	def flags(self, index):
		basic_flags = Qt.ItemIsEnabled | Qt.ItemIsSelectable
		return basic_flags | Qt.ItemIsEditable if index.column() == 0 else basic_flags

	def headerData(self, section, orientation, role):
		if role == Qt.DisplayRole:
			if orientation == Qt.Horizontal:
				return SituationSnapshotModel.columns[section]

	def data(self, index, role):
		sit, t, name = self.snapshots[index.row()] # situation, time, name
		col = index.column()
		if role == Qt.DisplayRole:
			if col == 0:
				return some(name, '%s UTC' % timestr(t, seconds=True))
			elif col == 1:
				spawned = len([acft for acft in sit if acft[4]])
				return '%d + %d' % (spawned, len(sit) - spawned)
		elif role == Qt.ToolTipRole:
			if col == 0:
				return 'Saved at %s UTC' % timestr(t, seconds=True) if name != None else 'Double-click to name.'
			elif col == 1:
				return 'Spawned + unspawned count'

	def setData(self, index, value, role=Qt.EditRole):
		if index.column() == 0:
			row = index.row()
			sit, t, old_name = self.snapshots[row]
			self.snapshots[row] = sit, t, (None if value == '' else value)
			return True
		else:
			return False

	def situationOnRow(self, row):
		return self.snapshots[row][0]
	
	def addSnapshot(self, snapshot):
		position = self.rowCount()
		self.beginInsertRows(QModelIndex(), position, position)
		self.snapshots.insert(position, (snapshot, now(), None))
		self.endInsertRows()
		return True
	
	def removeSnapshot(self, row):
		self.beginRemoveRows(QModelIndex(), row, row)
		del self.snapshots[row]
		self.endRemoveRows()
		return True













# ================================================ #

#                     WIDGETS                      #

# ================================================ #


class TeachingConsole(QWidget, Ui_teachingConsole):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.situationSnapshots_tableModel = SituationSnapshotModel(self)
		self.situationSnapshots_tableView.setModel(self.situationSnapshots_tableModel)
		self.windHdg_radioButton.setText('%s°' % self._windDialHdg().readTrue())
		self.frequency_select.addFrequencies([(frq, descr) for frq, descr, t in env.frequencies])
		self.setEnabled(False)
		self.ATC_neighbours = {} # atc callsign -> frq or None
		# Traffic tab
		self.xpdrMode_select_0.toggled.connect(lambda b: self.setXpdrMode(b, '0'))
		self.xpdrMode_select_G.toggled.connect(lambda b: self.setXpdrMode(b, 'G'))
		self.xpdrMode_select_A.toggled.connect(lambda b: self.setXpdrMode(b, 'A'))
		self.xpdrMode_select_C.toggled.connect(lambda b: self.setXpdrMode(b, 'C'))
		self.xpdrMode_select_S.toggled.connect(lambda b: self.setXpdrMode(b, 'S'))
		self.xpdrCode_select.codeChanged.connect(self.setXpdrCode)
		self.pushSQtoStrip_button.clicked.connect(self.SQcodeToStrip)
		self.xpdrIdent_tickBox.toggled.connect(self.toggleXpdrIdent)
		self.squawkVFR_button.clicked.connect(lambda: self.xpdrCode_select.setSQ(settings.uncontrolled_VFR_XPDR_code))
		self.freezeACFT_button.toggled.connect(self.freezeSelectedACFT)
		self.spawn_button.clicked.connect(self.spawnSelectedACFT)
		self.kill_button.clicked.connect(self.killSelectedACFT)
		# ATC neighbours tab
		self.addATC_button.clicked.connect(self.addATC)
		self.removeATC_button.clicked.connect(self.removeATC)
		self.ATC_select.editTextChanged.connect(self.updateNeighboursTab)
		self.frequency_tickBox.toggled.connect(self.setATCfrq)
		self.frequency_select.frequencyChanged.connect(self.setATCfrq)
		# Wind tab
		self.setWind_button.clicked.connect(self.setWind)
		self.windHdg_dial.valueChanged.connect(lambda: self.windHdg_radioButton.setText('%s°' % self._windDialHdg().readTrue()))
		self.windSpeed_edit.valueChanged.connect(lambda spd: self.windGusts_edit.setMinimum(spd + gust_diff_threshold))
		# Snapshots tab
		self.snapshotSituation_button.clicked.connect(self.snapshotSituation)
		self.restoreSituation_button.clicked.connect(self.restoreSituation)
		self.removeSituation_button.clicked.connect(self.removeSituation)
		self.situationSnapshots_tableView.selectionModel().selectionChanged.connect(self.updateSnapshotsTab)
		# Misc.
		self.pauseSim_button.toggled.connect(self.togglePause)
		signals.localSettingsChanged.connect(self.xpdrCode_select.updateXPDRranges)
		signals.gameStarted.connect(self.gameHasStarted)
		signals.gameStopped.connect(self.gameHasStopped)
		self.last_game_type = None
	
	def _windDialHdg(self):
		return Heading(5 * self.windHdg_dial.value(), True)
	
	def gameHasStarted(self):
		self.last_game_type = settings.game_manager.game_type
		if self.last_game_type == GameType.TEACHER:
			self.setEnabled(True)
			signals.selectionChanged.connect(self.updateTrafficTab)
			self.updateTrafficTab()
			self.updateNeighboursTab()
			self.setWind() # initialises the weather for the session
			self.updateSnapshotsTab()
	
	def gameHasStopped(self):
		if self.last_game_type == GameType.TEACHER:
			signals.selectionChanged.disconnect(self.updateTrafficTab)
			self.ATC_neighbours.clear()
			self.setEnabled(False)
	
	def togglePause(self, toggle):
		if toggle:
			settings.game_manager.pauseGame()
		else:
			settings.game_manager.resumeGame()
	
	def updateTrafficTab(self):
		ai_acft = selection.acft
		if ai_acft == None:
			self.selectedTraffic_tab.setEnabled(False)
		else:
			self.selectedCallsign_info.setText(ai_acft.identifier if ai_acft._spawned else '%s (unspawned)' % ai_acft.identifier)
			self.spawn_button.setEnabled(not ai_acft._spawned)
			self.freezeACFT_button.setChecked(ai_acft.frozen)
			self.selectedTraffic_tab.setEnabled(True)
			radio_button = {
				'0': self.xpdrMode_select_0, 'G': self.xpdrMode_select_G, 'A': self.xpdrMode_select_A,
				'C': self.xpdrMode_select_C, 'S': self.xpdrMode_select_S
			}[ai_acft.params.XPDR_mode]
			radio_button.setChecked(True)
			self.xpdrCode_select.setSQ(ai_acft.params.XPDR_code)
			self.xpdrIdent_tickBox.setChecked(ai_acft.params.XPDR_idents)
	
	def updateNeighboursTab(self):
		atc = self.ATC_select.currentText()
		name_blocked = atc in ['', teacher_callsign, student_callsign] + [acft.identifier for acft in settings.game_manager.getAircraft()]
		got_atc = atc in env.ATCs.knownATCs()
		self.addATC_button.setEnabled(not name_blocked and not got_atc)
		self.removeATC_button.setEnabled(not name_blocked and got_atc)
		if atc in self.ATC_neighbours:
			self.frequency_tickBox.setChecked(self.ATC_neighbours[atc] != None)
			if self.frequency_tickBox.isChecked():
				self.frequency_select.setEditText(some(self.ATC_neighbours[atc], '')) # STYLE setFrequency method in picker, where name is appended
	
	def updateSnapshotsTab(self):
		one_selected = len(self.situationSnapshots_tableView.selectionModel().selectedRows()) == 1
		self.restoreSituation_button.setEnabled(one_selected)
		self.removeSituation_button.setEnabled(one_selected)
	
	
	## TRAFFIC TAB
	
	def setXpdrMode(self, toggle, mode):
		if toggle:
			ai_acft = selection.acft
			if ai_acft != None:
				ai_acft.params.XPDR_mode = mode
	
	def SQcodeToStrip(self):
		strip = selection.strip
		if strip != None:
			strip.writeDetail(assigned_SQ_detail, self.xpdrCode_select.getSQ())
			signals.stripInfoChanged.emit()
	
	def setXpdrCode(self):
		ai_acft = selection.acft
		if ai_acft != None:
			ai_acft.params.XPDR_code = self.xpdrCode_select.getSQ()
	
	def toggleXpdrIdent(self, toggle):
		ai_acft = selection.acft
		if ai_acft != None:
			ai_acft.params.XPDR_idents = toggle
	
	def freezeSelectedACFT(self, toggle):
		ai_acft = selection.acft
		if ai_acft != None:
			ai_acft.frozen = toggle
		self.updateTrafficTab()
	
	def spawnSelectedACFT(self):
		ai_acft = selection.acft
		if ai_acft != None:
			ai_acft._spawned = True
		self.updateTrafficTab()
	
	def killSelectedACFT(self):
		acft = selection.acft
		if acft != None:
			selection.deselect()
			settings.game_manager.killAircraft(acft)
			env.radar.scan()
			self.updateTrafficTab()
	
	
	## ATC NEIGHBOURS TAB

	def addATC(self):
		atc = self.ATC_select.currentText()
		frq = self.frequency_select.getFrequency() if self.frequency_tickBox.isChecked() else None
		self.ATC_neighbours[atc] = frq
		self.ATC_select.addItem(atc)
		env.ATCs.updateATC(atc, False, None, None, frq)
		settings.game_manager.sendATCs() # updates distant student list
		self.updateNeighboursTab()
	
	def removeATC(self):
		atc = self.ATC_select.currentText()
		try:
			self.ATC_neighbours.pop(atc)
			env.ATCs.removeATC(atc)
			index = next(i for i in range(self.ATC_select.count()) if self.ATC_select.itemText(i) == atc)
		except (StopIteration, KeyError):
			print('ATC lists out of sync: %s not found' % atc)
		else:
			self.ATC_select.removeItem(index)
			self.ATC_select.setEditText(atc)
		settings.game_manager.sendATCs() # updates distant student list
		self.updateNeighboursTab()
	
	def setATCfrq(self):
		atc = self.ATC_select.currentText()
		if atc in self.ATC_neighbours:
			self.ATC_neighbours[atc] = self.frequency_select.getFrequency() if self.frequency_tickBox.isChecked() else None
			env.ATCs.updateATC(atc, False, None, None, self.ATC_neighbours[atc])
			settings.game_manager.sendATCs() # updates distant student list
	
	
	## WEATHER TAB
	
	def setWind(self):
		if self.windCalm_radioButton.isChecked():
			wind_str = '00000KT'
		else:
			wind_str = 'VRB' if self.windVRB_radioButton.isChecked() else self._windDialHdg().readTrue() # main dir chars
			wind_str += '%02d' % self.windSpeed_edit.value() # main speed chars
			if self.windGusts_edit.isEnabled():
				wind_str += 'G%02d' % self.windGusts_edit.value()
			wind_str += 'KT'
			if self.windHdgRange_edit.isEnabled():
				w = self._windDialHdg().trueAngle()
				v = self.windHdgRange_edit.value()
				wind_str += ' %sV%s' % (Heading(w - v, True).readTrue(), Heading(w + v, True).readTrue())
		settings.game_manager.setWeatherFromMetar(generateMETAR_clearWeather(settings.primary_METAR_station, wind=wind_str))
	
	
	## TRAFFIC TAB
	
	def snapshotSituation(self):
		self.situationSnapshots_tableModel.addSnapshot(settings.game_manager.situationSnapshot())
	
	def restoreSituation(self):
		try:
			index = self.situationSnapshots_tableView.selectedIndexes()[0]
		except IndexError:
			print('No situation selected to restore.')
		else:
			snapshot = self.situationSnapshots_tableModel.situationOnRow(index.row())
			settings.game_manager.restoreSituation(snapshot)
			env.radar.scan()
	
	def removeSituation(self):
		try:
			index = self.situationSnapshots_tableView.selectedIndexes()[0]
		except IndexError:
			print('No situation selected to remove.')
		else:
			if yesNo_question(self, 'Remove situation', 'This will make selected situation unavailable.', 'Are you sure?'):
				self.situationSnapshots_tableModel.removeSnapshot(index.row())
	
