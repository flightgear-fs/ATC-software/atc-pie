
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from os import path
from socket import socket, AF_INET, SOCK_DGRAM

from PyQt5.QtWidgets import QWidget, QDialog, QInputDialog, QMessageBox
from PyQt5.QtCore import QProcess, pyqtSignal, QFileInfo

from ui.fgcom import Ui_fgcomPane
from ui.radiobox import Ui_radioBox
from ui.fgcomEchoTestDialog import Ui_fgcomEchoTestDialog
from ui.recordAtisDialog import Ui_recordAtisDialog

from util import some
from settings import settings
from game.env import env
from game.manager import GameType
from GUI.misc import Ticker, signals




# ---------- Constants ----------

FGCom_exe_dir = 'resources/fgcom'

soft_sound_level = .25
loud_sound_level = 1

encoding = 'utf-8'
fgcom_controller_ticker_interval = 500 # in seconds

frequencies_always_proposed = [('123.500', 'Unicom'), ('121.500', 'EMG'), ('122.75', 'FG A/A 1'), ('123.450', 'FG A/A 2')]

# -------------------------------



def FGCom_callsign():
	cs = settings.game_manager.myCallsign()
	if settings.game_manager.game_type in [GameType.TEACHER, GameType.STUDENT]:
		cs += '_' + settings.location_code
	return cs



class InternalFgcomInstance(QProcess):
	def __init__(self, port, cmdopts, parent):
		QProcess.__init__(self, parent)
		cmdopts.append('--server=%s' % settings.fgcom_server)
		cmdopts.append('--port=%d' % port)
		cmdopts.append('--callsign=%s' % FGCom_callsign())
		full_exe_path = QFileInfo(path.join(FGCom_exe_dir, settings.fgcom_exe_path)).absoluteFilePath()
		wdir, prgm = path.split(full_exe_path)
		self.setWorkingDirectory(wdir)
		self.setProgram(full_exe_path)
		self.setArguments(cmdopts)
		self.setStandardErrorFile(settings.outputFileName('fgcom-stderr-port%d' % port, ext='log'))
		#print('FGCom command: %s %s' % (full_exe_path, ' '.join(cmdopts)))
	


class FgcomSettings:
	def __init__(self, socket, address):
		self.socket = socket
		self.address = address
		try:
			self.frq = env.frequencies[0][0]
		except KeyError:
			self.frq = frequencies_always_proposed[0][0]
		self.ptt = False  # "push to talk"
		self.vol = 1      # output volume
		pos = env.radarPos()
		# packet format has 3 slots to fill: PTT, frq, vol
		self.packet_format = 'PTT=%d'
		self.packet_format += ',LAT=%f,LON=%f,ALT=%f' % (pos.lat, pos.lon, env.elevation(pos))
		self.packet_format += ',COM1_FRQ=%s,COM2_FRQ=121.850'
		self.packet_format += ',OUTPUT_VOL=%f,SILENCE_THD=-60'
		self.packet_format += ',CALLSIGN=%s' % FGCom_callsign()

	def send(self):
		packet_str = self.packet_format % (self.ptt, self.frq, self.vol)
		self.socket.sendto(bytes(packet_str, encoding), self.address)




class RadioBox(QWidget, Ui_radioBox):
	def __init__(self, parent, external, port):
		'''
		external is a host (possibly localhost) for external FGCom instance, or None for internal (child process)
		'''
		QWidget.__init__(self, parent)
		self.setupUi(self)
		client_address = some(external, 'localhost'), port
		self.settings = FgcomSettings(socket(AF_INET, SOCK_DGRAM), client_address)
		self.controller = Ticker(self.settings.send, parent=self)
		self.frequency_combo.addFrequencies([(frq, descr) for frq, descr, t in env.frequencies])
		self.frequency_combo.addFrequencies(frequencies_always_proposed)
		if external == None: # child process
			self.onOff_button.setToolTip('Internal FGCom instance using local port %d' % port)
			ad = env.CTR_closest_airport.code if env.airport_data == None else settings.location_code
			self.instance = InternalFgcomInstance(port, ['--airport=%s' % ad], self)
			self.instance.started.connect(self.processHasStarted)
			self.instance.finished.connect(self.processHasStopped)
			self.onOff_button.toggled.connect(self.switchFGCom)
		else:
			self.instance = None
			self.onOff_button.setToolTip('External FGCom instance on %s:%d' % client_address)
			self.onOff_button.setText('Ext.')
			self.onOff_button.setChecked(True)
			self.onOff_button.setEnabled(False)
			self.PTT_button.setEnabled(True)
			self.controller.start(fgcom_controller_ticker_interval)
		self.PTT_button.pressed.connect(lambda: self.PTT(True))
		self.PTT_button.released.connect(lambda: self.PTT(False))
		self.softVolume_tickBox.clicked.connect(self.setVolume)
		self.frequency_combo.frequencyChanged.connect(self.setFrequency)
	
	def isInternal(self):
		return self.instance != None
	
	def clientAddress(self):
		return self.settings.address
	
	def getReadyForRemoval(self):
		if self.isInternal():
			self.switchFGCom(False)
			self.instance.waitForFinished(1000)
		else:
			self.controller.stop()
	
	def setVolume(self):
		if settings.mute_radios:
			self.settings.vol = 0
		elif self.softVolume_tickBox.isChecked():
			self.settings.vol = soft_sound_level
		else:
			self.settings.vol = loud_sound_level
		self.settings.send()
		
	def setFrequency(self):
		self.settings.frq = self.frequency_combo.getFrequency()
		self.settings.send()
	
	def PTT(self, toggle):
		self.settings.ptt = toggle
		self.PTT_button.setChecked(toggle and self.PTT_button.isEnabled())
		signals.radioPTT.emit('%s:%d' % self.clientAddress(), toggle)
		self.settings.send()
	
	
	## Controlling FGCom process
	
	def switchFGCom(self, on_off):
		if on_off: # Start FGCom
			self.instance.start()
		else: # Stop FGCom
			self.instance.kill()
	
	def processHasStarted(self):
		self.PTT_button.setEnabled(True)
		self.controller.start(fgcom_controller_ticker_interval)
	
	def processHasStopped(self, exit_code, status):
		self.controller.stop()
		self.PTT_button.setEnabled(False)
		self.PTT_button.setChecked(False)
		self.onOff_button.setChecked(False)
	








class EchoTestDialog(QDialog, Ui_fgcomEchoTestDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.instance = InternalFgcomInstance(settings.reserved_fgcom_port, ['--frequency=910.000'], self)
		self.OK_button.clicked.connect(self.closeMe)
		self.instance.started.connect(self.processHasStarted)
		self.instance.finished.connect(self.processHasStopped)
		self.instance.start()
		#print('Executed: %s %s' % (self.instance.program(), ' '.join(self.instance.arguments())))
	
	def processHasStopped(self):
		self.label.setText('FGCom has stopped.')
	
	def processHasStarted(self):
		self.label.setText('Hearing echo?')
	
	def closeMe(self):
		if self.instance.state() == QProcess.Running:
			self.instance.kill()
		self.accept()








class RecordAtisDialog(QDialog, Ui_recordAtisDialog):
	#STATIC:
	last_recorded = None
	
	def __init__(self, frequency, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		ad = env.CTR_closest_airport.code if env.airport_data == None else settings.location_code
		cmdopts = ['--airport=%s' % ad, '--atis=%s' % frequency]
		self.recorded = False
		if RecordAtisDialog.last_recorded == None:
			info_letter = 'A'
		else:
			info_letter = chr((ord(RecordAtisDialog.last_recorded) - ord('A') + 1) % 26 + ord('A'))
		self.infoLetter_edit.setText(info_letter)
		self.notepad_textEdit.setPlainText(env.suggestedATIS(info_letter))
		self.instance = InternalFgcomInstance(settings.reserved_fgcom_port, cmdopts, self)
		self.record_button.clicked.connect(self.startRecording)
		self.close_button.clicked.connect(self.closeMe)
		self.instance.started.connect(self.processHasStarted)
		self.instance.finished.connect(self.processHasStopped)
	
	def startRecording(self):
		self.status_infoLabel.setText('Starting FGCom instance...')
		self.instance.start()
		#print('Executed: %s %s' % (self.instance.program(), ' '.join(self.instance.arguments())))
	
	def processHasStopped(self):
		self.status_infoLabel.setText('Process has stopped.')
	
	def processHasStarted(self):
		self.recorded = True
		self.record_button.setEnabled(False)
		self.status_infoLabel.setText('Speak after beep...')
	
	def closeMe(self):
		if self.instance.state() == QProcess.Running:
			self.instance.kill()
		if self.recorded:
			RecordAtisDialog.last_recorded = self.infoLetter_edit.text().upper()
			self.accept()
		else:
			self.reject()
	






class FgcomPane(QWidget, Ui_fgcomPane):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.setEnabled(False)
		self.available_internal_ports = set()
		self.last_FGCom_external_host = 'localhost'
		self.ATISfreq_combo.addFrequencies([(f, d) for f, d, t in env.frequencies if t == 'recorded'])
		self.addBox_button.clicked.connect(self.addRadioBox_internalFGCom)
		self.addExtBox_button.clicked.connect(self.addRadioBox_externalFGCom)
		self.recordATIS_button.clicked.connect(self.recordATIS)
		self.muteAllRadios_tickBox.toggled.connect(self.toggleMuteAll)
		signals.kbdPTT.connect(self.generalKeyboardPTT)
		signals.mainWindowClosing.connect(self.removeAllBoxes)
		signals.gameStarted.connect(self.gameHasStarted)
		signals.gameStopped.connect(self.gameHasStopped)
	
	def gameHasStarted(self):
		if settings.game_manager.game_type != GameType.SOLO:
			self.available_internal_ports = set(settings.radio_fgcom_ports)
			self.setEnabled(True)
	
	def gameHasStopped(self):
		self.removeAllBoxes()
		self.setEnabled(False)
	
	def radio(self, index):
		return self.radios_table.cellWidget(index, 0)
	
	def addRadioBox_externalFGCom(self):
		host, ok = QInputDialog.getText(self, 'External FGCom radio', 'Client host:', text=self.last_FGCom_external_host)
		if not ok:
			return
		port, ok = QInputDialog.getInt(self, 'External FGCom radio', 'Client port:', value=16661, min=1, max=65535)
		if ok:
			if (host, port) in [self.radio(i).clientAddress() for i in range(self.radios_table.rowCount())] \
					or host == 'localhost' and (port in settings.radio_fgcom_ports or port == settings.reserved_fgcom_port):
				QMessageBox.critical(self, 'Radio box error', 'Used or reserved address.')
			else:
				self.last_FGCom_external_host = host
				self.addRadioBox(RadioBox(self, host, port))
	
	def addRadioBox_internalFGCom(self):
		try:
			port = self.available_internal_ports.pop()
		except KeyError:
			QMessageBox.critical(self, 'Radio box error', \
				'No more ports available for a new FGCom instance.\nConsider adding ports or use an external client.')
		else:
			self.addRadioBox(RadioBox(self, None, port))
		
	def addRadioBox(self, radiobox):
		index = self.radios_table.rowCount()
		radiobox.kbdPTT_checkBox.setChecked(index == 0)
		radiobox.removeBox_button.clicked.connect(lambda: self.removeRadioBox(address=radiobox.clientAddress()))
		self.radios_table.insertRow(index)
		self.radios_table.setCellWidget(index, 0, radiobox)
		self.radios_table.scrollToBottom()
		self.radios_table.resizeColumnToContents(0)
		self.radios_table.resizeRowToContents(index)
	
	def removeRadioBox(self, address=None, index=None):
		if index == None:
			index = next(i for i in range(self.radios_table.rowCount()) if self.radio(i).clientAddress() == address)
		else:
			address = self.radio(index).clientAddress()
		box = self.radio(index)
		port = address[1] if box.isInternal() else None
		box.getReadyForRemoval()
		self.radios_table.removeRow(index)
		if port != None:
			self.available_internal_ports.add(port)
	
	def generalKeyboardPTT(self, key_number, toggle): # only one key for now
		for i in range(self.radios_table.rowCount()):
			box = self.radio(i)
			if box.kbdPTT_checkBox.isChecked():
				box.PTT(toggle)
	
	def toggleMuteAll(self, toggle):
		settings.mute_radios = toggle
		for i in range(self.radios_table.rowCount()):
			self.radio(i).setVolume()
		
	def performEchoTest(self):
		EchoTestDialog(self).exec()
		
	def recordATIS(self):
		RecordAtisDialog(self.ATISfreq_combo.getFrequency(), self).exec()
	
	def removeAllBoxes(self):
		while self.radios_table.rowCount() > 0: # recovers all ports 
			self.removeRadioBox(index=0)
	
