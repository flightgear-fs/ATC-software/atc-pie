
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtCore import Qt, QRectF, QPointF, QLineF
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsPixmapItem, QGraphicsItemGroup, QGraphicsRectItem
from PyQt5.QtGui import QPen, QBrush, QConicalGradient, QPixmap, QVector2D, QTransform, QPolygonF, QPainterPath

from math import radians, tan, atan2
from settings import settings
from game.env import env
from data.coords import m2NM, m2ft
from data.strip import runway_box_detail

from GUI.misc import signals
from GUI.graphics.miscGraphics import new_pen, withMargins, MouseOverLabelledItem


# ---------- Constants ----------

slope_tick_amsl_step = 1000 # ft
slope_tick_spacing = .06 # NM
LOC_line_RWY_sep_dist = .1 # NM
tower_pixmap = 'resources/pixmap/control-TWR.png'
tower_width = .03
windsock_pixmap = 'resources/pixmap/windsock.png'
windsock_width = .03
TWY_line_margin = .0125

# -------------------------------



# ================================================ #

#                     RUNWAYS                      #

# ================================================ #


runway_border_colours = {
	0: 'runway',
	1: 'runway_reserved',
	2: 'runway_incursion'
}


class RunwayItem(QGraphicsItem):
	'''
	Draws a bidir runway from a base runway, with both RWY numbers at threshold
	'''
	def __init__(self, ad_data, physical_index):
		QGraphicsItem.__init__(self, parent=None)
		self.physical_location = ad_data.navpoint.code
		self.physical_runway_index = physical_index
		self.base_rwy, self.opposite_runway = ad_data.physicalRunway(physical_index)
		width_metres, self.paved = ad_data.physicalRunwayData(physical_index)
		self.width = m2NM * width_metres
		self.occupation_indication = 0
		thr = self.base_rwy.threshold().toRadarCoords()
		self.length = thr.distanceTo(self.opposite_runway.threshold().toRadarCoords())
		item_rot = self.base_rwy.orientation().trueAngle()
		self.label1 = RunwayNameItem(self.base_rwy.name, item_rot, self)
		self.label2 = RunwayNameItem(self.opposite_runway.name, item_rot + 180, self)
		self.label1.setPos(0, 0)
		self.label2.setPos(0, -self.length)
		self.LOC_item1 = LocaliserItem(self.base_rwy, self) # CAUTION: accessed from outside of class
		self.LOC_item2 = LocaliserItem(self.opposite_runway, self) # CAUTION: accessed from outside of class
		self.LOC_item2.setPos(0, -self.length)
		self.LOC_item2.setRotation(180)
		self.rect = QRectF(-self.width / 2, -self.length, self.width, self.length)
		self.setPos(thr.toQPointF())
		self.setRotation(item_rot)
		self.setAcceptHoverEvents(True)
	
	def updateItem(self):
		self.occupation_indication = 0
		if self.physical_location == settings.location_code:
			try:
				boxed_strip = env.strips.findStrip(lambda strip: strip.lookup(runway_box_detail) == self.physical_runway_index)
				self.occupation_indication = 1
			except StopIteration: # no strip boxed on this runway
				boxed_strip = None
			if settings.monitor_runway_occupation:
				occ = env.radar.runwayOccupation(self.physical_runway_index)
				if len(occ) >= 2 or len(occ) == 1 and boxed_strip == None:
					self.occupation_indication = 2
				elif len(occ) == 1: # one traffic on RWY and runway is reserved
					acft = occ[0]
					boxed_link = boxed_strip.linkedAircraft() # NB: can be None
					if not (boxed_link == None and env.linkedStrip(acft) == None or boxed_link is acft):
						self.occupation_indication = 2
		self.setZValue(self.occupation_indication)
		self.update(self.rect)
	
	def updateRwyNamesVisibility(self):
		forced = self.scene().runway_names_always_visible
		self.label1.setVisible(forced or self.base_rwy.inUse())
		self.label2.setVisible(forced or self.opposite_runway.inUse())
	
	def localiserItemForRWY(self, rwy):
		if self.base_rwy.name == rwy:
			return self.LOC_item1
		elif self.opposite_runway.name == rwy:
			return self.LOC_item2
		else:
			return None
	
	def boundingRect(self):
		return self.rect

	def paint(self, painter, option, widget):
		# 0,0 is threshold of first DirRunway
		col = runway_border_colours[self.occupation_indication]
		w = 0 if self.occupation_indication == 0 else 3
		painter.setPen(new_pen(settings.colour(col), width=w))
		if self.paved:
			painter.setBrush(QBrush(settings.colour('runway')))
		painter.drawRect(self.rect)
	
	## MOUSE HOVER
	
	def hoverEnterEvent(self, event):
		self.label1.setVisible(True)
		self.label2.setVisible(True)
	
	def hoverLeaveEvent(self, event):
		self.updateRwyNamesVisibility()





class HelipadItem(QGraphicsItem):
	def __init__(self, helipad):
		QGraphicsItem.__init__(self, parent=None)
		self.helipad = helipad
		self.l = m2NM * self.helipad.length
		self.w = m2NM * self.helipad.width
		self.label = RunwayNameItem(self.helipad.name, self.helipad.orientation.trueAngle(), self)
		self.rect = QRectF(-self.w / 2, -self.l / 2, self.w, self.l)
		self.setRotation(self.helipad.orientation.trueAngle())
		self.setPos(self.helipad.centre.toQPointF())
		self.label.setPos(0, self.l / 2)
		self.setAcceptHoverEvents(True)
	
	def boundingRect(self):
		return self.rect

	def paint(self, painter, option, widget):
		painter.setPen(new_pen(settings.colour('runway')))
		if self.helipad.paved:
			painter.setBrush(QBrush(settings.colour('runway')))
		painter.drawRect(self.rect)
		painter.drawText(self.rect, Qt.AlignCenter, self.helipad.name)
	
	def updateRwyNamesVisibility(self):
		self.label.setVisible(self.scene().runway_names_always_visible)
	
	## MOUSE HOVER
	
	def hoverEnterEvent(self, event):
		self.label.setVisible(True)
	
	def hoverLeaveEvent(self, event):
		self.updateRwyNamesVisibility()






class RunwayNameItem(QGraphicsItem):
	# also used to label helipads
	# 0,0 is middle of top of text
	def __init__(self, text, orientation, parentItem):
		QGraphicsItem.__init__(self, parentItem)
		self.setFlag(QGraphicsItem.ItemIgnoresTransformations, True)
		self.setRotation(orientation)
		self.text = text
	
	def boundingRect(self):
		return QRectF(-15, 0, 30, 15)

	def paint(self, painter, option, widget):
		painter.setPen(new_pen(settings.colour('runway')))
		painter.drawText(QRectF(-15, 0, 30, 15), Qt.AlignHCenter | Qt.AlignTop, self.text)





class LocaliserItem(QGraphicsItem):
	'''
	Draws the final approach helper (centre line, intercepting cone, GS altitude marks)
	'''
	def __init__(self, runway, parentItem):
		QGraphicsItem.__init__(self, parentItem)
		self.runway = runway
		self.updateFromSettings() # CAUTION: creates a few attributes
		self.setVisible(False)
	
	def updateFromSettings(self):
		self.prepareGeometryChange()
		self.slope_fact = m2ft * self.runway.param_FPA / 100 / m2NM
		self.dthr_NM = m2NM * self.runway.dthr
		self.dthr_elev = env.elevation(self.runway.threshold(dthr=True))
		tick_min_alt = self.dthr_elev + self.slope_fact * (self.dthr_NM + 2 * LOC_line_RWY_sep_dist)
		self.GS_init_step = int(tick_min_alt / slope_tick_amsl_step) + 1
		self.GS_init_dist = (self.GS_init_step * slope_tick_amsl_step - self.dthr_elev) / self.slope_fact
		self.GS_step_dist = slope_tick_amsl_step / self.slope_fact
		self.LOC_range = self.runway.param_LOCrange
		self.LOC_angle = self.runway.param_LOCangle
	
	def boundingRect(self):
		hw = max(5, self.coneHalfWidth())
		return QRectF(-hw, -1, 2 * hw, self.LOC_range + 2)
	
	def paint(self, painter, option, widget):
		# 0,0 is top of line at RWY THR and drawing down
		paint_colour = settings.colour('LDG_helper_ILS' if self.runway.param_ILS else 'LDG_helper_noILS')
		painter.setPen(new_pen(paint_colour))
		painter.setBrush(QBrush(paint_colour))
		## Centre line
		painter.drawLine(QPointF(0, LOC_line_RWY_sep_dist), QPointF(0, self.LOC_range))
		## GS altitude marks
		if self.scene().show_slope_altitudes:
			alt_step = self.GS_init_step
			dthr_dist = self.GS_init_dist
			while dthr_dist <= self.LOC_range:
				y = dthr_dist - self.dthr_NM
				for i in range(alt_step % 5):
					painter.drawLine(QPointF(-.12, y), QPointF(.12, y))
					y += slope_tick_spacing
				hw = .6 * slope_tick_spacing # slope diamond half width
				for i in range(alt_step // 5):
					painter.drawPolygon(QPointF(-.15, y), QPointF(0, y - hw), QPointF(.15, y), QPointF(0, y + hw))
					y += slope_tick_spacing
				alt_step += 1
				dthr_dist += self.GS_step_dist
		## Interception cone
		if self.scene().show_interception_cones:
			cone_gradient = QConicalGradient(QPointF(0, 0), 270 - 2 * self.LOC_angle)
			r = self.LOC_angle / 180
			cone_gradient.setColorAt(0, paint_colour)
			cone_gradient.setColorAt(.9 * r, Qt.transparent)
			cone_gradient.setColorAt(1.1 * r, Qt.transparent)
			cone_gradient.setColorAt(2 * r, paint_colour)
			cone_brush = QBrush(cone_gradient)
			painter.setPen(Qt.NoPen)
			painter.setBrush(cone_brush)
			p1 = QPointF(-self.coneHalfWidth(), self.LOC_range)
			p2 = QPointF(self.coneHalfWidth(), self.LOC_range)
			painter.drawPolygon(p1, QPointF(0, 0), p2)
	
	def coneHalfWidth(self):
		return self.LOC_range * tan(radians(self.LOC_angle))

















# ================================================ #

#         GROUND NETS, PARKING, TAXIWAYS           #

# ================================================ #


class TarmacSectionItem(QGraphicsItem):
	'''
	Airport movement or non movement area: taxiway, ramp, etc. whether paved or unpaved.
	'''
	def __init__(self, name, paved, path, parent=None):
		QGraphicsItem.__init__(self, parent)
		self.name = name
		self.paved = paved
		self.path = path
	
	def boundingRect(self):
		return self.path.boundingRect()

	def paint(self, painter, option, widget):
		painter.setPen(new_pen(settings.colour('airport_tarmac')))
		painter.setBrush(QBrush(settings.colour('airport_tarmac')) if self.paved else QBrush())
		painter.drawPath(self.path)





class ParkingPositionItem(QGraphicsItem):
	def __init__(self, name, hdg):
		QGraphicsItem.__init__(self, parent=None)
		self.name = name
		self.setRotation(hdg.trueAngle())
		self.setAcceptedMouseButtons(Qt.LeftButton)
	
	def boundingRect(self):
		return QRectF(-.02, -.02, .04, .04)

	def paint(self, painter, option, widget):
		painter.setPen(new_pen(settings.colour('parking_position')))
		painter.drawLine(QPointF(-.01, -.01), QPointF(.01, -.01))
		painter.drawLine(QPointF(0, -.01), QPointF(0, .015))
	
	def mousePressEvent(self, event):
		if event.button() == Qt.LeftButton:
			signals.pkPosClick.emit(self.name)
		QGraphicsItem.mousePressEvent(self, event)








class AirportGroundNetItem(QGraphicsItem):
	def __init__(self, gndnet):
		QGraphicsItem.__init__(self, parent=None)
		self.setAcceptedMouseButtons(Qt.NoButton)
		q = lambda n: gndnet.nodePosition(n).toQPointF() # node ID to QPointF
		edges = [(q(n1), q(n2)) for n1, n2 in gndnet.apronEdges()]
		self.apron_item = TaxiRouteItem(self, edges, None)
		self.twy_items = []
		for twy in gndnet.taxiways():
			edges = [(q(n1), q(n2)) for n1, n2 in gndnet.taxiwayEdges(twy)]
			edges.sort(key=(lambda p1p2: QVector2D(p1p2[1] - p1p2[0]).length()), reverse=True)
			# Create a hoverable TWY item
			twy_item = TaxiRouteItem(self, edges, twy)
			self.twy_items.append(twy_item)
		self.apron_item.prepareGeometryChange()
		for item in self.twy_items:
			item.prepareGeometryChange()
	
	def boundingRect(self):
		return QRectF()
	
	def paint(self, painter, option, widget):
		pass
	
	def updateLabelsVisibility(self):
		for item in self.twy_items:
			item.labels.setVisible(self.scene().show_taxiway_names)
	






class TaxiRouteItem(QGraphicsItem):
	'''
	Draws all edges for a given TWY (by name) or apron.
	Call prepareGeometryChange after building to initialise correctly.
	'''
	def __init__(self, parentItem, segments, twy=None):
		QGraphicsItem.__init__(self, parent=parentItem)
		self.name = twy # None for apron; any string for TWY name
		self.colour_name = 'ground_route_apron' if self.name == None else 'ground_route_taxiway'
		self.shape = QPainterPath()
		self.labels = QGraphicsItemGroup(self)
		self.bbox = QRectF(0, 0, 0, 0)
		for p1, p2 in segments:
			lvect = QVector2D(p2 - p1)
			lpath = QPainterPath()
			m = TWY_line_margin
			l = lvect.length()
			plst = [QPointF(-m, 0), QPointF(-m/3, -m), QPointF(l + m/3, -m), QPointF(l + m, 0), QPointF(l + m/3, m), QPointF(-m/3, m)]
			lpath.addPolygon(QPolygonF(plst))
			lrot = QTransform()
			lrot.rotateRadians(atan2(lvect.y(), lvect.x()))
			lpath = lrot.map(lpath)
			lpath.translate(p1)
			self.shape.addPath(lpath)
			rect = QRectF(p1, p2).normalized()
			if self.name != None:
				self.labels.addToGroup(TaxiwayLabelItem(self.name, rect.center(), self))
			self.bbox |= rect
		self.shape.setFillRule(Qt.WindingFill)
		self.mouse_highlight = False
		self.setAcceptHoverEvents(self.name != None)
		self.labels.setVisible(False)
	
	def hoverEnterEvent(self, event):
		self.mouse_highlight = self.scene().mouseover_highlights_groundnet_edges
		if self.mouse_highlight and not self.labels.isVisible():
			self.labels.setVisible(True)
		self.setVisible(False)
		self.setVisible(True)
	
	def hoverLeaveEvent(self, event):
		self.mouse_highlight = False
		self.labels.setVisible(self.scene().show_taxiway_names)
		self.setVisible(False)
		self.setVisible(True)
	
	def boundingRect(self):
		return withMargins(self.bbox, TWY_line_margin)
	
	def shape(self):
		return self.shape

	def paint(self, painter, option, widget):
		if self.mouse_highlight or self.scene().show_ground_networks:
			painter.setPen(QPen(Qt.NoPen))
			brushcol = settings.colour(self.colour_name)
			brushcol.setAlpha(64 if self.mouse_highlight else 96)
			painter.setBrush(QBrush(brushcol))
			painter.drawPath(self.shape)





class TaxiwayLabelItem(QGraphicsItem):
	rect = QRectF(-15, -5, 30, 10)
	text_offset = QPointF(0, -.001)
	
	def __init__(self, text, position, parentItem):
		QGraphicsItem.__init__(self, parentItem)
		self.setFlag(QGraphicsItem.ItemIgnoresTransformations, True)
		self.setPos(position + TaxiwayLabelItem.text_offset)
		self.text = text
	
	def boundingRect(self):
		return TaxiwayLabelItem.rect

	def paint(self, painter, option, widget):
		painter.setPen(new_pen(settings.colour('ground_route_taxiway' if self.text != '' else 'ground_route_apron')))
		painter.drawText(TaxiwayLabelItem.rect, Qt.AlignCenter, self.text)









# =============================================== #

#                      OTHER                      #

# =============================================== #



class AirportLinearObjectItem(QGraphicsItem):
	'''
	Draws the outline of the airfield if given in the source data file
	'''
	def __init__(self, path, colour_name):
		QGraphicsItem.__init__(self, parent=None)
		self.setAcceptedMouseButtons(Qt.NoButton)
		self.path = path
		self.colour_name = colour_name
	
	def boundingRect(self):
		return QRectF() if self.path == None else self.path.boundingRect()

	def paint(self, painter, option, widget):
		if self.path != None:
			painter.setPen(new_pen(settings.colour(self.colour_name)))
			painter.drawPath(self.path)



class WindsockItem(QGraphicsPixmapItem):
	'''
	Places and sizes a windsock pixmap.
	'''
	def __init__(self, coords):
		QGraphicsPixmapItem.__init__(self, QPixmap(windsock_pixmap), None)
		rect = self.boundingRect()
		self.setScale(windsock_width / rect.width())
		self.setOffset(-rect.bottomRight())
		self.setPos(coords.toQPointF())
	



class TowerItem(QGraphicsPixmapItem):
	'''
	Resized control tower pixmap item.
	'''
	def __init__(self, viewpoint_name):
		QGraphicsPixmapItem.__init__(self, QPixmap(tower_pixmap), parent=None)
		rect = self.boundingRect()
		self.setScale(tower_width / rect.width())
		self.setOffset(-rect.width() / 2, -rect.height())
		#lbl_item = QGraphicsRectItem(-tower_width / 2, -tower_width, tower_width, tower_width)
		lbl_item = QGraphicsRectItem(-rect.width() / 2, -rect.width(), rect.width(), rect.width())
		MouseOverLabelledItem(lbl_item, viewpoint_name, 'viewpoint', None).setParentItem(self)

