
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtCore import Qt, QRectF
from PyQt5.QtWidgets import QGraphicsScene, QGraphicsItem, QGraphicsPixmapItem, QStyleOptionGraphicsItem
from PyQt5.QtGui import QTextDocument, QPen, QBrush, QDrag, QPixmap, QPainter

from util import some
from settings import settings
from game.env import env

from data.FPL import FPL
from data.strip import strip_mime_type, received_from_detail, sent_to_detail, \
		assigned_SQ_detail, parsed_route_detail, soft_link_detail, recycled_detail

from GUI.misc import signals, selection
from GUI.graphics.miscGraphics import new_pen, EmptyGraphicsItem


# ---------- Constants ----------

loose_strip_width = 200
loose_strip_height = 46
loose_strip_margin = 2 # between selection indicator and strip border

# -------------------------------



def acknowledge_strip(strip):
	strip.writeDetail(received_from_detail, None)
	strip.writeDetail(sent_to_detail, None)
	strip.writeDetail(recycled_detail, None)


def strip_mouse_press(strip, event):
	if event.button() == Qt.LeftButton:
		acknowledge_strip(strip)
		selection.selectStrip(strip)
	elif event.button() == Qt.MiddleButton:
		if event.modifiers() & Qt.ShiftModifier: # STRIP UNLINK  REQUEST
			if strip is selection.strip:
				if selection.fpl != None and selection.acft != None: # both are linked
					signals.statusBarMsg.emit('Ambiguous action. SHIFT-click on what to unlink, or use strip menu.')
				elif selection.fpl == None and selection.acft != None: # only one is linked
					strip.linkAircraft(None)
					signals.stripInfoChanged.emit()
					selection.selectAircraft(selection.acft)
				elif selection.fpl != None and selection.acft == None:
					strip.linkFPL(None)
					signals.stripInfoChanged.emit()
					selection.selectFPL(selection.fpl)
		else: # STRIP LINK REQUEST
			if selection.fpl != None and env.linkedStrip(selection.fpl) == None and strip.linkedFPL() == None:
				strip.linkFPL(selection.fpl)
				if settings.strip_autofill_on_FPL_link:
					strip.fillFromFPL()
				signals.stripInfoChanged.emit()
				selection.selectStrip(strip)
			elif selection.acft != None and env.linkedStrip(selection.acft) == None and strip.linkedAircraft() == None:
				strip.linkAircraft(selection.acft)
				if settings.strip_autofill_on_ACFT_link:
					strip.fillFromXPDR()
				signals.stripInfoChanged.emit()
				selection.selectStrip(strip)
			if strip is selection.strip:
				acknowledge_strip(strip)







def paint_strip_box(painter, strip, rect, parent_widget):
	acft = strip.linkedAircraft()
	
	### LINE 1
	## Callsign section
	callsign_section = ''
	# handover from
	fromATC = strip.lookup(received_from_detail)
	if fromATC != None:
		callsign_section += fromATC + '&gt;&gt; '
	# callsigns
	if strip.lookup(FPL.COMMENTS) != None:
		callsign_section += '*'
	scs = strip.callsign(fpl=True)
	acs = None if acft == None else acft.xpdrCallsign()
	callsign_section += '<strong>%s</strong>' % some(scs, some(acs, '?'))
	if scs != None and acs != None and scs != acs: # callsign conflict with XPDR
		callsign_section += ' <strong>(%s)</strong>' % acs
	# handover to
	toATC = strip.lookup(sent_to_detail)
	if toATC != None:
		callsign_section += '&gt;&gt; ' + toATC
	line1_sections = [callsign_section]
	## Wake turb. cat. / aircraft type
	atyp = None if acft == None else acft.xpdrAcftType()
	typesec = some(strip.lookup(FPL.ACFT_TYPE, fpl=True), some(atyp, ''))
	wtc = strip.lookup(FPL.WTC, fpl=True)
	if wtc != None:
		typesec += '/%s' % wtc
	line1_sections.append(typesec)
	## Optional sections
	# transponder code
	assSQ = strip.lookup(assigned_SQ_detail)
	if assSQ != None:
		if acft == None: # no ACFT linked
			line1_sections.append('sq=%04d' % assSQ)
		else:
			sq = acft.xpdrSqCode()
			if sq != None and sq != assSQ:
				line1_sections.append('sq=%04d (%04d)' % (assSQ, sq))
	# conflicts
	conflicts = []
	if strip.transponderConflictList() != []:
		conflicts.append('!!XPDR')
	if len(strip.vectoringConflicts(env.QNH())) != 0:
		conflicts.append('!!vect')
	elif strip.routeConflict():
		conflicts.append('!!route')
	if conflicts != []:
		line1_sections.append(' '.join(conflicts))
	
	### LINE 2
	line2_sections = []
	rules = strip.lookup(FPL.FLIGHT_RULES, fpl=True)
	if rules != None:
		line2_sections.append(rules)
	parsed_route = strip.lookup(parsed_route_detail)
	if parsed_route == None:
		arr = strip.lookup(FPL.ICAO_ARR, fpl=True)
		if arr != None:
			line2_sections.append(arr)
	elif acft == None:
		line2_sections.append(str(parsed_route))
	else:
		line2_sections.append(parsed_route.toGoStr(acft.coords()))
	
	## MAKE DOCUMENT
	line1 = ' &nbsp; '.join(line1_sections)
	line2 = ': '.join(line2_sections)
	doc = QTextDocument(parent_widget)
	doc.setHtml('<html><body><p>%s<br>&nbsp;&nbsp; %s</p></body></html>' % (line1, line2))
	
	## PAINT
	## Background
	if acft == None:
		if strip.lookup(soft_link_detail) == None:
			bgcol = 'strip_unlinked'
		else:
			bgcol = 'strip_unlinked_identified'
	else:
		if strip.transponderConflictList() != []:
			bgcol = 'strip_linked_conflict_XPDR'
		elif len(strip.vectoringConflicts(env.QNH())) != 0 or strip.routeConflict():
			bgcol = 'strip_linked_conflict_vect'
		else:
			bgcol = 'strip_linked_OK'
	painter.save()
	painter.setPen(new_pen(Qt.darkGray))
	painter.setBrush(QBrush(settings.colour(bgcol)))
	painter.drawRect(rect)
	painter.translate(rect.topLeft())
	doc.drawContents(painter, QRectF(0, 0, rect.width(), rect.height()))
	painter.restore()








class LooseStripItem(QGraphicsItem):
	def __init__(self, strip, compact, parent=None):
		QGraphicsItem.__init__(self, parent)
		self.setFlag(QGraphicsItem.ItemIsMovable, True)
		self.strip = strip
		self.compact_display = compact
	
	def setCompact(self, toggle):
		self.compact_display = toggle
		self.prepareGeometryChange()
	
	def boundingRect(self):
		w = loose_strip_width
		h = loose_strip_height
		if self.compact_display:
			w /= 2
			h /= 2
		return QRectF(-w / 2, -h / 2, w, h)
	
	def paint(self, painter, option, widget):
		if self.strip is selection.strip:
			painter.save()
			painter.setPen(new_pen(settings.colour('selection_indicator'), width=2))
			painter.drawRect(self.boundingRect())
			painter.restore()
		painter.save()
		painter.translate(option.rect.topLeft())
		painter.translate(loose_strip_margin, loose_strip_margin)
		doc_rect = QRectF(0, 0, option.rect.width() - 2 * loose_strip_margin, option.rect.height() - 2 * loose_strip_margin)
		paint_strip_box(painter, self.strip, doc_rect, widget)
		painter.restore()
	
	def toPixmap(self):
		rect = self.boundingRect()
		pixmap = QPixmap(rect.width(), rect.height())
		pixmap.fill(Qt.darkRed)
		painter = QPainter(pixmap)
		painter.drawRect(rect)
		self.scene().render(painter, QRectF(), self.sceneBoundingRect())
		painter.end()
		return pixmap
	
	def mousePressEvent(self, event):
		self._pos_at_mouse_press = self.pos()
		strip_mouse_press(self.strip, event)
		QGraphicsItem.mousePressEvent(self, event)
		
	def mouseMoveEvent(self, event):
		if event.modifiers() & Qt.ShiftModifier:
			QGraphicsItem.mouseMoveEvent(self, event)
		else:
			drag = QDrag(event.widget())
			drag.setMimeData(env.strips.mkMimeDez(self.strip))
			pixmap = self.toPixmap()
			drag.setPixmap(pixmap)
			drag.setHotSpot(pixmap.rect().center())
			parent_scene_backup = self.scene()
			parent_scene_backup.removeItem(self)
			drag_result = drag.exec()
			if drag_result == Qt.IgnoreAction: # no drop action performed; must restore strip
				self.setPos(self._pos_at_mouse_press)
				parent_scene_backup.addStripItem(self)

	def mouseDoubleClickEvent(self, event):
		signals.stripEditRequest.emit(self.strip)
		event.accept()
		QGraphicsItem.mouseDoubleClickEvent(self, event)








class LooseStripBayScene(QGraphicsScene):
	def __init__(self, parent):
		QGraphicsScene.__init__(self, parent)
		self.bg_item = None
		self.compact_strips = False
		self.fillBackground()
		r = -2.5 * loose_strip_width
		self.addRect(QRectF(r, r, 2 * r, 2 * r), pen=QPen(Qt.NoPen)) # avoid empty scene
		self.strip_items = EmptyGraphicsItem()
		self.addItem(self.strip_items)
		self.strip_items.setZValue(1) # gets strips on top of bg_item
		signals.selectionChanged.connect(self.updateSelection)
		signals.colourConfigReloaded.connect(self.fillBackground)
	
	def disconnectAllSignals(self):
		signals.selectionChanged.disconnect(self.updateSelection)
		signals.colourConfigReloaded.disconnect(self.fillBackground)
	
	def fillBackground(self):
		self.setBackgroundBrush(settings.colour('loose_strip_bay_background'))
	
	def clearBgImg(self):
		if self.bg_item != None:
			self.removeItem(self.bg_item)
			self.bg_item = None
	
	def setBgImg(self, pixmap, scale): # pixmap None to clear background
		self.clearBgImg()
		self.bg_item = QGraphicsPixmapItem(pixmap, None)
		rect = self.bg_item.boundingRect()
		self.bg_item.setScale(scale * loose_strip_width / rect.width())
		self.bg_item.setOffset(-rect.center())
		self.addItem(self.bg_item)
	
	def setCompactStrips(self, b):
		self.compact_strips = b
		for item in self.strip_items.childItems():
			item.setCompact(b)
	
	def updateSelection(self):
		for item in self.strip_items.childItems():
			item.setZValue(1 if item.strip is selection.strip else 0)
			item.update()
	
	def stripsInBay(self):
		return [item.strip for item in self.strip_items.childItems()]
	
	def placeNewStrip(self, strip, pos=None):
		item = LooseStripItem(strip, self.compact_strips)
		if pos != None:
			item.setPos(pos)
		self.addStripItem(item)
	
	def deleteStripItem(self, strip):
		try:
			self.removeItem(next(item for item in self.strip_items.childItems() if item.strip is strip))
		except StopIteration:
			print('Report error: strip not in this loose bay for deletion.')
	
	def deleteAllStripItems(self):
		for item in self.strip_items.childItems():
			self.removeItem(item)
	
	def addStripItem(self, item):
		item.setParentItem(self.strip_items)
	
	def dropEvent(self, event):
		if event.mimeData().hasFormat(strip_mime_type):
			strip = env.strips.fromMimeDez(event.mimeData())
			env.strips.repositionStrip(strip, None)
			self.placeNewStrip(strip, pos=event.scenePos())
			signals.selectionChanged.emit()
			event.acceptProposedAction()
	
	def mousePressEvent(self, event):
		if self.mouseGrabberItem() == None:
			selection.deselect()
			event.accept()
		QGraphicsScene.mousePressEvent(self, event)
	
	def dragMoveEvent(self, event):
		pass # Scene's default impl. ignores the event when no item is under mouse (this enables mouse drop on scene)

