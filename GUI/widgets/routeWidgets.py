
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QWidget, QGraphicsView, QInputDialog, QMessageBox
from PyQt5.QtGui import QTransform, QIcon
from PyQt5.QtCore import pyqtSignal

from ui.routeEditWidget import Ui_routeEditWidget

from settings import settings
from data.UTC import datestr, timestr
from GUI.misc import signals, IconFile
from GUI.dialog.miscDialogs import yesNo_question
from ext.resources import route_presets_file


# ---------- Constants ----------

zoom_factor = 1.25

# -------------------------------





class RouteView(QGraphicsView):	
	def __init__(self, parent):
		QGraphicsView.__init__(self, parent)
		self.scale = .25
		self.setTransform(QTransform.fromScale(self.scale, self.scale))
		
	def wheelEvent(self, event):
		if event.angleDelta().y() > 0:
			self.scale *= zoom_factor
		else:
			self.scale /= zoom_factor
		self.setTransform(QTransform.fromScale(self.scale, self.scale))





class RouteEditWidget(QWidget, Ui_routeEditWidget):
	viewRoute_signal = pyqtSignal()
	
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.view_button.setIcon(QIcon(IconFile.button_view))
		self.saveAsPreset_button.setIcon(QIcon(IconFile.button_save))
		self.setFocusProxy(self.text_edit)
		self.data_DEP = None # Airfield
		self.data_ARR = None # Airfield
		self._updateButtons()
		self.view_button.clicked.connect(self.viewRoute_signal.emit)
		self.suggestions_button.clicked.connect(self.suggestPreset)
		self.saveAsPreset_button.clicked.connect(self.savePreset)
		self.text_edit.textChanged.connect(self._updateButtons)
	
	def _updateButtons(self):
		if self.data_DEP == None or self.data_ARR == None:
			self.view_button.setEnabled(False)
			self.suggestions_button.setEnabled(False)
			self.saveAsPreset_button.setEnabled(False)
		else:
			self.view_button.setEnabled(True)
			self.suggestions_button.setEnabled((self.data_DEP.code, self.data_ARR.code) in settings.route_presets)
			self.saveAsPreset_button.setEnabled(self.getRouteText() != '')
	
	def setDEP(self, airport):
		self.data_DEP = airport
		self._updateButtons()
	
	def setARR(self, airport):
		self.data_ARR = airport
		self._updateButtons()
	
	def resetDEP(self):
		self.data_DEP = None
		self._updateButtons()
	
	def resetARR(self):
		self.data_ARR = None
		self._updateButtons()
	
	def setRouteText(self, txt):
		self.text_edit.setPlainText(txt)
	
	def getRouteText(self):
		return self.text_edit.toPlainText()
	
	def suggestPreset(self):
		suggestions = settings.route_presets[self.data_DEP.code, self.data_ARR.code]
		if self.getRouteText() == '' and len(suggestions) == 1:
			self.setRouteText(suggestions[0])
		else:
			text, ok = QInputDialog.getItem(self, 'Route suggestions', \
				'From %s to %s:' % (self.data_DEP, self.data_ARR), suggestions, editable=False)
			if ok:
				self.setRouteText(text)
		self.text_edit.setFocus()
	
	def savePreset(self):
		icao_pair = self.data_DEP.code, self.data_ARR.code
		route_txt = ' '.join(self.getRouteText().split())
		got_routes = settings.route_presets.get(icao_pair, [])
		if route_txt == '' or route_txt in got_routes:
			QMessageBox.critical(self, 'Route preset rejected', 'This route entry is already saved!')
			return
		msg = 'Saving a route preset between %s and %s.' % icao_pair
		if got_routes != []:
			msg += '\n(%d route%s already saved for these end airports)' % (len(got_routes), ('s' if len(got_routes) != 1 else ''))
		if yesNo_question(self, 'Saving route preset', msg, 'Confirm?'):
			try:
				settings.route_presets[icao_pair].append(route_txt)
			except KeyError:
				settings.route_presets[icao_pair] = [route_txt]
			self.suggestions_button.setEnabled(True)
			try:
				with open(route_presets_file, mode='a') as f:
					f.write('\n# Saved on %s at %s UTC:\n' % (datestr(), timestr()))
					f.write('%s %s\t%s\n\n' % (self.data_DEP.code, self.data_ARR.code, route_txt))
				QMessageBox.information(self, 'Route preset saved', 'Check file %s to remove or edit.' % route_presets_file)
			except OSError:
				QMessageBox.critical(self, 'Error', 'There was an error writing to "%s".\nYour preset will be lost at the end of the session.')

