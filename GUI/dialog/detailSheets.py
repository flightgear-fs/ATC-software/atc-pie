
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtCore import QDateTime, QThread
from PyQt5.QtGui import QIcon
from ui.FPLdetailsDialog import Ui_FPLdetailsDialog
from ui.stripDetailsDialog import Ui_stripDetailsDialog

from datetime import timedelta, timezone

from util import some
from settings import settings
from game.env import env
from game.manager import GameType
from ext.lenny64 import file_new_FPL, upload_FPL_updates, Lenny64Error

from data.db import wake_turb_cat
from data.FPL import FPL
from data.route import Route
from data.UTC import now, datestr, timestr
from data.params import Heading, StdPressureAlt, Speed
from data.strip import rack_detail, assigned_SQ_detail, assigned_heading_detail, assigned_speed_detail, assigned_altitude_detail

from GUI.misc import signals, IconFile
from GUI.dialog.miscDialogs import yesNo_question
from GUI.dialog.routeDialog import RouteDialog
from GUI.widgets.miscWidgets import RadioKeyEventFilter


# ---------- Constants ----------

FPLpush_timeout = 5000 # ms
unknown_callsign_string = '(unknown callsign)'
unracked_strip_str = '(unracked)'

# -------------------------------



def FPL_match_button_filter(callsign_filter):
	return lambda fpl: callsign_filter.upper() in fpl[FPL.CALLSIGN].upper() and fpl.flightIsInTimeWindow(timedelta(hours=12))


class FPLpusher(QThread):
	
	def __init__(self, fpl, parent):
		QThread.__init__(self, parent)
		self.fpl = fpl
		self.lenny64_error = None
		
	def run(self):
		self.lenny64_error = None
		try:
			upload_FPL_updates(self.fpl) if self.fpl.existsOnline() else file_new_FPL(self.fpl)
		except Lenny64Error as err:
			self.lenny64_error = err










class SharedDetailSheet:
	'''
	WARNING! Methods to define in subclasses:
	- get_detail (reads from strip or FPL)
	- set_detail (writes to strip or FPL)
	- resetData (should call this resetData after doing its additional work)
	- saveChangesAndClose (call this shared saveChangesAndClose)
	'''
	def __init__(self):
		self.route_edit.viewRoute_signal.connect(self.viewRoute)
		self.depAirportPicker_widget.recognised.connect(self.route_edit.setDEP)
		self.arrAirportPicker_widget.recognised.connect(self.route_edit.setARR)
		self.depAirportPicker_widget.unrecognised.connect(self.route_edit.resetDEP)
		self.arrAirportPicker_widget.unrecognised.connect(self.route_edit.resetARR)
		self.save_button.clicked.connect(self.saveChangesAndClose)
		self.resetData()
		self.aircraftType_edit.editTextChanged.connect(self.autoFillWTCfromType)
		self.autoFillWTC_button.toggled.connect(self.autoFillWTCtoggled)
	
	def autoFillWTCfromType(self, dez):
		if self.autoFillWTC_button.isChecked():
			self.wakeTurbCat_select.setCurrentText(some(wake_turb_cat(dez), ''))
	
	def autoFillWTCtoggled(self, toggle):
		if toggle:
			self.autoFillWTCfromType(self.aircraftType_edit.currentText())
	
	def viewRoute(self):
		tas = Speed(self.TAS_edit.value()) if self.TAS_enable.isChecked() else None
		route_to_view = Route(self.route_edit.data_DEP, self.route_edit.data_ARR, self.route_edit.getRouteText())
		RouteDialog(route_to_view, speedHint=tas, acftHint=self.aircraftType_edit.getAircraftType(), parent=self).exec()
	
	def resetData(self):
		# FPL.CALLSIGN
		self.callsign_edit.setText(some(self.get_detail(FPL.CALLSIGN), ''))
		#	FLIGHT_RULES
		self.flightRules_select.setCurrentText(some(self.get_detail(FPL.FLIGHT_RULES), ''))
		# FPL.ACFT_TYPE
		self.aircraftType_edit.setCurrentText(some(self.get_detail(FPL.ACFT_TYPE), ''))
		# FPL.WTC
		self.wakeTurbCat_select.setCurrentText(some(self.get_detail(FPL.WTC), ''))
		# FPL.ICAO_DEP
		self.depAirportPicker_widget.setEditText(some(self.get_detail(FPL.ICAO_DEP), ''))
		# FPL.ICAO_ARR
		self.arrAirportPicker_widget.setEditText(some(self.get_detail(FPL.ICAO_ARR), ''))
		#	ROUTE
		self.route_edit.setRouteText(some(self.get_detail(FPL.ROUTE), ''))
		#	CRUISE_ALT
		self.cruiseAlt_edit.setText(some(self.get_detail(FPL.CRUISE_ALT), ''))
		#	TAS
		tas = self.get_detail(FPL.TAS)
		self.TAS_enable.setChecked(tas != None)
		if tas != None:
			self.TAS_edit.setValue(tas.kt)
		#	COMMENTS
		self.comments_edit.setPlainText(some(self.get_detail(FPL.COMMENTS), ''))
		self.callsign_edit.setFocus()
		
	def saveChangesAndClose(self):
		# FPL.CALLSIGN
		self.set_detail(FPL.CALLSIGN, self.callsign_edit.text().upper())
		# FPL.FLIGHT_RULES
		self.set_detail(FPL.FLIGHT_RULES, self.flightRules_select.currentText())
		# FPL.ACFT_TYPE
		self.set_detail(FPL.ACFT_TYPE, self.aircraftType_edit.getAircraftType())
		# FPL.WTC
		self.set_detail(FPL.WTC, self.wakeTurbCat_select.currentText())
		# FPL.ICAO_DEP
		self.set_detail(FPL.ICAO_DEP, self.depAirportPicker_widget.currentText())
		# FPL.ICAO_ARR
		self.set_detail(FPL.ICAO_ARR, self.arrAirportPicker_widget.currentText())
		# FPL.ROUTE
		self.set_detail(FPL.ROUTE, self.route_edit.getRouteText())
		# FPL.CRUISE_ALT
		self.set_detail(FPL.CRUISE_ALT, self.cruiseAlt_edit.text())
		# FPL.TAS
		self.set_detail(FPL.TAS, (Speed(self.TAS_edit.value()) if self.TAS_enable.isChecked() else None))
		# FPL.COMMENTS
		self.set_detail(FPL.COMMENTS, self.comments_edit.toPlainText())













# =========== STRIP =========== #

class StripDetailSheetDialog(QDialog, Ui_stripDetailsDialog, SharedDetailSheet):
	def __init__(self, gui, strip):
		QDialog.__init__(self, gui)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowIcon(QIcon(IconFile.dock_strips))
		self.matchingFPL_button.setIcon(QIcon(IconFile.dock_FPLs))
		self.strip = strip
		self.FPL_matches = []
		if self.strip.lookup(rack_detail) == None: # unracked strip
			self.rack_select.addItem(unracked_strip_str)
			self.rack_select.setEnabled(False)
		else:
			self.rack_select.addItems(env.strips.rackNames())
		ro_tod = strip.lookup(FPL.TIME_OF_DEP)
		ro_eet = strip.lookup(FPL.EET)
		ro_altAD = strip.lookup(FPL.ICAO_ALT)
		ro_souls = strip.lookup(FPL.SOULS)
		other_details = []
		if ro_tod != None:
			other_details.append('Dep. date: %s' % datestr(ro_tod))
			other_details.append('Dep. time: %s' % timestr(ro_tod))
		if ro_eet != None:
			minutes = int(ro_eet.total_seconds() + .5) // 60
			other_details.append('EET: %d h %d min' % (minutes // 60, minutes % 60))
		if ro_altAD != None:
			other_details.append('Alt. AD: %s' % ro_altAD)
		if ro_souls != None:
			other_details.append('Souls on board: %d' % ro_souls)
		if other_details == []:
			self.clearOtherDetails_button.setEnabled(False)
			self.otherDetails_info.setText('None')
		else:
			self.otherDetails_info.setText('\n'.join(other_details))
			self.clearOtherDetails_button.clicked.connect(self.clearOtherDetails)
		self.depAirportPicker_widget.recognised.connect(lambda ad: self.depAirportName_info.setText(ad.long_name))
		self.arrAirportPicker_widget.recognised.connect(lambda ad: self.arrAirportName_info.setText(ad.long_name))
		self.depAirportPicker_widget.unrecognised.connect(self.depAirportName_info.clear)
		self.arrAirportPicker_widget.unrecognised.connect(self.arrAirportName_info.clear)
		self.callsign_edit.textChanged.connect(self.updateMatchingFPLs)
		self.cruiseAlt_edit.textChanged.connect(self.updateCruiseButton)
		self.matchingFPL_button.clicked.connect(self.openMatchingFlightPlans)
		self.assignCruiseAlt_button.clicked.connect(self.assignCruiseLevel)
		SharedDetailSheet.__init__(self)
		self.updateMatchingFPLs('')
	
	def updateCruiseButton(self, cruise_alt_text):
		self.assignCruiseAlt_button.setEnabled(cruise_alt_text != '')
	
	def updateMatchingFPLs(self, cs):
		if self.strip.linkedFPL() != None or cs == '':
			self.FPL_matches = []
		else:
			self.FPL_matches = env.FPLs.findAll(FPL_match_button_filter(cs))
		self.matchingFPL_button.setEnabled(self.FPL_matches != [])
	
	def openMatchingFlightPlans(self):
		msg = 'Flight plans today whose callsigns contain "%s":\n\n' % self.callsign_edit.text()
		msg += '\n'.join(fpl.shortDescr() for fpl in self.FPL_matches)
		QMessageBox.information(self, 'Matching FPLs today', msg)
	
	def assignCruiseLevel(self):
		self.assignAltitude.setChecked(True)
		self.assignedAltitude_edit.setText(self.cruiseAlt_edit.text())
	
	def get_detail(self, detail):
		return self.strip.lookup(detail)
	
	def set_detail(self, detail, new_val):
		self.strip.writeDetail(detail, new_val)
	
	def selectedRack(self):
		return None if self.strip.lookup(rack_detail) == None else self.rack_select.currentText()
	
	def clearOtherDetails(self):
		if yesNo_question(self, 'Clearing details', 'Details will permanently be removed from the strip.', 'Are you sure?'):
			for detail in [FPL.TIME_OF_DEP, FPL.EET, FPL.SOULS]:
				self.strip.writeDetail(detail, None)
			self.clearOtherDetails_button.setEnabled(False)
			self.otherDetails_info.setText('None')
	
	def resetData(self):
		## Rack
		self.rack_select.setCurrentText(some(self.strip.lookup(rack_detail), unracked_strip_str))
		## Assigned stuff
		# Squawk code
		assSQ = self.strip.lookup(assigned_SQ_detail)
		self.assignSquawkCode.setChecked(assSQ != None)
		if assSQ != None:
			self.xpdrCode_select.setSQ(assSQ)
		# Heading
		assHdg = self.strip.lookup(assigned_heading_detail)
		self.assignHeading.setChecked(assHdg != None)
		if assHdg != None:
			self.assignedHeading_edit.setValue(int(assHdg.read()))
		# Altitude/FL
		assAlt = self.strip.lookup(assigned_altitude_detail)
		self.assignAltitude.setChecked(assAlt != None)
		if assAlt != None:
			self.assignedAltitude_edit.setText(assAlt)
		# Speed
		assSpd = self.strip.lookup(assigned_speed_detail)
		self.assignSpeed.setChecked(assSpd != None)
		if assSpd != None:
			self.assignedSpeed_edit.setValue(assSpd.kt)
		## Links and conflicts:
		acft = self.strip.linkedAircraft()
		fpl = self.strip.linkedFPL()
		if acft == None:
			self.xpdrConflicts_info.setText('no link')
		else:
			clst = self.strip.transponderConflictList()
			self.xpdrConflicts_info.setText('no conflicts' if clst == [] else 'conflicts ' + ', '.join(clst))
		if fpl == None:
			self.fplConflicts_info.setText('no link')
			self.pushToFPL_tickBox.setText('Link new local FPL (opens editor)')
		else:
			clst = self.strip.FPLconflictList()
			self.fplConflicts_info.setText('no conflicts' if clst == [] else 'conflicts ' + ', '.join(clst))
			self.pushToFPL_tickBox.setText('Push details to FPL')
		## FPL stuff
		SharedDetailSheet.resetData(self)
	
	def saveChangesAndClose(self):
		SharedDetailSheet.saveChangesAndClose(self)
		## Assigned stuff
		# Squawk code
		if self.assignSquawkCode.isChecked():
			self.set_detail(assigned_SQ_detail, self.xpdrCode_select.getSQ())
		else:
			self.set_detail(assigned_SQ_detail, None)
		# Heading
		if self.assignHeading.isChecked():
			self.set_detail(assigned_heading_detail, Heading(self.assignedHeading_edit.value(), False))
		else:
			self.set_detail(assigned_heading_detail, None)
		# Altitude/FL
		if self.assignAltitude.isChecked():
			self.set_detail(assigned_altitude_detail, StdPressureAlt.ckReading(self.assignedAltitude_edit.text())[1])
		else:
			self.set_detail(assigned_altitude_detail, None)
		# Speed
		if self.assignSpeed.isChecked():
			self.set_detail(assigned_speed_detail, Speed(self.assignedSpeed_edit.value()))
		else:
			self.set_detail(assigned_speed_detail, None)
		# DONE.
		if self.pushToFPL_tickBox.isChecked():
			if self.strip.linkedFPL() == None:
				signals.newLinkedFPLrequest.emit(self.strip)
			else:
				self.strip.pushToFPL()
		self.accept()
		# WARNING: deal with rack_select change after dialog accept (use selectedRack method)



















# =========== FPL =========== #

class FPLdetailSheetDialog(QDialog, Ui_FPLdetailsDialog, SharedDetailSheet):
	def __init__(self, gui, fpl):
		QDialog.__init__(self, gui)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowIcon(QIcon(IconFile.dock_FPLs))
		self.fpl = fpl
		self.updateOnlineStatusBox()
		SharedDetailSheet.__init__(self)
		self.viewOnlineComments_button.clicked.connect(self.viewOnlineComments)
		self.openFPL_button.clicked.connect(lambda: self.changeFplOnlineStatus(FPL.OPEN))
		self.closeFPL_button.clicked.connect(lambda: self.changeFplOnlineStatus(FPL.CLOSED))
	
	def updateOnlineStatusBox(self):
		online_status = self.fpl.status()
		online_edit_OK = settings.game_manager.onlineFplEditAvailable()
		self.openFPL_button.setEnabled(online_edit_OK and online_status == FPL.FILED)
		self.closeFPL_button.setEnabled(online_edit_OK and online_status == FPL.OPEN)
		self.publishOnlineOnSave_tickBox.setEnabled(online_edit_OK)
		if self.fpl.existsOnline():
			status_txt = {FPL.FILED: 'filed', FPL.OPEN: 'open', FPL.CLOSED: 'closed'}.get(online_status, 'unknown')
			changes_txt = ', '.join(FPL.detailStrNames[d] for d in self.fpl.modified_details) if self.fpl.needsUpload() else 'none'
		else:
			status_txt = 'not online'
			changes_txt = '--'
		self.onlineStatus_infoLabel.setText(status_txt)
		self.syncStatus_infoLabel.setText(changes_txt)
	
	def get_detail(self, detail):
		return self.fpl[detail]
	
	def set_detail(self, detail, new_val):
		self.fpl[detail] = new_val
	
	def viewOnlineComments(self):
		QMessageBox.information(self, 'Online FPL comments', '\n'.join(self.fpl.onlineComments()))
	
	def changeFplOnlineStatus(self, new_status):
		t = now()
		if new_status == FPL.OPEN:
			text = 'Do you want also to update departure time with the current date & time below?\n%s, %s' % (datestr(t), timestr(t))
			text += '\n\nWARNING: Answering "yes" or "no" will open the flight plan online.'
			button = QMessageBox.question(self, 'Open FPL', text, buttons=(QMessageBox.Cancel | QMessageBox.No | QMessageBox.Yes))
			if button == QMessageBox.Yes:
				self.depTime_edit.setDateTime(QDateTime(t.year, t.month, t.day, t.hour, t.minute))
			ok = button != QMessageBox.Cancel
		elif new_status == FPL.CLOSED:
			ok = yesNo_question(self, 'Close FPL', 'Time is %s.' % timestr(t), 'This will close the flight plan online. OK?')
		if ok:
			settings.game_manager.changeFplStatus(self.fpl, new_status)
			if self.fpl.status() == new_status:
				self.updateOnlineStatusBox()
			else:
				QMessageBox.critical(self, 'FPL error', 'Error setting online flight plan status.\nReport any console output message.')
	
	def closeFPL(self):
			if self.fpl.status() == FPL.CLOSED:
				self.closeFPL_button.setEnabled(False)
			else:
				QMessageBox.critical(self, 'FPL error', 'Error closing flight plan.\nReport any console output message.')
	
	def resetData(self):
		# FPL.TIME_OF_DEP
		dep = self.fpl[FPL.TIME_OF_DEP]
		self.depTime_enable.setChecked(dep != None)
		if dep == None:
			dep = now()
		self.depTime_edit.setDateTime(QDateTime(dep.year, dep.month, dep.day, dep.hour, dep.minute))
		# FPL.EET
		eet = self.fpl[FPL.EET]
		self.EET_enable.setChecked(eet != None)
		if eet != None:
			minutes = int(eet.total_seconds() / 60 + .5)
			self.EETh_edit.setValue(minutes // 60)
			self.EETmin_edit.setValue(minutes % 60)
		#	ICAO_ALT
		self.altAirportPicker_widget.setEditText(some(self.fpl[FPL.ICAO_ALT], ''))
		#	SOULS
		souls = self.fpl[FPL.SOULS]
		self.soulsOnBoard_enable.setChecked(souls != None)
		if souls != None:
			self.soulsOnBoard_edit.setValue(souls)
		# ONLINE COMMENTS
		self.viewOnlineComments_button.setEnabled(self.fpl.onlineComments() != [])
		SharedDetailSheet.resetData(self)
	
	def saveChangesAndClose(self):
		SharedDetailSheet.saveChangesAndClose(self)
		# FPL.TIME_OF_DEP
		if self.depTime_enable.isChecked():
			self.set_detail(FPL.TIME_OF_DEP, self.depTime_edit.dateTime().toPyDateTime().replace(tzinfo=timezone.utc))
		else:
			self.set_detail(FPL.TIME_OF_DEP, None)
		# FPL.EET
		if self.EET_enable.isChecked():
			self.set_detail(FPL.EET, timedelta(hours=self.EETh_edit.value(), minutes=self.EETmin_edit.value()))
		else:
			self.set_detail(FPL.EET, None)
		# FPL.ICAO_ALT
		self.set_detail(FPL.ICAO_ALT, self.altAirportPicker_widget.currentText())
		# FPL.SOULS
		self.set_detail(FPL.SOULS, (self.soulsOnBoard_edit.value() if self.soulsOnBoard_enable.isChecked() else None))
		# Done details!
		if self.publishOnlineOnSave_tickBox.isChecked():
			thread = FPLpusher(self.fpl, self)
			thread.start()
			if thread.wait(FPLpush_timeout):
				if thread.lenny64_error != None:
					if thread.lenny64_error.srvResponse() == None:
						msg = str(thread.lenny64_error)
					else: # Lenny64's server answered something
						msg = 'Are you missing mandatory details?\nCheck console output for full server response.'
						print('Lenny64 server response: %s' % thread.lenny64_error.srvResponse())
					QMessageBox.critical(self, 'FPL upload error', 'A problem occured while pushing your flight plan.\n%s' % msg)
					return
			else: # Sadly waiting but Python crashes (!) when thread ends otherwise
				print('QThread.wait timed out. Will this lead to crash? Please report if so.')
				return
		
		self.accept()

