
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re

from PyQt5.QtCore import Qt, QStringListModel, QDateTime, QTime
from PyQt5.QtWidgets import QDialog, QInputDialog, QMessageBox, QColorDialog, QLabel
from PyQt5.QtGui import QIcon, QPixmap

from ui.presetChatMessagesDialog import Ui_presetChatMessagesDialog
from ui.postLennySessionDialog import Ui_postLennySessionDialog
from ui.envInfoDialog import Ui_envInfoDialog
from ui.aboutDialog import Ui_aboutDialog
from ui.routeSpecsLostDialog import Ui_routeSpecsLostDialog
from ui.discardedStripsDialog import Ui_discardedStripsDialog
from ui.editRackDialog import Ui_editRackDialog

from util import some
from settings import settings, version_string

from ext.lenny64 import post_session, Lenny64Error
from data.UTC import now
from data.coords import m2NM
from data.strip import recycled_detail, rack_detail, Strip
from game.env import env
from models.gameStrips import default_rack_name
from GUI.misc import signals
from GUI.widgets.miscWidgets import RadioKeyEventFilter


# ---------- Constants ----------

version_string_placeholder = '##version##'

# -------------------------------


def yesNo_question(parent_window, title, text, question):
	return QMessageBox.question(parent_window, title, text + '\n' + question) == QMessageBox.Yes




class PostLennySessionDialog(QDialog, Ui_postLennySessionDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		t = now()
		qdt = QDateTime(t.year, t.month, t.day, t.hour, t.minute, timeSpec=Qt.UTC)
		self.beginTime_edit.setDateTime(qdt)
		self.endTime_edit.setTime(QTime((t.hour + 2) % 24, 0))
		self.frequency_edit.addFrequencies([(frq, descr) for frq, descr, t in env.frequencies])
		self.frequency_edit.clearEditText()
		self.announce_button.clicked.connect(self.announceSession)
		self.cancel_button.clicked.connect(self.reject)

	def announceSession(self):
		beg = self.beginTime_edit.dateTime().toPyDateTime()
		end = self.endTime_edit.time().toPyTime()
		try:
			post_session(beg, end, frq=self.frequency_edit.getFrequency())
			QMessageBox.information(self, 'Session announced', 'Session successfully announced!')
			self.accept()
		except Lenny64Error as err:
			if err.srvResponse() == None:
				QMessageBox.critical(self, 'Network error', 'A network problem occured: %s' % err)
			else:
				QMessageBox.critical(self, 'Lenny64 error', 'Announcement failed.\nCheck session details and Lenny64 identification settings.')







class AboutDialog(QDialog, Ui_aboutDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.about_text.setHtml(re.sub(version_string_placeholder, version_string, self.about_text.toHtml()))





class EnvironmentInfoDialog(QDialog, Ui_envInfoDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.magneticDeclination_infoLabel.setText(some(env.readDeclination(), 'N/A'))
		if env.airport_data != None:
			self.airportName_infoLabel.setText(env.locationName())
			self.airportElevation_infoLabel.setText('%.1f ft' % env.airport_data.navpoint.altitude)
			self.contents_layout.addWidget(QLabel('\nRWY info: length, DTHR, magn. course'))
			for rwy in env.airport_data.allRunways(sortByName=True):
				dthr = 'none' if rwy.dthr == 0 else '%d m' % rwy.dthr
				descr = '    %s:\t%d m,\t%s,\t%s°' % (rwy.name, rwy.length(dthr=False) / m2NM, dthr, rwy.orientation().read())
				self.contents_layout.addWidget(QLabel(descr))
		
	def showEvent(self, event):
		QDialog.showEvent(self, event)
		self.selectedRunways_infoLabel.setText(env.readRunwaysInUse())














class QStringListModel_noDropReplacement(QStringListModel):
	def __init__(self, strlst):
		QStringListModel.__init__(self, strlst)
	
	def flags(self, index):
		flags = QStringListModel.flags(self, index)
		if index.isValid():
			flags &= ~Qt.ItemIsDropEnabled
		return flags



class PresetChatMessagesDialog(QDialog, Ui_presetChatMessagesDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.model = QStringListModel_noDropReplacement(settings.preset_chat_messages)
		self.messageList_view.setModel(self.model)
		self.add_button.clicked.connect(self.addPresetMessage)
		self.remove_button.clicked.connect(self.removePresetMessage)
		self.close_button.clicked.connect(self.closeDialog)
	
	def addPresetMessage(self):
		msg, ok = QInputDialog.getText(self, 'New preset text message', 'Enter message (aliases allowed):')
		if ok:
			self.model.setStringList(self.model.stringList() + [msg])
	
	def removePresetMessage(self):
		ilst = self.messageList_view.selectedIndexes()
		if ilst != []:
			self.model.removeRow(ilst[0].row())
	
	def closeDialog(self):
		settings.preset_chat_messages = self.model.stringList()
		self.accept()






class RouteSpecsLostDialog(QDialog, Ui_routeSpecsLostDialog):
	def __init__(self, parent, title, lost_specs_text):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowTitle(title)
		self.lostSpecs_box.setText(lost_specs_text)
	
	def mustOpenStripDetails(self):
		return self.openStripDetailSheet_tickBox.isChecked()







class DiscardedStripsDialog(QDialog, Ui_discardedStripsDialog):
	def __init__(self, parent, view_model, dialog_title):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.setWindowTitle(dialog_title)
		self.model = view_model
		self.strip_view.setModel(view_model)
		self.recall_button.clicked.connect(self.recallSelectedStrip)
		self.close_button.clicked.connect(self.accept)
		self.clear_button.clicked.connect(self.model.forgetStrips)
	
	def recallSelectedStrip(self):
		try:
			strip = self.model.stripAt(self.strip_view.selectedIndexes()[0])
			signals.stripRecall.emit(strip)
		except IndexError:
			pass





class EditRackDialog(QDialog, Ui_editRackDialog):
	def __init__(self, parent, rack_name):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.initial_rack_name = rack_name
		self.rack_colour = settings.rack_colours.get(rack_name, None)
		self.flagged_for_deletion = False
		self.rackName_edit.setText(self.initial_rack_name)
		self.updateColourIcon()
		if rack_name == default_rack_name:
			self.rackName_edit.setEnabled(False)
			self.deleteRack_button.setEnabled(False)
		self.pickColour_button.clicked.connect(self.chooseRackColour)
		self.clearColour_button.clicked.connect(self.clearRackColour)
		self.deleteRack_button.toggled.connect(self.flagRackForDeletion)
	
	def flaggedForDeletion(self):
		return self.deleteRack_button.isChecked()
	
	def getNameAndColour(self):
		return self.rackName_edit.text(), self.rack_colour
	
	def updateColourIcon(self):
		if self.rack_colour == None:
			self.pickColour_button.setIcon(QIcon())
		else:
			pixmap = QPixmap(24, 24)
			pixmap.fill(self.rack_colour)
			self.pickColour_button.setIcon(QIcon(pixmap))
	
	def chooseRackColour(self):
		colour = QColorDialog.getColor(initial=some(self.rack_colour, settings.colour('uncoloured_rack')), \
			parent=self, title='Set colour for linked contacts')
		if colour.isValid():
			self.rack_colour = colour
			self.updateColourIcon()
			
	def clearRackColour(self):
		self.rack_colour = None
		self.updateColourIcon()

	def flagRackForDeletion(self, toggle):
		if toggle:
			if env.strips.count(lambda s: s.lookup(rack_detail) == self.initial_rack_name) > 0:
				QMessageBox.warning(self, 'Non-empty rack deletion', 'Rack not empty. Strips will be reracked before deletion.')
			self.rackDeletion_info.setText('Flagged for deletion')
		else:
			self.rackDeletion_info.clear()
