
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QDialog
from ui.startFlightGearMPdialog import Ui_startFlightGearMPdialog
from ui.startSoloDialog_AD import Ui_startSoloDialog_AD
from ui.startStudentSessionDialog import Ui_startStudentSessionDialog

from settings import settings


# ---------- Constants ----------

# -------------------------------



class StartFlightGearMPdialog(QDialog, Ui_startFlightGearMPdialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		srv_str = '[%s]' % settings.FGMS_server_name if ':' in settings.FGMS_server_name else settings.FGMS_server_name
		self.FGMSserver_info.setText('%s:%d' % (srv_str, settings.FGMS_server_port))
		self.callsign_edit.setText(settings.location_code + 'obs')
		self.nickname_edit.setText(settings.ATC_name)
		self.clientPort_edit.setValue(settings.FGMS_client_port)
		self.updateOKbutton()
		self.callsign_edit.textChanged.connect(self.updateOKbutton)
		self.nickname_edit.textChanged.connect(self.updateOKbutton)
		self.accepted.connect(self.updateSettings)
	
	def updateOKbutton(self):
		self.OK_button.setEnabled(self.callsign_edit.text() != '' and self.nickname_edit.text() != '')
	
	def updateSettings(self):
		settings.ATC_name = self.nickname_edit.text()
		settings.FGMS_client_port = self.clientPort_edit.value()
	
	def chosenCallsign(self):
		return self.callsign_edit.text()







class StartSoloDialog_AD(QDialog, Ui_startSoloDialog_AD):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.GND_tickBox.toggled.connect(self.updateOKbutton)
		self.TWR_tickBox.toggled.connect(self.updateOKbutton)
		self.APP_tickBox.toggled.connect(self.updateOKbutton)
		self.DEP_tickBox.toggled.connect(self.updateOKbutton)
		self.updateOKbutton()
		self.accepted.connect(self.updateSettings)
	
	def initialTrafficCount(self):
		return self.initTrafficCount_edit.value()
	
	def updateOKbutton(self):
		gnd, twr, app, dep = (box.isChecked() for box in [self.GND_tickBox, self.TWR_tickBox, self.APP_tickBox, self.DEP_tickBox])
		self.OK_button.setEnabled((gnd or twr or app or dep) and (not gnd or twr or not app and not dep))
	
	def updateSettings(self):
		settings.solo_role_GND = self.GND_tickBox.isChecked()
		settings.solo_role_TWR = self.TWR_tickBox.isChecked()
		settings.solo_role_APP = self.APP_tickBox.isChecked()
		settings.solo_role_DEP = self.DEP_tickBox.isChecked()






class StartStudentSessionDialog(QDialog, Ui_startStudentSessionDialog):
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.teachingServiceHost_edit.setText(settings.teaching_service_host)
		self.teachingServicePort_edit.setValue(settings.teaching_service_port)
		self.updateOKbutton()
		self.teachingServiceHost_edit.textChanged.connect(self.updateOKbutton)
		self.accepted.connect(self.updateSettings)
	
	def updateOKbutton(self):
		self.OK_button.setEnabled(self.teachingServiceHost_edit.text() != '')
	
	def updateSettings(self):
		settings.teaching_service_host = self.teachingServiceHost_edit.text()
		settings.teaching_service_port = self.teachingServicePort_edit.value()




