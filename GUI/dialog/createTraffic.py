
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from PyQt5.QtWidgets import QDialog, QDialogButtonBox
from ui.createTrafficDialog import Ui_createTrafficDialog

from util import some
from settings import settings

from data.db import known_aircraft_types, cruise_speed
from data.params import StdPressureAlt, Speed
from ext.sr import known_airline_codes
from game.env import env
from game.aiStatus import Status, SoloParams
from GUI.widgets.miscWidgets import RadioKeyEventFilter


# ---------- Constants ----------

max_spawn_THR_dist = .25 # NM
max_spawn_PKG_dist = .05 # NM

# -------------------------------


class CreateTrafficDialog(QDialog, Ui_createTrafficDialog):
	last_known_acft_type_used = 'B772'
	last_strip_link = True
	last_start_frozen = False
	
	def __init__(self, spawn_coords, spawn_hdg, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.createAircraftType_edit.setAircraftFilter(lambda t: cruise_speed(t) != None)
		self.airline_codes = known_airline_codes()
		self.createAircraftType_edit.setEditText(CreateTrafficDialog.last_known_acft_type_used)
		self.startFrozen_tickBox.setChecked(CreateTrafficDialog.last_start_frozen)
		self.createStripLink_tickBox.setChecked(CreateTrafficDialog.last_strip_link)
		self.suggestCallsign()
		if env.airport_data != None:
			rwys = [r.name for r in env.airport_data.allRunways() if r.threshold(dthr=True).distanceTo(spawn_coords) <= max_spawn_THR_dist]
			if rwys != []:
				self.createStatus_radio_ready.setEnabled(True)
				self.depRWY_select.addItems(sorted(rwys))
			pk = env.airport_data.ground_net.closestParkingPosition(spawn_coords, maxdist=max_spawn_PKG_dist)
			if pk != None:
				self.createStatus_radio_parked.setEnabled(True)
				self.closestParkingPosition_info.setText(pk)
		self.spawn_coords = spawn_coords
		self.spawn_hdg = spawn_hdg
		self.updateButtons()
		self.createAircraftType_edit.editTextChanged.connect(self.updateButtons)
		self.createAircraftType_edit.editTextChanged.connect(self.suggestCallsign)
		self.createCallsign_edit.textChanged.connect(self.updateButtons)
		self.accepted.connect(self.rememberOptions)
	
	def suggestCallsign(self):
		t = self.createAircraftType_edit.getAircraftType()
		if t in known_aircraft_types():
			self.createCallsign_edit.setText(settings.game_manager.generateCallsign(t, self.airline_codes))
	
	def updateButtons(self):
		cs = self.createCallsign_edit.text()
		t = self.createAircraftType_edit.getAircraftType()
		ok = cs not in [acft.identifier for acft in settings.game_manager.getAircraft()] + env.ATCs.knownATCs()
		ok &= t in known_aircraft_types() and cruise_speed(t) != None
		self.buttonBox.button(QDialogButtonBox.Ok).setEnabled(ok)
	
	def acftCallsign(self):
		return self.createCallsign_edit.text()
	
	def acftType(self):
		return self.createAircraftType_edit.getAircraftType()
	
	def startFrozen(self):
		return self.startFrozen_tickBox.isChecked()
	
	def createStrip(self):
		return self.createStripLink_tickBox.isChecked()
	
	def initStatus(self):
		if self.createStatus_radio_ready.isChecked():
			return Status(Status.READY, arg=self.depRWY_select.currentText())
		elif self.createStatus_radio_parked.isChecked():
			return Status(Status.TAXIING)
		elif self.createStatus_radio_airborne.isChecked():
			return Status(Status.AIRBORNE)
		else:
			print('No status!?')
	
	def acftInitParams(self): 
		status = self.initStatus()
		init_params = SoloParams(status)
		init_params.position = self.spawn_coords
		init_params.heading = self.spawn_hdg
		if self.createStatus_radio_airborne.isChecked():
			init_params.ias = cruise_speed(self.createAircraftType_edit.getAircraftType())
			init_params.altitude = StdPressureAlt.fromFL(self.airborneFL_edit.value())
		else: # on ground
			init_params.ias = Speed(0)
			init_params.altitude = env.groundStdPressureAlt(init_params.position)
			if env.airport_data != None and self.createStatus_radio_parked.isChecked():
				pki = env.airport_data.ground_net.parkingPosInfo(self.closestParkingPosition_info.text())
				init_params.position = pki[0]
				if self.parkedSetsHeading_tickBox.isChecked():
					init_params.heading = pki[1]
		return init_params
	
	def rememberOptions(self): # on dialog accept
		t = self.acftType()
		if t in known_aircraft_types(): # normally it is since we do not allow others for now
			CreateTrafficDialog.last_known_acft_type_used = t
		CreateTrafficDialog.last_strip_link = self.createStrip()
		CreateTrafficDialog.last_start_frozen = self.startFrozen()


