
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from datetime import timedelta
from hashlib import md5

from PyQt5.QtWidgets import QWidget, QDialog, QMessageBox, QFileDialog
from ui.localSettingsDialog import Ui_localSettingsDialog
from ui.generalSettingsDialog import Ui_generalSettingsDialog
from ui.systemSettingsDialog import Ui_systemSettingsDialog
from ui.airspaceRules_AD import Ui_airspaceRulesWidget_AD
from ui.airspaceRules_CTR import Ui_airspaceRulesWidget_CTR

from util import all_diff
from settings import settings, SemiCircRule, XpdrAssignmentRange, XPDR_range_str_sep
from ext.fgfs import fgTwrPositioningOptions
from ext.tts import speech_synthesis_available
from ext.sr import speech_recognition_available, get_pyaudio_devices_info
from game.env import env
from GUI.misc import signals
from GUI.widgets.miscWidgets import RadioKeyEventFilter, RunwayTabWidget
from GUI.dialog.runways import RunwayParametersWidget

from data.aircraft import snapshot_history_size
from data.params import StdPressureAlt
from data.nav import Navpoint


# ---------- Constants ----------

# -------------------------------


class AirspaceRulesWidget_AD(QWidget, Ui_airspaceRulesWidget_AD):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.spawnAPP_minFL_edit.setValue(settings.solo_APP_ceiling_FL_min)
		self.spawnAPP_maxFL_edit.setValue(settings.solo_APP_ceiling_FL_max)
		self.TWRrangeDistance_edit.setValue(settings.solo_TWR_range_dist)
		self.TWRrangeCeiling_edit.setValue(settings.solo_TWR_ceiling_FL)
		self.entryPoints_edit.setText(' '.join(settings.solo_entry_points))
		self.exitPoints_edit.setText(' '.join(settings.solo_exit_points))
		self.initialClimb_edit.setText(settings.solo_initial_climb_reading)
		self.speedRestriction_tickBox.setChecked(settings.solo_enforce_speed_restriction)
		self.spawnAPP_minFL_edit.valueChanged.connect(self.spawnAPP_minFL_valueChanged)
		self.spawnAPP_maxFL_edit.valueChanged.connect(self.spawnAPP_maxFL_valueChanged)
		self.TWRrangeCeiling_edit.valueChanged.connect(self.TWRrangeCeiling_valueChanged)
	
	def spawnAPP_minFL_valueChanged(self, v):
		if v < self.TWRrangeCeiling_edit.value():
			self.TWRrangeCeiling_edit.setValue(v)
		if v > self.spawnAPP_maxFL_edit.value():
			self.spawnAPP_maxFL_edit.setValue(v)
	
	def spawnAPP_maxFL_valueChanged(self, v):
		if v < self.spawnAPP_minFL_edit.value():
			self.spawnAPP_minFL_edit.setValue(v)
	
	def TWRrangeCeiling_valueChanged(self, v):
		if v > self.spawnAPP_minFL_edit.value():
			self.spawnAPP_minFL_edit.setValue(v)
	
	def checkAndSaveSettings(self):
		alt_ok, alt_reading = StdPressureAlt.ckReading(self.initialClimb_edit.text())
		if not alt_ok:
			QMessageBox.critical(self, 'Invalid entry', 'Could not read altitude or level for default initial climb.')
			return False
		entry_points = self.entryPoints_edit.text().split()
		exit_points = self.exitPoints_edit.text().split()
		bad_points = set(pname for pname in entry_points + exit_points \
			if len(env.navpoints.findAll(code=pname, types=[Navpoint.VOR, Navpoint.NDB, Navpoint.FIX])) != 1)
		if len(bad_points) != 0:
			QMessageBox.critical(self, 'Invalid entry', 'Bad navpoint choices (unknown, not unique or bad type):\n%s' % ', '.join(bad_points))
			return False
		settings.solo_APP_ceiling_FL_min = self.spawnAPP_minFL_edit.value() // 10 * 10
		settings.solo_APP_ceiling_FL_max = ((self.spawnAPP_maxFL_edit.value() - 1) // 10 + 1) * 10
		settings.solo_TWR_range_dist = self.TWRrangeDistance_edit.value()
		settings.solo_TWR_ceiling_FL = self.TWRrangeCeiling_edit.value()
		settings.solo_entry_points = entry_points
		settings.solo_exit_points = exit_points
		settings.solo_enforce_speed_restriction = self.speedRestriction_tickBox.isChecked()
		settings.solo_initial_climb_reading = self.initialClimb_edit.text()
		return True




class AirspaceRulesWidget_CTR(QWidget, Ui_airspaceRulesWidget_CTR):
	def __init__(self, parent=None):
		QWidget.__init__(self, parent)
		self.setupUi(self)
		self.spawnCTR_minFL_edit.setValue(settings.solo_CTR_floor_FL)
		self.spawnCTR_maxFL_edit.setValue(settings.solo_CTR_ceiling_FL)
		self.CTRrangeDistance_edit.setValue(settings.solo_CTR_range_dist)
		self.routingPoints_edit.setText(' '.join(settings.solo_CTR_routing_points))
		self.selectSemiCircRule(settings.solo_CTR_semi_circular_rule)
		self.spawnCTR_minFL_edit.valueChanged.connect(self.spawnCTR_minFL_valueChanged)
		self.spawnCTR_maxFL_edit.valueChanged.connect(self.spawnCTR_maxFL_valueChanged)
	
	def spawnCTR_minFL_valueChanged(self, v):
		if v > self.spawnCTR_maxFL_edit.value():
			self.spawnCTR_maxFL_edit.setValue(v)
	
	def spawnCTR_maxFL_valueChanged(self, v):
		if v < self.spawnCTR_minFL_edit.value():
			self.spawnCTR_minFL_edit.setValue(v)
		
	def selectedSemiCircRule(self):
		if self.semiCircRule_radioButton_off.isChecked(): return SemiCircRule.OFF
		elif self.semiCircRule_radioButton_EW.isChecked(): return SemiCircRule.E_W
		elif self.semiCircRule_radioButton_NS.isChecked(): return SemiCircRule.N_S
		
	def selectSemiCircRule(self, rule):
		radio_button = {
				SemiCircRule.OFF: self.semiCircRule_radioButton_off,
				SemiCircRule.E_W: self.semiCircRule_radioButton_EW,
				SemiCircRule.N_S: self.semiCircRule_radioButton_NS
			}[rule]
		radio_button.setChecked(True)
	
	def checkAndSaveSettings(self):
		routing_points = self.routingPoints_edit.text().split()
		bad_points = set(pname for pname in routing_points \
			if len(env.navpoints.findAll(code=pname, types=[Navpoint.VOR, Navpoint.NDB, Navpoint.FIX])) != 1)
		if len(bad_points) != 0:
			QMessageBox.critical(self, 'Invalid entry', 'Bad navpoint choices (unknown, not unique or bad type):\n%s' % ', '.join(bad_points))
			return False
		settings.solo_CTR_floor_FL = self.spawnCTR_minFL_edit.value()
		settings.solo_CTR_ceiling_FL = self.spawnCTR_maxFL_edit.value()
		settings.solo_CTR_range_dist = self.CTRrangeDistance_edit.value()
		settings.solo_CTR_routing_points = routing_points
		settings.solo_CTR_semi_circular_rule = self.selectedSemiCircRule()
		return True





# =================================
#
#           L O C A L
#
# =================================


class LocalSettingsDialog(QDialog, Ui_localSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.runway_tabs = None # Only added in AD mode
		self.airspaceRules_tab = None # AD or CTR variant added
		self.setWindowTitle('Location settings - %s (%s)' % (env.locationName(), settings.location_code))
		self.previous_METAR_station = settings.primary_METAR_station
		self.SQboxes = [self.range1_groupBox, self.range2_groupBox, self.range3_groupBox, self.range4_groupBox]
		self.SQrangeEdits = [self.rangeName1_edit, self.rangeName2_edit, self.rangeName3_edit, self.rangeName4_edit]
		self.SQloEdits = [self.lowerSQ1_edit, self.lowerSQ2_edit, self.lowerSQ3_edit, self.lowerSQ4_edit]
		self.SQhiEdits = [self.upperSQ1_edit, self.upperSQ2_edit, self.upperSQ3_edit, self.upperSQ4_edit]
		if env.airport_data == None:
			self.altOffsetForAiTraffic_label.setEnabled(False)
			self.altOffsetForAiTraffic_edit.setEnabled(False)
			self.airspaceRules_tab = AirspaceRulesWidget_CTR(self)
			self.settings_tabs.addTab(self.airspaceRules_tab, 'Airspace rules (solo CTR)')
		else:
			self.runway_tabs = RunwayTabWidget(self, lambda rwy: RunwayParametersWidget(self, rwy))
			self.settings_tabs.addTab(self.runway_tabs, 'Runway parameters')
			self.airspaceRules_tab = AirspaceRulesWidget_AD(self)
			self.settings_tabs.addTab(self.airspaceRules_tab, 'Airspace rules (solo AD)')
		self.installEventFilter(RadioKeyEventFilter(self))
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(GeneralSettingsDialog.last_tab_used)
		self.buttonBox.accepted.connect(self.storeSettings)

	def fillFromSettings(self):
		self.primaryMetarStation_edit.setText(settings.primary_METAR_station)
		self.transitionAltitude_edit.setValue(settings.transition_altitude)
		self.uncontrolledVFRcode_edit.setValue(settings.uncontrolled_VFR_XPDR_code)
		self.altOffsetForAiTraffic_edit.setValue(settings.AI_traffic_FG_AMSL_correction)
		
		for i in range(len(self.SQboxes)):
			to_fill = settings.XPDR_assignment_ranges[i]
			self.SQboxes[i].setChecked(to_fill != None)
			if to_fill != None:
				self.SQrangeEdits[i].setText(to_fill.name)
				self.SQloEdits[i].setText('%04d' % to_fill.lo)
				self.SQhiEdits[i].setText('%04d' % to_fill.hi)
	
	def storeSettings(self):
		try:
			new_VFR_code = sq(str(self.uncontrolledVFRcode_edit.value()))
			new_ranges = 4 * [None]
			for i in range(len(self.SQboxes)):
				if self.SQboxes[i].isChecked():
					name = self.SQrangeEdits[i].text()
					if XPDR_range_str_sep in name:
						raise ValueError('Reserved character in name %s' % name)
					if not all(r == None or r.name != name for r in new_ranges):
						raise ValueError('Duplicate name for range %d' % (i + 1))
					lo = sq(self.SQloEdits[i].text())
					hi = sq(self.SQhiEdits[i].text())
					if hi < lo:
						raise ValueError('Invalid range %d-%d' % (lo, hi))
					new_ranges[i] = XpdrAssignmentRange(name, lo, hi)
		except ValueError as err:
			QMessageBox.critical(self, 'Assignment range error', str(err))
			return
		
		if self.airspaceRules_tab.checkAndSaveSettings():
			GeneralSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
			
			settings.primary_METAR_station = self.primaryMetarStation_edit.text().upper()
			settings.transition_altitude = self.transitionAltitude_edit.value()
			settings.uncontrolled_VFR_XPDR_code = new_VFR_code
			settings.AI_traffic_FG_AMSL_correction = self.altOffsetForAiTraffic_edit.value()
			settings.XPDR_assignment_ranges = new_ranges
			if env.airport_data != None:
				for w in self.runway_tabs.rwyWidgets():
					w.applyToRWY()
			
			signals.localSettingsChanged.emit()
			self.accept()




def sq(s):
	'''
	Checks the validity of a squawk code string, raising ValueError if not returned.
	'''
	try:
		ignored = int(s, 8)
	except ValueError:
		if s == '': raise ValueError('Missing squawk code')
		else: raise ValueError('Invalid squawk code: %s' % s)
	return int(s)








# =================================
#
#           G E N E R A L
#
# =================================


class GeneralSettingsDialog(QDialog, Ui_generalSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.voiceInstr_off_radioButton.setChecked(True) # auto-excludes if voice instr selected below
		self.voiceInstr_on_radioButton.setEnabled(speech_recognition_available)
		self.readBack_off_radioButton.setChecked(True) # auto-excludes if other selection below
		self.readBack_voice_radioButton.setEnabled(speech_synthesis_available)
		self.installEventFilter(RadioKeyEventFilter(self))
		self.positionHistoryTraceTime_edit.setMaximum(snapshot_history_size)
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(GeneralSettingsDialog.last_tab_used)
		self.buttonBox.accepted.connect(self.storeSettings)
		self.readBack_voice_radioButton.setToolTip('Feature to come') # FUTURE[tts] remove tool tip

	def fillFromSettings(self):
		self.autoSwitchRacks_tickBox.setChecked(settings.auto_switch_racks)
		self.autoFillStripFromXPDR_tickBox.setChecked(settings.strip_autofill_on_ACFT_link)
		self.autoFillStripFromFPL_tickBox.setChecked(settings.strip_autofill_on_FPL_link)
		self.autoFillStripBeforeHandovers_tickBox.setChecked(settings.strip_autofill_before_handovers)
		self.confirmHandovers_tickBox.setChecked(settings.confirm_handovers)
		self.confirmLossyStripReleases_tickBox.setChecked(settings.confirm_lossy_strip_releases)
		self.confirmLinkedStripDeletions_tickBox.setChecked(settings.confirm_linked_strip_deletions)
		self.showRackColourDeco_tickBox.setChecked(settings.show_rack_colours)
		
		self.radarHorizontalRange_edit.setValue(settings.radar_range)
		self.radarFloor_edit.setValue(settings.radar_signal_floor_height)
		self.ignoreFloorOnRadarCheat_tickBox.setChecked(settings.ignore_floor_on_radar_cheat)
		self.radarUpdateInterval_edit.setValue(int(settings.radar_sweep_interval.total_seconds()))
		self.radarContactTimeout_edit.setValue(int(settings.invisible_blips_before_contact_lost))
		self.positionHistoryTraceTime_edit.setValue(int(settings.radar_contact_trace_time.total_seconds()))
		
		self.headingTolerance_edit.setValue(settings.heading_tolerance)
		self.altitudeTolerance_edit.setValue(settings.altitude_tolerance)
		self.speedTolerance_edit.setValue(settings.speed_tolerance)
		self.horizontalSeparation_edit.setValue(settings.horizontal_separation)
		self.verticalSeparation_edit.setValue(settings.vertical_separation)
		self.conflictWarningTime_edit.setValue(int(settings.conflict_warning_anticipation.total_seconds()) / 60)
		self.conflictWarningFloorFL_edit.setValue(settings.conflict_warning_floor_FL)
		self.instructRouteChangeImmediately_tickBox.setChecked(settings.instruct_route_change_immediately)
		self.openLostLegSpecsDialog_tickBox.setChecked(settings.open_lost_leg_specs_dialog)
		
		self.maxAircraftCount_edit.setValue(settings.solo_max_aircraft_count)
		self.minSpawnDelay_seconds_edit.setValue(int(settings.solo_min_spawn_delay.total_seconds()))
		self.maxSpawnDelay_minutes_edit.setValue(int(settings.solo_max_spawn_delay.total_seconds()) / 60)
		self.ARRvsDEP_edit.setValue(int(100 * settings.solo_ARRvsDEP_balance))
		self.ILSvsVisual_edit.setValue(int(100 * settings.solo_ILSvsVisual_balance))
		self.voiceInstr_on_radioButton.setChecked(speech_recognition_available and settings.solo_voice_instructions)
		self.readBack_wilcoBeep_radioButton.setChecked(settings.solo_wilco_beeps)
		self.readBack_voice_radioButton.setChecked(speech_synthesis_available and settings.solo_voice_readback)
	
	def storeSettings(self):
		GeneralSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
		
		settings.auto_switch_racks = self.autoSwitchRacks_tickBox.isChecked()
		settings.strip_autofill_on_ACFT_link = self.autoFillStripFromXPDR_tickBox.isChecked()
		settings.strip_autofill_on_FPL_link = self.autoFillStripFromFPL_tickBox.isChecked()
		settings.strip_autofill_before_handovers = self.autoFillStripBeforeHandovers_tickBox.isChecked()
		settings.confirm_handovers = self.confirmHandovers_tickBox.isChecked()
		settings.confirm_lossy_strip_releases = self.confirmLossyStripReleases_tickBox.isChecked()
		settings.confirm_linked_strip_deletions = self.confirmLinkedStripDeletions_tickBox.isChecked()
		settings.show_rack_colours = self.showRackColourDeco_tickBox.isChecked()
		
		settings.radar_range = self.radarHorizontalRange_edit.value()
		settings.radar_signal_floor_height = self.radarFloor_edit.value()
		settings.ignore_floor_on_radar_cheat = self.ignoreFloorOnRadarCheat_tickBox.isChecked()
		settings.radar_sweep_interval = timedelta(seconds=self.radarUpdateInterval_edit.value())
		settings.invisible_blips_before_contact_lost = self.radarContactTimeout_edit.value()
		settings.radar_contact_trace_time = timedelta(seconds=self.positionHistoryTraceTime_edit.value())
		
		settings.heading_tolerance = self.headingTolerance_edit.value()
		settings.altitude_tolerance = self.altitudeTolerance_edit.value()
		settings.speed_tolerance = self.speedTolerance_edit.value()
		settings.horizontal_separation = self.horizontalSeparation_edit.value()
		settings.vertical_separation = self.verticalSeparation_edit.value()
		settings.conflict_warning_anticipation = timedelta(minutes=self.conflictWarningTime_edit.value())
		settings.conflict_warning_floor_FL = self.conflictWarningFloorFL_edit.value()
		settings.instruct_route_change_immediately = self.instructRouteChangeImmediately_tickBox.isChecked()
		settings.open_lost_leg_specs_dialog = self.openLostLegSpecsDialog_tickBox.isChecked()
		
		settings.solo_max_aircraft_count = self.maxAircraftCount_edit.value()
		settings.solo_min_spawn_delay = timedelta(seconds=self.minSpawnDelay_seconds_edit.value())
		settings.solo_max_spawn_delay = timedelta(minutes=self.maxSpawnDelay_minutes_edit.value())
		settings.solo_ARRvsDEP_balance = self.ARRvsDEP_edit.value() / 100
		settings.solo_ILSvsVisual_balance = self.ILSvsVisual_edit.value() / 100
		settings.solo_voice_instructions = self.voiceInstr_on_radioButton.isChecked()
		settings.solo_wilco_beeps = self.readBack_wilcoBeep_radioButton.isChecked()
		settings.solo_voice_readback = self.readBack_voice_radioButton.isChecked()
		
		signals.generalSettingsChanged.emit()
		self.accept()









# =================================
#
#           S Y S T E M
#
# =================================

class SystemSettingsDialog(QDialog, Ui_systemSettingsDialog):
	#STATIC:
	last_tab_used = 0
	
	def __init__(self, parent=None):
		QDialog.__init__(self, parent)
		self.setupUi(self)
		self.towerView_page.setEnabled(env.airport_data != None and not settings.controlled_tower_viewer.running)
		self.fgcom_page.setEnabled(not settings.game_manager.isRunning())
		self.FG_network_groupBox.setEnabled(not settings.game_manager.isRunning())
		self.FG_lenny64_groupBox.setEnabled(not settings.game_manager.isRunning())
		self.solo_page.setEnabled(not settings.game_manager.isRunning())
		self.SR_groupBox.setEnabled(speech_recognition_available)
		if speech_recognition_available:
			self.audio_devices = get_pyaudio_devices_info()
			self.sphinxAcousticModel_edit.setClearButtonEnabled(True)
			self.audioDeviceIndex_edit.setMaximum(len(self.audio_devices) - 1)
			self.audioDeviceInfo_button.clicked.connect(self.showAudioDeviceInfo)
		self.lennyPassword_edit.setPlaceholderText('No password set' if settings.lenny64_password_md5 == '' else '(unchanged)')
		self.fillFromSettings()
		self.settings_tabs.setCurrentIndex(SystemSettingsDialog.last_tab_used)
		self.produceExtViewerCmd_button.clicked.connect(self.showExternalViewerFgOptions)
		self.lennyPassword_edit.textChanged.connect(self._pwdTextChanged)
		self.browseForSphinxAcousticModel_button.clicked.connect(self.browseForSphinxAcousticModel)
		self.buttonBox.accepted.connect(self.storeSettings)
		self.guessXpdrModes_tickBox.toggled.connect(self.fgmsXpdrModeGuessWarning) # FUTURE[fgms_xpdr] remove
	
	def _pwdTextChanged(self, s):
		self.passwordChange_info.setText('Password will be changed!' if s != '' and settings.lenny64_password_md5 != '' else '')
	
	def fgmsXpdrModeGuessWarning(self, toggle): # FUTURE[fgms_xpdr] remove
		if toggle:
			QMessageBox.warning(self, 'FlightGear XPDR mode guess', 'Note that this makes GND and S modes undiscoverable.')
	
	def browseForSphinxAcousticModel(self):
		txt = QFileDialog.getExistingDirectory(self, caption='Choose Sphinx acoustic model directory')
		if txt != '':
			self.sphinxAcousticModel_edit.setText(txt)
	
	def showAudioDeviceInfo(self):
		txt = 'PyAudio detects %d devices.' % len(self.audio_devices)
		for index, info in enumerate(self.audio_devices):
			txt += '\n\n[%d] %s\n' % (index, info.get('name', '???'))
			txt += ', '.join('%s: %s' % item for item in info.items() if item[0] != 'name')
		QMessageBox.information(self, 'PyAudio devices information', txt)
	
	def showExternalViewerFgOptions(self):
		required_options = fgTwrPositioningOptions()
		required_options.append('--multiplay=out,100,this_host,%d' % settings.FGFS_views_send_port)
		required_options.append('--multiplay=in,100,,%d' % self.towerView_fgmsPort_edit.value())
		required_options.append('--telnet=,,100,,%d,' % self.towerView_telnetPort_edit.value())
		print('Options required for external FlightGear viewer with current dialog options: ' + ' '.join(required_options))
		msg = 'Options required with present configuration (also sent to console):\n'
		msg += '\n'.join('  ' + opt for opt in required_options)
		msg += '\n\nNB: Replace "this_host" with appropriate value.'
		QMessageBox.information(self, 'Required FlightGear options', msg)

	def fillFromSettings(self):
		## Tower view
		(self.towerView_external_radioButton if settings.external_tower_viewer_process else self.towerView_internal_radioButton).setChecked(True)
		self.towerView_fgmsPort_edit.setValue(settings.tower_viewer_UDP_port)
		self.towerView_telnetPort_edit.setValue(settings.tower_viewer_telnet_port)
		self.FGexe_edit.setText(settings.FGFS_executable)
		self.FGrootDir_edit.setText(settings.FGFS_root_dir)
		self.FGsceneryDir_edit.setText(settings.FGFS_scenery_dir)
		self.externalTowerViewerHost_edit.setText(settings.external_tower_viewer_host)
		## FGCom
		self.fgcomExe_edit.setText(settings.fgcom_exe_path)
		self.fgcomServer_edit.setText(settings.fgcom_server)
		self.fgcomReservedPort_edit.setValue(settings.reserved_fgcom_port)
		self.fgcomRadioBoxPorts_edit.setText(','.join(str(n) for n in settings.radio_fgcom_ports))
		## FlightGear MP
		self.MPserverHost_edit.setText(settings.FGMS_server_name)
		self.MPserverPort_edit.setValue(settings.FGMS_server_port)
		self.SXserver_edit.setText(settings.FGSX_server_name)
		self.SXenabled_tickBox.setChecked(settings.FGSX_enabled)
		self.lennyAccountEmail_edit.setText(settings.lenny64_account_email)
		self.lennyPassword_edit.setText('') # unchanged if stays blank
		self.FPLupdateInterval_edit.setValue(int(settings.FPL_update_interval.total_seconds()) / 60)
		self.METARupdateInterval_edit.setValue(int(settings.METAR_update_interval.total_seconds()) / 60)
		self.xpdrModeForNonEquippedAcft_select.setCurrentIndex('0ACS'.index(settings.XPDR_mode_if_not_equipped))
		self.guessXpdrModes_tickBox.setChecked(settings.FGMS_XPDR_mode_guess) # FUTURE[fgms_xpdr] remove option
		self.strictAcftTypeDesignators_tickBox.setChecked(settings.strict_FGFS_ATDs)
		## Solo and teacher session types
		self.soloWeatherChangeInterval_edit.setValue(int(settings.solo_weather_change_interval.total_seconds()) / 60)
		self.soloAircraftTypes_edit.setPlainText('\n'.join(settings.solo_aircraft_types))
		self.restrictAirlineChoiceToLiveries_tickBox.setChecked(settings.solo_restrict_to_available_liveries)
		self.sphinxAcousticModel_edit.setText(settings.sphinx_acoustic_model_dir)
		self.audioDeviceIndex_edit.setValue(settings.audio_input_device_index)

	def storeSettings(self):
		try:
			fgcom_reserved_port = self.fgcomReservedPort_edit.value()
			fgcom_radio_ports = [int(p) for p in self.fgcomRadioBoxPorts_edit.text().split(',')]
			if fgcom_reserved_port in fgcom_radio_ports or not all_diff(fgcom_radio_ports):
				raise ValueError
		except ValueError:
			QMessageBox.critical(self, 'Invalid entry', 'Error or duplicates in FGCom port configuration.')
			return
		
		SystemSettingsDialog.last_tab_used = self.settings_tabs.currentIndex()
		
		## Tower view
		settings.external_tower_viewer_process = self.towerView_external_radioButton.isChecked()
		settings.tower_viewer_UDP_port = self.towerView_fgmsPort_edit.value()
		settings.tower_viewer_telnet_port = self.towerView_telnetPort_edit.value()
		settings.FGFS_executable = self.FGexe_edit.text()
		settings.FGFS_root_dir = self.FGrootDir_edit.text()
		settings.FGFS_scenery_dir = self.FGsceneryDir_edit.text()
		settings.external_tower_viewer_host = self.externalTowerViewerHost_edit.text()
		
		## FGCom
		settings.fgcom_exe_path = self.fgcomExe_edit.text()
		settings.fgcom_server = self.fgcomServer_edit.text()
		settings.reserved_fgcom_port = fgcom_reserved_port
		settings.radio_fgcom_ports = fgcom_radio_ports
		
		## FlightGear MP
		settings.FGMS_server_name = self.MPserverHost_edit.text()
		settings.FGMS_server_port = self.MPserverPort_edit.value()
		settings.FGSX_server_name = self.SXserver_edit.text()
		settings.FGSX_enabled = self.SXenabled_tickBox.isChecked()
		settings.lenny64_account_email = self.lennyAccountEmail_edit.text()
		new_lenny64_pwd = self.lennyPassword_edit.text()
		if new_lenny64_pwd != '': # password change!
			digester = md5()
			digester.update(bytes(new_lenny64_pwd, 'utf-8'))
			settings.lenny64_password_md5 = ''.join('%02x' % x for x in digester.digest())
		settings.METAR_update_interval = timedelta(minutes=self.METARupdateInterval_edit.value())
		settings.FPL_update_interval = timedelta(minutes=self.FPLupdateInterval_edit.value())
		settings.XPDR_mode_if_not_equipped = '0ACS'[self.xpdrModeForNonEquippedAcft_select.currentIndex()]
		settings.FGMS_XPDR_mode_guess = self.guessXpdrModes_tickBox.isChecked() # FUTURE[fgms_xpdr] remove option
		settings.strict_FGFS_ATDs = self.strictAcftTypeDesignators_tickBox.isChecked()
		
		## Solo and teacher session types
		settings.solo_weather_change_interval = timedelta(minutes=self.soloWeatherChangeInterval_edit.value())
		stripped_lines = [s.strip() for s in self.soloAircraftTypes_edit.toPlainText().split('\n')]
		settings.solo_aircraft_types = [s for s in stripped_lines if s != '']
		settings.solo_restrict_to_available_liveries = self.restrictAirlineChoiceToLiveries_tickBox.isChecked()
		settings.sphinx_acoustic_model_dir = self.sphinxAcousticModel_edit.text()
		settings.audio_input_device_index = self.audioDeviceIndex_edit.value()
		
		signals.systemSettingsChanged.emit()
		self.accept()




