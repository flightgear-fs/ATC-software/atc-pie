#!/usr/bin/env python3

#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re
import sys
from os import path
from random import randint

from util import some
from data.coords import EarthCoords


# ---------- Constants ----------

extract_lst_file = 'extract.lst'
extract_err_file = 'extract.err'

fmt_dms = '(\\d{1,3})\\.(\\d{1,2})\\.(\\d{1,2})\\.(\\d{,3})'
re_point = re.compile('(?P<lat>[NSns]%s) +(?P<lon>[EWew]%s)|(?P<named>\\w+) +(?P=named)' % (fmt_dms, fmt_dms))

# -------------------------------


def conv_coords(s):
	lat, lon = s.split()
	lat_d, lat_m, lat_s = lat[1:].split('.', maxsplit=2)
	lon_d, lon_m, lon_s = lon[1:].split('.', maxsplit=2)
	return EarthCoords.fromString('%sd%s\'%s%s,%sd%s\'%s%s' \
		% (lat_d, lat_m, lat_s, lat[0].upper(), lon_d, lon_m, lon_s, lon[0].upper()))


def segment_in_limits(p1, p2, centre, radius):
	return all(not isinstance(p, EarthCoords) or p.distanceTo(centre) <= radius for p in (p1, p2))


def read_point(txt):
	match = re_point.fullmatch(txt)
	if match:
		if match.group('named') == None:
			return conv_coords(txt)
		else:
			return match.group('named')
	else:
		raise ValueError('Not a valid point spec: %s' % txt)


def get_segment(txt):
	tokens = txt.split(maxsplit=4)
	if len(tokens) < 4: # len should be 4 or 5
		return None, txt
	try:
		p1 = read_point(' '.join(tokens[0:2]))
		p2 = read_point(' '.join(tokens[2:4]))
		return (p1, p2, tokens[:4]), (tokens[4] if len(tokens) > 4 else '')
	except ValueError as err:
		return None, txt
	


def repl_spaces(txt, repl='_'):
	return re.sub(' ', repl, txt)


def point_to_string(p):
	if isinstance(p, EarthCoords):
		return p.toString()
	else: # named point
		return p




if __name__ == "__main__":
	args = sys.argv[1:]
	if len(args) != 3:
		sys.exit('Usage: %s <sector_file> <centre:radius> <output_dir>' % sys.argv[0])
	elif ':' not in args[1]:
		sys.exit('Bad range spec for sector coverage. Use EarthCoords:NM format.')
	centre, radius = args[1].split(':', maxsplit=1) # only arg left
	limits = EarthCoords.fromString(centre), float(radius)
	sector_file = args[0]
	output_dir = args[-1]
	ferr = open(path.join(output_dir, extract_err_file), 'w')
	with open(sector_file, encoding='iso-8859-1') as fin:
		print('Creating files... ', end='', flush=True)
		in_section = last_file = last_drawing_block = last_coord_spec = None
		src_line_number = 0
		files = {} # file name -> file
		object_counters = {} # file name -> int
		for src_line in fin:
			src_line_number += 1
			line = src_line.split(';', maxsplit=1)[0].rstrip()
			
			# --- Interpret spec line --- #
			# must set got_segment match, and if not None must set: output_file, drawing_block, block_colour
			
			if line.startswith('[') and line.endswith(']'):
				in_section = line[1:-1]
				got_segment = None
			
			# --------- GEO --------- #
			elif in_section == 'GEO':
				got_segment, drawing_block = get_segment(line)
				if got_segment:
					if drawing_block == '':
						drawing_block = 'unnamed'
					output_file = 'geo-%s' % repl_spaces(drawing_block)
					block_colour = 'yellow'
				elif line != '':
					ferr.write('Bad GEO spec on line %d: %s' % (src_line_number, src_line))
			
			# ---- ARTCC (HI/LO), SID, STAR ---- #
			elif in_section != None and in_section.startswith('ARTCC') or in_section in ['SID', 'STAR']:
				err_msg = None
				if line.startswith(' '): # indented sequal to prev. line
					if last_drawing_block == None:
						got_segment = None
						err_msg = 'Isolated %s segment on line %d (is header commented out?)\n' % (in_section, src_line_number)
					else:
						drawing_block = last_drawing_block
						got_segment, rest_of_line = get_segment(line)
				else:
					if in_section in ['SID', 'STAR']:
						drawing_block = line[:26].strip()
						got_segment, rest_of_line = get_segment(line[26:])
					else: # in an ARTCC section
						line_split = [token.strip() for token in line.split(' ', maxsplit=1)] # min len is 1
						drawing_block = line_split[0]
						try:
							got_segment, rest_of_line = get_segment(line_split[1])
						except IndexError:
							got_segment = None
				if got_segment:
					if in_section in ['SID', 'STAR']:
						output_file = 'proc-%s' % in_section
						block_colour = {'SID':'#%02X%02XFF', 'STAR':'#FF%02X%02X'}[in_section] % (randint(0, 0xDD), randint(0, 0xDD))
					else: # ARTCC
						output_file = 'boundaries-%s' % {'ARTCC': 'main', 'ARTCC HIGH': 'high', 'ARTCC LOW': 'low'}.get(in_section, 'unknown')
						block_colour = 'cyan'
				elif line != '':
					ferr.write(some(err_msg, 'Bad spec on line %d: %s' % (src_line_number, src_line)))
			
			else: # unhandled section or out of section
				got_segment = None
				
			# ---- Write if necessary ---- #
			# must set: last_output_file, last_drawing_block, last_coord_spec
			
			if got_segment:
				point1, point2, coords_spec = got_segment
			if got_segment and segment_in_limits(point1, point2, *limits):
				try:
					fout = files[output_file]
				except KeyError:
					fout = files[output_file] = open(path.join(output_dir, output_file), 'w')
					object_counters[output_file] = 0
				if output_file == last_output_file and drawing_block == last_drawing_block \
							and coords_spec[0] == last_coord_spec[0] and coords_spec[1] == last_coord_spec[1]:
					fout.write('%s\n' % point_to_string(point2))
				else:
					fout.write('\n%s\n' % block_colour)
					fout.write('%s  %s @%d\n' % (point_to_string(point1), drawing_block, src_line_number))
					fout.write('%s\n' % point_to_string(point2))
					object_counters[output_file] += 1
				last_output_file = output_file
				last_drawing_block = drawing_block
				last_coord_spec = coords_spec[2:4]
			else:
				last_output_file = last_drawing_block = last_coord_spec = None
		
		ferr.close()
		print('%d files written.' % len(files))
	with open(path.join(output_dir, extract_lst_file), 'w') as flst:
		for fname in sorted(files):
			flst.write('%s\tDRAW\t%s (%d objects)\n' % (fname, fname, object_counters[fname]))
			files[fname].close()
	print('Created %s file.' % extract_lst_file)



