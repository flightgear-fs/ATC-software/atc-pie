
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from random import choice
from heapq import heappush, heappop


# ---------- Constants ----------

# -------------------------------


# =========================================
#   VALUE COMPARISONS and PAIRS/INTERVALS
# =========================================

def some(value, fallback):
	if value != None:
		return value
	return fallback


def nofail(f, *args):
	try:
		return f(*args)
	except:
		return None


def rounded(value, step=1):
	return step * int((value + step / 2) / step)


def bounded(lower, value, upper):
	return min(max(lower, value), upper)


def ordered_pair(a, b):
	if a <= b:
		return a, b
	else:
		return b, a


def intervals_intersect(i1, i2):
	return i1[1] >= i2[0] and i2[1] >= i1[0]




# ===========
#    LISTS
# ===========


def pop_all(lst, pred):
	i = 0
	result = []
	while i < len(lst):
		if pred(lst[i]):
			result.append(lst.pop(i))
		else:
			i+= 1
	return result


def discard_one(lst, pred):
	i = 0
	while i < len(lst):
		if pred(lst[i]):
			del lst[i]
			return
		i += 1


def all_diff(lst):
	return len(lst) == len(set(lst))


class PriorityQueue:
	def __init__(self):
		self.elements = []
	
	def empty(self):
		return len(self.elements) == 0
	
	def put(self, item, priority):
		heappush(self.elements, (priority, item))
	
	def get(self):
		return heappop(self.elements)[1]



# =============
#    STRINGS
# =============

def random_string(length, chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ'):
	result = ''
	for i in range(length):
		result += choice(chars)
	return result

