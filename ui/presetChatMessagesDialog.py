# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'presetChatMessagesDialog.ui'
#
# Created: Wed Nov 25 15:01:00 2015
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_presetChatMessagesDialog(object):
    def setupUi(self, presetChatMessagesDialog):
        presetChatMessagesDialog.setObjectName("presetChatMessagesDialog")
        presetChatMessagesDialog.resize(405, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(presetChatMessagesDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.messageList_view = QtWidgets.QListView(presetChatMessagesDialog)
        self.messageList_view.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.messageList_view.setObjectName("messageList_view")
        self.verticalLayout.addWidget(self.messageList_view)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.add_button = QtWidgets.QToolButton(presetChatMessagesDialog)
        self.add_button.setObjectName("add_button")
        self.horizontalLayout_2.addWidget(self.add_button)
        self.remove_button = QtWidgets.QToolButton(presetChatMessagesDialog)
        self.remove_button.setObjectName("remove_button")
        self.horizontalLayout_2.addWidget(self.remove_button)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.close_button = QtWidgets.QPushButton(presetChatMessagesDialog)
        self.close_button.setObjectName("close_button")
        self.horizontalLayout_2.addWidget(self.close_button)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(presetChatMessagesDialog)
        self.close_button.clicked.connect(presetChatMessagesDialog.accept)
        QtCore.QMetaObject.connectSlotsByName(presetChatMessagesDialog)

    def retranslateUi(self, presetChatMessagesDialog):
        _translate = QtCore.QCoreApplication.translate
        presetChatMessagesDialog.setWindowTitle(_translate("presetChatMessagesDialog", "Preset text chat messages"))
        self.add_button.setText(_translate("presetChatMessagesDialog", "+"))
        self.remove_button.setText(_translate("presetChatMessagesDialog", "-"))
        self.close_button.setText(_translate("presetChatMessagesDialog", "Close"))

