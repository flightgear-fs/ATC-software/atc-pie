# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'discardedStripsDialog.ui'
#
# Created: Fri Jan 13 17:29:48 2017
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_discardedStripsDialog(object):
    def setupUi(self, discardedStripsDialog):
        discardedStripsDialog.setObjectName("discardedStripsDialog")
        discardedStripsDialog.resize(387, 292)
        self.verticalLayout = QtWidgets.QVBoxLayout(discardedStripsDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.strip_view = QtWidgets.QListView(discardedStripsDialog)
        self.strip_view.setDragEnabled(True)
        self.strip_view.setDragDropMode(QtWidgets.QAbstractItemView.DragOnly)
        self.strip_view.setObjectName("strip_view")
        self.verticalLayout.addWidget(self.strip_view)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.recall_button = QtWidgets.QPushButton(discardedStripsDialog)
        self.recall_button.setObjectName("recall_button")
        self.horizontalLayout_2.addWidget(self.recall_button)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.clear_button = QtWidgets.QToolButton(discardedStripsDialog)
        self.clear_button.setObjectName("clear_button")
        self.horizontalLayout_2.addWidget(self.clear_button)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.close_button = QtWidgets.QPushButton(discardedStripsDialog)
        self.close_button.setObjectName("close_button")
        self.horizontalLayout_2.addWidget(self.close_button)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(discardedStripsDialog)
        self.close_button.clicked.connect(discardedStripsDialog.accept)
        QtCore.QMetaObject.connectSlotsByName(discardedStripsDialog)

    def retranslateUi(self, discardedStripsDialog):
        _translate = QtCore.QCoreApplication.translate
        discardedStripsDialog.setWindowTitle(_translate("discardedStripsDialog", "Discarded strips"))
        self.recall_button.setText(_translate("discardedStripsDialog", "Recall"))
        self.clear_button.setText(_translate("discardedStripsDialog", "Clear list"))
        self.close_button.setText(_translate("discardedStripsDialog", "Close"))

