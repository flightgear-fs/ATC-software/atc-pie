# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'teachingConsole.ui'
#
# Created: Mon Apr 17 10:33:46 2017
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_teachingConsole(object):
    def setupUi(self, teachingConsole):
        teachingConsole.setObjectName("teachingConsole")
        teachingConsole.resize(325, 295)
        self.verticalLayout = QtWidgets.QVBoxLayout(teachingConsole)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tabs = QtWidgets.QTabWidget(teachingConsole)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabs.sizePolicy().hasHeightForWidth())
        self.tabs.setSizePolicy(sizePolicy)
        self.tabs.setObjectName("tabs")
        self.selectedTraffic_tab = QtWidgets.QWidget()
        self.selectedTraffic_tab.setObjectName("selectedTraffic_tab")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.selectedTraffic_tab)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.selectedCallsign_info = QtWidgets.QLabel(self.selectedTraffic_tab)
        self.selectedCallsign_info.setObjectName("selectedCallsign_info")
        self.horizontalLayout_2.addWidget(self.selectedCallsign_info)
        spacerItem = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.freezeACFT_button = QtWidgets.QToolButton(self.selectedTraffic_tab)
        self.freezeACFT_button.setCheckable(True)
        self.freezeACFT_button.setObjectName("freezeACFT_button")
        self.horizontalLayout_2.addWidget(self.freezeACFT_button)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.spawn_button = QtWidgets.QToolButton(self.selectedTraffic_tab)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.spawn_button.setFont(font)
        self.spawn_button.setObjectName("spawn_button")
        self.horizontalLayout_2.addWidget(self.spawn_button)
        self.kill_button = QtWidgets.QToolButton(self.selectedTraffic_tab)
        self.kill_button.setObjectName("kill_button")
        self.horizontalLayout_2.addWidget(self.kill_button)
        self.verticalLayout_2.addLayout(self.horizontalLayout_2)
        self.line = QtWidgets.QFrame(self.selectedTraffic_tab)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_2.addWidget(self.line)
        self.groupBox = QtWidgets.QGroupBox(self.selectedTraffic_tab)
        self.groupBox.setObjectName("groupBox")
        self.formLayout = QtWidgets.QFormLayout(self.groupBox)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.xpdrMode_select_0 = QtWidgets.QRadioButton(self.groupBox)
        self.xpdrMode_select_0.setObjectName("xpdrMode_select_0")
        self.horizontalLayout.addWidget(self.xpdrMode_select_0)
        self.xpdrMode_select_G = QtWidgets.QRadioButton(self.groupBox)
        self.xpdrMode_select_G.setObjectName("xpdrMode_select_G")
        self.horizontalLayout.addWidget(self.xpdrMode_select_G)
        self.xpdrMode_select_A = QtWidgets.QRadioButton(self.groupBox)
        self.xpdrMode_select_A.setObjectName("xpdrMode_select_A")
        self.horizontalLayout.addWidget(self.xpdrMode_select_A)
        self.xpdrMode_select_C = QtWidgets.QRadioButton(self.groupBox)
        self.xpdrMode_select_C.setChecked(False)
        self.xpdrMode_select_C.setObjectName("xpdrMode_select_C")
        self.horizontalLayout.addWidget(self.xpdrMode_select_C)
        self.xpdrMode_select_S = QtWidgets.QRadioButton(self.groupBox)
        self.xpdrMode_select_S.setObjectName("xpdrMode_select_S")
        self.horizontalLayout.addWidget(self.xpdrMode_select_S)
        self.formLayout.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout)
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.xpdrCode_select = XpdrCodeSelectorWidget(self.groupBox)
        self.xpdrCode_select.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.xpdrCode_select.setObjectName("xpdrCode_select")
        self.horizontalLayout_7.addWidget(self.xpdrCode_select)
        self.pushSQtoStrip_button = QtWidgets.QToolButton(self.groupBox)
        self.pushSQtoStrip_button.setObjectName("pushSQtoStrip_button")
        self.horizontalLayout_7.addWidget(self.pushSQtoStrip_button)
        self.formLayout.setLayout(1, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_7)
        self.label_3 = QtWidgets.QLabel(self.groupBox)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.xpdrIdent_tickBox = QtWidgets.QCheckBox(self.groupBox)
        self.xpdrIdent_tickBox.setCheckable(True)
        self.xpdrIdent_tickBox.setObjectName("xpdrIdent_tickBox")
        self.horizontalLayout_4.addWidget(self.xpdrIdent_tickBox)
        self.squawkVFR_button = QtWidgets.QToolButton(self.groupBox)
        self.squawkVFR_button.setObjectName("squawkVFR_button")
        self.horizontalLayout_4.addWidget(self.squawkVFR_button)
        self.formLayout.setLayout(2, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_4)
        self.verticalLayout_2.addWidget(self.groupBox)
        self.tabs.addTab(self.selectedTraffic_tab, "")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout = QtWidgets.QGridLayout(self.tab)
        self.gridLayout.setObjectName("gridLayout")
        self.ATC_select = QtWidgets.QComboBox(self.tab)
        self.ATC_select.setEditable(True)
        self.ATC_select.setObjectName("ATC_select")
        self.gridLayout.addWidget(self.ATC_select, 0, 0, 1, 2)
        self.addATC_button = QtWidgets.QToolButton(self.tab)
        self.addATC_button.setObjectName("addATC_button")
        self.gridLayout.addWidget(self.addATC_button, 0, 2, 1, 1)
        self.removeATC_button = QtWidgets.QToolButton(self.tab)
        self.removeATC_button.setObjectName("removeATC_button")
        self.gridLayout.addWidget(self.removeATC_button, 0, 3, 1, 1)
        self.frequency_tickBox = QtWidgets.QCheckBox(self.tab)
        self.frequency_tickBox.setObjectName("frequency_tickBox")
        self.gridLayout.addWidget(self.frequency_tickBox, 1, 0, 1, 1)
        self.frequency_select = FrequencyPickCombo(self.tab)
        self.frequency_select.setEnabled(False)
        self.frequency_select.setObjectName("frequency_select")
        self.gridLayout.addWidget(self.frequency_select, 1, 1, 1, 3)
        self.tabs.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.tab_2)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.groupBox_2 = QtWidgets.QGroupBox(self.tab_2)
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.groupBox_2)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.windVRB_radioButton = QtWidgets.QRadioButton(self.groupBox_2)
        self.windVRB_radioButton.setObjectName("windVRB_radioButton")
        self.gridLayout_3.addWidget(self.windVRB_radioButton, 2, 0, 1, 1)
        self.windHdg_radioButton = QtWidgets.QRadioButton(self.groupBox_2)
        self.windHdg_radioButton.setObjectName("windHdg_radioButton")
        self.gridLayout_3.addWidget(self.windHdg_radioButton, 1, 0, 1, 1)
        self.windCalm_radioButton = QtWidgets.QRadioButton(self.groupBox_2)
        self.windCalm_radioButton.setChecked(True)
        self.windCalm_radioButton.setObjectName("windCalm_radioButton")
        self.gridLayout_3.addWidget(self.windCalm_radioButton, 0, 0, 1, 1)
        self.widget = QtWidgets.QWidget(self.groupBox_2)
        self.widget.setEnabled(False)
        self.widget.setObjectName("widget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.widget)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.windHdg_dial = QtWidgets.QDial(self.widget)
        self.windHdg_dial.setMinimum(0)
        self.windHdg_dial.setMaximum(72)
        self.windHdg_dial.setSingleStep(5)
        self.windHdg_dial.setProperty("value", 36)
        self.windHdg_dial.setWrapping(True)
        self.windHdg_dial.setNotchesVisible(True)
        self.windHdg_dial.setObjectName("windHdg_dial")
        self.gridLayout_2.addWidget(self.windHdg_dial, 0, 0, 2, 1)
        self.windHdgRange_enable = QtWidgets.QCheckBox(self.widget)
        self.windHdgRange_enable.setObjectName("windHdgRange_enable")
        self.gridLayout_2.addWidget(self.windHdgRange_enable, 0, 1, 1, 1)
        self.windHdgRange_edit = QtWidgets.QSpinBox(self.widget)
        self.windHdgRange_edit.setEnabled(False)
        self.windHdgRange_edit.setMinimum(10)
        self.windHdgRange_edit.setMaximum(80)
        self.windHdgRange_edit.setSingleStep(5)
        self.windHdgRange_edit.setObjectName("windHdgRange_edit")
        self.gridLayout_2.addWidget(self.windHdgRange_edit, 1, 1, 1, 1)
        self.gridLayout_3.addWidget(self.widget, 0, 1, 3, 1)
        self.verticalLayout_4.addLayout(self.gridLayout_3)
        self.line_4 = QtWidgets.QFrame(self.groupBox_2)
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.verticalLayout_4.addWidget(self.line_4)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.widget_2 = QtWidgets.QWidget(self.groupBox_2)
        self.widget_2.setEnabled(False)
        self.widget_2.setObjectName("widget_2")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.widget_2)
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.windSpeed_edit = QtWidgets.QSpinBox(self.widget_2)
        self.windSpeed_edit.setMinimum(1)
        self.windSpeed_edit.setMaximum(50)
        self.windSpeed_edit.setProperty("value", 5)
        self.windSpeed_edit.setObjectName("windSpeed_edit")
        self.horizontalLayout_5.addWidget(self.windSpeed_edit)
        self.windGusts_enable = QtWidgets.QCheckBox(self.widget_2)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.windGusts_enable.sizePolicy().hasHeightForWidth())
        self.windGusts_enable.setSizePolicy(sizePolicy)
        self.windGusts_enable.setObjectName("windGusts_enable")
        self.horizontalLayout_5.addWidget(self.windGusts_enable)
        self.windGusts_edit = QtWidgets.QSpinBox(self.widget_2)
        self.windGusts_edit.setEnabled(False)
        self.windGusts_edit.setMinimum(10)
        self.windGusts_edit.setProperty("value", 15)
        self.windGusts_edit.setObjectName("windGusts_edit")
        self.horizontalLayout_5.addWidget(self.windGusts_edit)
        self.horizontalLayout_6.addWidget(self.widget_2)
        spacerItem2 = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem2)
        self.setWind_button = QtWidgets.QToolButton(self.groupBox_2)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.setWind_button.setFont(font)
        self.setWind_button.setObjectName("setWind_button")
        self.horizontalLayout_6.addWidget(self.setWind_button)
        self.verticalLayout_4.addLayout(self.horizontalLayout_6)
        self.verticalLayout_3.addWidget(self.groupBox_2)
        self.tabs.addTab(self.tab_2, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.tab_3)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.situationSnapshots_tableView = QtWidgets.QTableView(self.tab_3)
        self.situationSnapshots_tableView.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.situationSnapshots_tableView.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.situationSnapshots_tableView.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.situationSnapshots_tableView.setObjectName("situationSnapshots_tableView")
        self.situationSnapshots_tableView.horizontalHeader().setStretchLastSection(True)
        self.situationSnapshots_tableView.verticalHeader().setVisible(False)
        self.verticalLayout_5.addWidget(self.situationSnapshots_tableView)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.snapshotSituation_button = QtWidgets.QToolButton(self.tab_3)
        self.snapshotSituation_button.setObjectName("snapshotSituation_button")
        self.horizontalLayout_3.addWidget(self.snapshotSituation_button)
        self.restoreSituation_button = QtWidgets.QToolButton(self.tab_3)
        self.restoreSituation_button.setObjectName("restoreSituation_button")
        self.horizontalLayout_3.addWidget(self.restoreSituation_button)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem3)
        self.removeSituation_button = QtWidgets.QToolButton(self.tab_3)
        self.removeSituation_button.setObjectName("removeSituation_button")
        self.horizontalLayout_3.addWidget(self.removeSituation_button)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.tabs.addTab(self.tab_3, "")
        self.verticalLayout.addWidget(self.tabs)
        self.pauseSim_button = QtWidgets.QPushButton(teachingConsole)
        self.pauseSim_button.setCheckable(True)
        self.pauseSim_button.setObjectName("pauseSim_button")
        self.verticalLayout.addWidget(self.pauseSim_button)
        self.label.setBuddy(self.xpdrMode_select_0)
        self.label_2.setBuddy(self.xpdrCode_select)
        self.label_3.setBuddy(self.xpdrIdent_tickBox)

        self.retranslateUi(teachingConsole)
        self.tabs.setCurrentIndex(0)
        self.frequency_tickBox.toggled['bool'].connect(self.frequency_select.setEnabled)
        self.frequency_tickBox.toggled['bool'].connect(self.frequency_select.setFocus)
        self.windHdg_radioButton.toggled['bool'].connect(self.widget.setEnabled)
        self.windHdgRange_enable.toggled['bool'].connect(self.windHdgRange_edit.setEnabled)
        self.windHdgRange_enable.toggled['bool'].connect(self.windHdgRange_edit.setFocus)
        self.windHdg_radioButton.toggled['bool'].connect(self.windHdg_dial.setFocus)
        self.windGusts_enable.toggled['bool'].connect(self.windGusts_edit.setEnabled)
        self.windGusts_enable.toggled['bool'].connect(self.windGusts_edit.setFocus)
        self.windCalm_radioButton.toggled['bool'].connect(self.widget_2.setDisabled)
        QtCore.QMetaObject.connectSlotsByName(teachingConsole)
        teachingConsole.setTabOrder(self.tabs, self.freezeACFT_button)
        teachingConsole.setTabOrder(self.freezeACFT_button, self.spawn_button)
        teachingConsole.setTabOrder(self.spawn_button, self.kill_button)
        teachingConsole.setTabOrder(self.kill_button, self.xpdrMode_select_0)
        teachingConsole.setTabOrder(self.xpdrMode_select_0, self.xpdrMode_select_G)
        teachingConsole.setTabOrder(self.xpdrMode_select_G, self.xpdrMode_select_A)
        teachingConsole.setTabOrder(self.xpdrMode_select_A, self.xpdrMode_select_C)
        teachingConsole.setTabOrder(self.xpdrMode_select_C, self.xpdrMode_select_S)
        teachingConsole.setTabOrder(self.xpdrMode_select_S, self.xpdrCode_select)
        teachingConsole.setTabOrder(self.xpdrCode_select, self.pushSQtoStrip_button)
        teachingConsole.setTabOrder(self.pushSQtoStrip_button, self.xpdrIdent_tickBox)
        teachingConsole.setTabOrder(self.xpdrIdent_tickBox, self.squawkVFR_button)
        teachingConsole.setTabOrder(self.squawkVFR_button, self.ATC_select)
        teachingConsole.setTabOrder(self.ATC_select, self.addATC_button)
        teachingConsole.setTabOrder(self.addATC_button, self.removeATC_button)
        teachingConsole.setTabOrder(self.removeATC_button, self.frequency_tickBox)
        teachingConsole.setTabOrder(self.frequency_tickBox, self.frequency_select)
        teachingConsole.setTabOrder(self.frequency_select, self.windCalm_radioButton)
        teachingConsole.setTabOrder(self.windCalm_radioButton, self.windHdg_radioButton)
        teachingConsole.setTabOrder(self.windHdg_radioButton, self.windVRB_radioButton)
        teachingConsole.setTabOrder(self.windVRB_radioButton, self.windHdg_dial)
        teachingConsole.setTabOrder(self.windHdg_dial, self.windHdgRange_enable)
        teachingConsole.setTabOrder(self.windHdgRange_enable, self.windHdgRange_edit)
        teachingConsole.setTabOrder(self.windHdgRange_edit, self.windSpeed_edit)
        teachingConsole.setTabOrder(self.windSpeed_edit, self.windGusts_enable)
        teachingConsole.setTabOrder(self.windGusts_enable, self.windGusts_edit)
        teachingConsole.setTabOrder(self.windGusts_edit, self.setWind_button)
        teachingConsole.setTabOrder(self.setWind_button, self.situationSnapshots_tableView)
        teachingConsole.setTabOrder(self.situationSnapshots_tableView, self.snapshotSituation_button)
        teachingConsole.setTabOrder(self.snapshotSituation_button, self.restoreSituation_button)
        teachingConsole.setTabOrder(self.restoreSituation_button, self.removeSituation_button)
        teachingConsole.setTabOrder(self.removeSituation_button, self.pauseSim_button)

    def retranslateUi(self, teachingConsole):
        _translate = QtCore.QCoreApplication.translate
        self.selectedCallsign_info.setText(_translate("teachingConsole", "N/A"))
        self.freezeACFT_button.setText(_translate("teachingConsole", "Freeze"))
        self.spawn_button.setText(_translate("teachingConsole", "Spawn"))
        self.kill_button.setText(_translate("teachingConsole", "Kill"))
        self.groupBox.setTitle(_translate("teachingConsole", "Transponder"))
        self.label.setText(_translate("teachingConsole", "Mode:"))
        self.xpdrMode_select_0.setText(_translate("teachingConsole", "0"))
        self.xpdrMode_select_G.setText(_translate("teachingConsole", "G"))
        self.xpdrMode_select_A.setText(_translate("teachingConsole", "A"))
        self.xpdrMode_select_C.setText(_translate("teachingConsole", "C"))
        self.xpdrMode_select_S.setText(_translate("teachingConsole", "S"))
        self.label_2.setText(_translate("teachingConsole", "Code:"))
        self.pushSQtoStrip_button.setText(_translate("teachingConsole", "To strip"))
        self.label_3.setText(_translate("teachingConsole", "Options:"))
        self.xpdrIdent_tickBox.setText(_translate("teachingConsole", "Ident"))
        self.squawkVFR_button.setText(_translate("teachingConsole", "VFR"))
        self.tabs.setTabText(self.tabs.indexOf(self.selectedTraffic_tab), _translate("teachingConsole", "Selected traffic"))
        self.addATC_button.setText(_translate("teachingConsole", "+"))
        self.removeATC_button.setText(_translate("teachingConsole", "-"))
        self.frequency_tickBox.setText(_translate("teachingConsole", "Set frq:"))
        self.tabs.setTabText(self.tabs.indexOf(self.tab), _translate("teachingConsole", "Neighbours"))
        self.groupBox_2.setTitle(_translate("teachingConsole", "Wind"))
        self.windVRB_radioButton.setText(_translate("teachingConsole", "VRB"))
        self.windHdg_radioButton.setText(_translate("teachingConsole", "###"))
        self.windCalm_radioButton.setText(_translate("teachingConsole", "Calm"))
        self.windHdg_dial.setToolTip(_translate("teachingConsole", "Select blowing direction"))
        self.windHdgRange_enable.setText(_translate("teachingConsole", "+/-"))
        self.windHdgRange_edit.setSuffix(_translate("teachingConsole", "°"))
        self.windSpeed_edit.setSuffix(_translate("teachingConsole", " kt"))
        self.windGusts_enable.setText(_translate("teachingConsole", "gusts"))
        self.windGusts_edit.setSuffix(_translate("teachingConsole", " kt"))
        self.setWind_button.setText(_translate("teachingConsole", "Set"))
        self.tabs.setTabText(self.tabs.indexOf(self.tab_2), _translate("teachingConsole", "Weather"))
        self.snapshotSituation_button.setText(_translate("teachingConsole", "Snapshot current situation"))
        self.restoreSituation_button.setText(_translate("teachingConsole", "Restore"))
        self.removeSituation_button.setText(_translate("teachingConsole", "Remove"))
        self.tabs.setTabText(self.tabs.indexOf(self.tab_3), _translate("teachingConsole", "Snapshots"))
        self.pauseSim_button.setText(_translate("teachingConsole", "Pause session"))

from GUI.widgets.miscWidgets import FrequencyPickCombo, XpdrCodeSelectorWidget
