# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'envInfoDialog.ui'
#
# Created: Sun Apr 30 12:20:58 2017
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_envInfoDialog(object):
    def setupUi(self, envInfoDialog):
        envInfoDialog.setObjectName("envInfoDialog")
        envInfoDialog.resize(264, 136)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(envInfoDialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.contents_layout = QtWidgets.QVBoxLayout()
        self.contents_layout.setObjectName("contents_layout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(envInfoDialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.magneticDeclination_infoLabel = QtWidgets.QLabel(envInfoDialog)
        self.magneticDeclination_infoLabel.setObjectName("magneticDeclination_infoLabel")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.magneticDeclination_infoLabel)
        self.line = QtWidgets.QFrame(envInfoDialog)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.SpanningRole, self.line)
        self.label_3 = QtWidgets.QLabel(envInfoDialog)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.airportName_infoLabel = QtWidgets.QLabel(envInfoDialog)
        self.airportName_infoLabel.setObjectName("airportName_infoLabel")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.airportName_infoLabel)
        self.label_2 = QtWidgets.QLabel(envInfoDialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.airportElevation_infoLabel = QtWidgets.QLabel(envInfoDialog)
        self.airportElevation_infoLabel.setObjectName("airportElevation_infoLabel")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.airportElevation_infoLabel)
        self.label_4 = QtWidgets.QLabel(envInfoDialog)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.selectedRunways_infoLabel = QtWidgets.QLabel(envInfoDialog)
        self.selectedRunways_infoLabel.setObjectName("selectedRunways_infoLabel")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.selectedRunways_infoLabel)
        self.contents_layout.addLayout(self.formLayout)
        self.verticalLayout_2.addLayout(self.contents_layout)
        self.OK_button = QtWidgets.QPushButton(envInfoDialog)
        self.OK_button.setObjectName("OK_button")
        self.verticalLayout_2.addWidget(self.OK_button)

        self.retranslateUi(envInfoDialog)
        self.OK_button.clicked.connect(envInfoDialog.accept)
        QtCore.QMetaObject.connectSlotsByName(envInfoDialog)

    def retranslateUi(self, envInfoDialog):
        _translate = QtCore.QCoreApplication.translate
        envInfoDialog.setWindowTitle(_translate("envInfoDialog", "Environment information"))
        self.label.setText(_translate("envInfoDialog", "Magnetic declination:"))
        self.magneticDeclination_infoLabel.setText(_translate("envInfoDialog", "N/A"))
        self.label_3.setText(_translate("envInfoDialog", "Base airport name:"))
        self.airportName_infoLabel.setText(_translate("envInfoDialog", "N/A"))
        self.label_2.setText(_translate("envInfoDialog", "Field elevation:"))
        self.airportElevation_infoLabel.setText(_translate("envInfoDialog", "N/A"))
        self.label_4.setText(_translate("envInfoDialog", "Runways selected:"))
        self.selectedRunways_infoLabel.setText(_translate("envInfoDialog", "N/A"))
        self.OK_button.setText(_translate("envInfoDialog", "OK"))

