# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FPLlist.ui'
#
# Created: Wed Sep 28 19:01:46 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_FPLlistFrame(object):
    def setupUi(self, FPLlistFrame):
        FPLlistFrame.setObjectName("FPLlistFrame")
        FPLlistFrame.resize(296, 401)
        self.verticalLayout = QtWidgets.QVBoxLayout(FPLlistFrame)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(FPLlistFrame)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.filterCallsign_edit = QtWidgets.QLineEdit(FPLlistFrame)
        self.filterCallsign_edit.setObjectName("filterCallsign_edit")
        self.horizontalLayout.addWidget(self.filterCallsign_edit)
        self.filterDate_button = QtWidgets.QToolButton(FPLlistFrame)
        self.filterDate_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.filterDate_button.setObjectName("filterDate_button")
        self.horizontalLayout.addWidget(self.filterDate_button)
        self.filterArrDep_button = QtWidgets.QToolButton(FPLlistFrame)
        self.filterArrDep_button.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        self.filterArrDep_button.setObjectName("filterArrDep_button")
        self.horizontalLayout.addWidget(self.filterArrDep_button)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.checkNow_button = QtWidgets.QToolButton(FPLlistFrame)
        self.checkNow_button.setObjectName("checkNow_button")
        self.horizontalLayout.addWidget(self.checkNow_button)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.list_view = FPLlistView(FPLlistFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.list_view.sizePolicy().hasHeightForWidth())
        self.list_view.setSizePolicy(sizePolicy)
        self.list_view.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.list_view.setAlternatingRowColors(False)
        self.list_view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.list_view.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.list_view.setObjectName("list_view")
        self.list_view.horizontalHeader().setStretchLastSection(True)
        self.list_view.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.list_view)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.newFPL_button = QtWidgets.QToolButton(FPLlistFrame)
        self.newFPL_button.setObjectName("newFPL_button")
        self.horizontalLayout_2.addWidget(self.newFPL_button)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.remove_button = QtWidgets.QToolButton(FPLlistFrame)
        self.remove_button.setEnabled(False)
        self.remove_button.setObjectName("remove_button")
        self.horizontalLayout_2.addWidget(self.remove_button)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.label.setBuddy(self.filterCallsign_edit)

        self.retranslateUi(FPLlistFrame)
        QtCore.QMetaObject.connectSlotsByName(FPLlistFrame)
        FPLlistFrame.setTabOrder(self.list_view, self.filterCallsign_edit)
        FPLlistFrame.setTabOrder(self.filterCallsign_edit, self.filterDate_button)
        FPLlistFrame.setTabOrder(self.filterDate_button, self.filterArrDep_button)
        FPLlistFrame.setTabOrder(self.filterArrDep_button, self.checkNow_button)
        FPLlistFrame.setTabOrder(self.checkNow_button, self.newFPL_button)
        FPLlistFrame.setTabOrder(self.newFPL_button, self.remove_button)

    def retranslateUi(self, FPLlistFrame):
        _translate = QtCore.QCoreApplication.translate
        self.label.setText(_translate("FPLlistFrame", "Filter:"))
        self.filterCallsign_edit.setToolTip(_translate("FPLlistFrame", "Filter by callsign"))
        self.filterCallsign_edit.setPlaceholderText(_translate("FPLlistFrame", "Callsign"))
        self.filterDate_button.setToolTip(_translate("FPLlistFrame", "Filter by scheduled date"))
        self.filterDate_button.setText(_translate("FPLlistFrame", "Date"))
        self.filterArrDep_button.setToolTip(_translate("FPLlistFrame", "Filter by ARR/DEP/alt. airport"))
        self.filterArrDep_button.setText(_translate("FPLlistFrame", "AD"))
        self.checkNow_button.setToolTip(_translate("FPLlistFrame", "Check for unlisted FPLs filed online"))
        self.checkNow_button.setText(_translate("FPLlistFrame", "Check\n"
"now"))
        self.newFPL_button.setText(_translate("FPLlistFrame", "+ FPL"))
        self.remove_button.setText(_translate("FPLlistFrame", "- FPL"))

from GUI.widgets.FPLlistView import FPLlistView
