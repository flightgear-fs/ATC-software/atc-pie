# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'postLennySessionDialog.ui'
#
# Created: Fri Feb 26 17:06:33 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_postLennySessionDialog(object):
    def setupUi(self, postLennySessionDialog):
        postLennySessionDialog.setObjectName("postLennySessionDialog")
        postLennySessionDialog.resize(434, 408)
        self.verticalLayout = QtWidgets.QVBoxLayout(postLennySessionDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_3 = QtWidgets.QLabel(postLennySessionDialog)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.textEdit = QtWidgets.QTextEdit(postLennySessionDialog)
        self.textEdit.setFocusPolicy(QtCore.Qt.NoFocus)
        self.textEdit.setReadOnly(True)
        self.textEdit.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout.addWidget(self.textEdit)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.groupBox = QtWidgets.QGroupBox(postLennySessionDialog)
        self.groupBox.setObjectName("groupBox")
        self.gridLayout = QtWidgets.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(self.groupBox)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.beginTime_edit = QtWidgets.QDateTimeEdit(self.groupBox)
        self.beginTime_edit.setObjectName("beginTime_edit")
        self.gridLayout.addWidget(self.beginTime_edit, 0, 1, 1, 1)
        self.label_10 = QtWidgets.QLabel(self.groupBox)
        self.label_10.setObjectName("label_10")
        self.gridLayout.addWidget(self.label_10, 0, 2, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.groupBox)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.endTime_edit = QtWidgets.QTimeEdit(self.groupBox)
        self.endTime_edit.setObjectName("endTime_edit")
        self.gridLayout.addWidget(self.endTime_edit, 1, 1, 1, 1)
        self.label_11 = QtWidgets.QLabel(self.groupBox)
        self.label_11.setObjectName("label_11")
        self.gridLayout.addWidget(self.label_11, 1, 2, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.groupBox)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 2, 0, 1, 1)
        self.frequency_edit = FrequencyPickCombo(self.groupBox)
        self.frequency_edit.setObjectName("frequency_edit")
        self.gridLayout.addWidget(self.frequency_edit, 2, 1, 1, 1)
        self.label_9 = QtWidgets.QLabel(self.groupBox)
        self.label_9.setObjectName("label_9")
        self.gridLayout.addWidget(self.label_9, 2, 2, 1, 1)
        self.verticalLayout.addWidget(self.groupBox)
        spacerItem1 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.cancel_button = QtWidgets.QPushButton(postLennySessionDialog)
        self.cancel_button.setObjectName("cancel_button")
        self.horizontalLayout.addWidget(self.cancel_button)
        self.announce_button = QtWidgets.QPushButton(postLennySessionDialog)
        self.announce_button.setObjectName("announce_button")
        self.horizontalLayout.addWidget(self.announce_button)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.label.setBuddy(self.beginTime_edit)
        self.label_10.setBuddy(self.beginTime_edit)
        self.label_2.setBuddy(self.endTime_edit)
        self.label_11.setBuddy(self.endTime_edit)
        self.label_4.setBuddy(self.frequency_edit)
        self.label_9.setBuddy(self.frequency_edit)

        self.retranslateUi(postLennySessionDialog)
        QtCore.QMetaObject.connectSlotsByName(postLennySessionDialog)
        postLennySessionDialog.setTabOrder(self.textEdit, self.beginTime_edit)
        postLennySessionDialog.setTabOrder(self.beginTime_edit, self.endTime_edit)
        postLennySessionDialog.setTabOrder(self.endTime_edit, self.frequency_edit)
        postLennySessionDialog.setTabOrder(self.frequency_edit, self.announce_button)
        postLennySessionDialog.setTabOrder(self.announce_button, self.cancel_button)

    def retranslateUi(self, postLennySessionDialog):
        _translate = QtCore.QCoreApplication.translate
        postLennySessionDialog.setWindowTitle(_translate("postLennySessionDialog", "Announce ATC session"))
        self.label_3.setText(_translate("postLennySessionDialog", "READ ME!"))
        self.textEdit.setHtml(_translate("postLennySessionDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">This dialog allows you to announce an ATC session on </span><a href=\"http://flightgear-atc.alwaysdata.net/\"><span style=\" font-size:14pt; text-decoration: underline; color:#0000ff;\">Lenny64\'s website</span></a><span style=\" font-size:14pt;\">, which you plan to host at this location. Pilots will use it to plan flights from/to/through your controlled area.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:14pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">For everyone to keep enjoying this feature, please observe the following rules:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">- </span><span style=\" font-size:14pt; font-weight:600;\">do not post sessions if you are unsure of attending them</span><span style=\" font-size:14pt;\">: pilots also plan arrivals at towered airports after long flights;</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt;\">- </span><span style=\" font-size:14pt; font-weight:600;\">do not post sessions that are too short in time</span><span style=\" font-size:14pt;\">: sessions shorter than two hours are probably better left unannounced.</span></p></body></html>"))
        self.groupBox.setTitle(_translate("postLennySessionDialog", "Session details"))
        self.label.setText(_translate("postLennySessionDialog", "Starting date && time:"))
        self.beginTime_edit.setDisplayFormat(_translate("postLennySessionDialog", "dd/MM/yyyy, HH:mm"))
        self.label_10.setText(_translate("postLennySessionDialog", "UTC"))
        self.label_2.setText(_translate("postLennySessionDialog", "End time:"))
        self.label_11.setText(_translate("postLennySessionDialog", "UTC"))
        self.label_4.setText(_translate("postLennySessionDialog", "Frequency for contact:"))
        self.label_9.setText(_translate("postLennySessionDialog", "(optional)"))
        self.cancel_button.setText(_translate("postLennySessionDialog", "Cancel"))
        self.announce_button.setText(_translate("postLennySessionDialog", "Announce!"))

from GUI.widgets.miscWidgets import FrequencyPickCombo
