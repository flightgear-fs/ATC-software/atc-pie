# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'xpdrCodeSelector.ui'
#
# Created: Thu May  5 09:13:14 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_xpdrCodeSelectorWidget(object):
    def setupUi(self, xpdrCodeSelectorWidget):
        xpdrCodeSelectorWidget.setObjectName("xpdrCodeSelectorWidget")
        xpdrCodeSelectorWidget.resize(163, 42)
        self.horizontalLayout = QtWidgets.QHBoxLayout(xpdrCodeSelectorWidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.xpdrCode_edit = QtWidgets.QSpinBox(xpdrCodeSelectorWidget)
        self.xpdrCode_edit.setWrapping(True)
        self.xpdrCode_edit.setMaximum(7777)
        self.xpdrCode_edit.setProperty("value", 7000)
        self.xpdrCode_edit.setObjectName("xpdrCode_edit")
        self.horizontalLayout.addWidget(self.xpdrCode_edit)
        self.xpdrRange_select = QtWidgets.QComboBox(xpdrCodeSelectorWidget)
        self.xpdrRange_select.setObjectName("xpdrRange_select")
        self.xpdrRange_select.addItem("")
        self.horizontalLayout.addWidget(self.xpdrRange_select)

        self.retranslateUi(xpdrCodeSelectorWidget)
        QtCore.QMetaObject.connectSlotsByName(xpdrCodeSelectorWidget)

    def retranslateUi(self, xpdrCodeSelectorWidget):
        _translate = QtCore.QCoreApplication.translate
        self.xpdrRange_select.setItemText(0, _translate("xpdrCodeSelectorWidget", "From range..."))

