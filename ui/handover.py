# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'handover.ui'
#
# Created: Sun Oct 23 10:17:33 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_handOverPane(object):
    def setupUi(self, handOverPane):
        handOverPane.setObjectName("handOverPane")
        handOverPane.resize(274, 242)
        self.verticalLayout = QtWidgets.QVBoxLayout(handOverPane)
        self.verticalLayout.setObjectName("verticalLayout")
        self.ATC_view = AtcNeighboursTableView(handOverPane)
        self.ATC_view.setAcceptDrops(True)
        self.ATC_view.setDragDropMode(QtWidgets.QAbstractItemView.DropOnly)
        self.ATC_view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.ATC_view.setShowGrid(False)
        self.ATC_view.setObjectName("ATC_view")
        self.ATC_view.horizontalHeader().setVisible(False)
        self.ATC_view.verticalHeader().setVisible(False)
        self.ATC_view.verticalHeader().setDefaultSectionSize(35)
        self.verticalLayout.addWidget(self.ATC_view)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.publiciseFrequency_tickBox = QtWidgets.QCheckBox(handOverPane)
        self.publiciseFrequency_tickBox.setObjectName("publiciseFrequency_tickBox")
        self.horizontalLayout.addWidget(self.publiciseFrequency_tickBox)
        self.publicFrequency_edit = FrequencyPickCombo(handOverPane)
        self.publicFrequency_edit.setEnabled(False)
        self.publicFrequency_edit.setObjectName("publicFrequency_edit")
        self.horizontalLayout.addWidget(self.publicFrequency_edit)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(handOverPane)
        self.publiciseFrequency_tickBox.toggled['bool'].connect(self.publicFrequency_edit.setEnabled)
        self.publiciseFrequency_tickBox.toggled['bool'].connect(self.publicFrequency_edit.setFocus)
        QtCore.QMetaObject.connectSlotsByName(handOverPane)

    def retranslateUi(self, handOverPane):
        _translate = QtCore.QCoreApplication.translate
        self.publiciseFrequency_tickBox.setToolTip(_translate("handOverPane", "Make frequency visible to neighbouring ATCs"))
        self.publiciseFrequency_tickBox.setText(_translate("handOverPane", "Publicise frq:"))

from GUI.widgets.neighboursView import AtcNeighboursTableView
from GUI.widgets.miscWidgets import FrequencyPickCombo
