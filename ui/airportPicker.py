# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'airportPicker.ui'
#
# Created: Wed Nov 25 15:01:01 2015
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_airportPicker(object):
    def setupUi(self, airportPicker):
        airportPicker.setObjectName("airportPicker")
        airportPicker.resize(125, 24)
        self.horizontalLayout = QtWidgets.QHBoxLayout(airportPicker)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.airport_edit = QtWidgets.QLineEdit(airportPicker)
        self.airport_edit.setObjectName("airport_edit")
        self.horizontalLayout.addWidget(self.airport_edit)
        self.search_button = QtWidgets.QToolButton(airportPicker)
        self.search_button.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.search_button.setObjectName("search_button")
        self.horizontalLayout.addWidget(self.search_button)

        self.retranslateUi(airportPicker)
        QtCore.QMetaObject.connectSlotsByName(airportPicker)

    def retranslateUi(self, airportPicker):
        _translate = QtCore.QCoreApplication.translate
        self.search_button.setToolTip(_translate("airportPicker", "Search by name"))
        self.search_button.setText(_translate("airportPicker", "..."))

