# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'editRackDialog.ui'
#
# Created: Sun Nov 27 08:57:52 2016
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_editRackDialog(object):
    def setupUi(self, editRackDialog):
        editRackDialog.setObjectName("editRackDialog")
        editRackDialog.resize(226, 146)
        self.verticalLayout = QtWidgets.QVBoxLayout(editRackDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(editRackDialog)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.rackName_edit = QtWidgets.QLineEdit(editRackDialog)
        self.rackName_edit.setObjectName("rackName_edit")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.rackName_edit)
        self.label_10 = QtWidgets.QLabel(editRackDialog)
        self.label_10.setObjectName("label_10")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_10)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pickColour_button = QtWidgets.QToolButton(editRackDialog)
        self.pickColour_button.setObjectName("pickColour_button")
        self.horizontalLayout.addWidget(self.pickColour_button)
        self.clearColour_button = QtWidgets.QToolButton(editRackDialog)
        self.clearColour_button.setObjectName("clearColour_button")
        self.horizontalLayout.addWidget(self.clearColour_button)
        self.formLayout.setLayout(1, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout)
        self.label_2 = QtWidgets.QLabel(editRackDialog)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.deleteRack_button = QtWidgets.QToolButton(editRackDialog)
        self.deleteRack_button.setCheckable(True)
        self.deleteRack_button.setObjectName("deleteRack_button")
        self.horizontalLayout_2.addWidget(self.deleteRack_button)
        self.rackDeletion_info = QtWidgets.QLabel(editRackDialog)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.rackDeletion_info.setFont(font)
        self.rackDeletion_info.setText("")
        self.rackDeletion_info.setObjectName("rackDeletion_info")
        self.horizontalLayout_2.addWidget(self.rackDeletion_info)
        self.formLayout.setLayout(2, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_2)
        self.verticalLayout.addLayout(self.formLayout)
        self.buttonBox = QtWidgets.QDialogButtonBox(editRackDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)
        self.label.setBuddy(self.rackName_edit)
        self.label_10.setBuddy(self.pickColour_button)

        self.retranslateUi(editRackDialog)
        self.buttonBox.accepted.connect(editRackDialog.accept)
        self.buttonBox.rejected.connect(editRackDialog.reject)
        self.deleteRack_button.toggled['bool'].connect(self.rackName_edit.setDisabled)
        self.deleteRack_button.toggled['bool'].connect(self.clearColour_button.setDisabled)
        self.deleteRack_button.toggled['bool'].connect(self.pickColour_button.setDisabled)
        QtCore.QMetaObject.connectSlotsByName(editRackDialog)
        editRackDialog.setTabOrder(self.rackName_edit, self.pickColour_button)
        editRackDialog.setTabOrder(self.pickColour_button, self.clearColour_button)
        editRackDialog.setTabOrder(self.clearColour_button, self.deleteRack_button)
        editRackDialog.setTabOrder(self.deleteRack_button, self.buttonBox)

    def retranslateUi(self, editRackDialog):
        _translate = QtCore.QCoreApplication.translate
        editRackDialog.setWindowTitle(_translate("editRackDialog", "Edit strip rack"))
        self.label.setText(_translate("editRackDialog", "Rack name:"))
        self.label_10.setText(_translate("editRackDialog", "Link colour:"))
        self.pickColour_button.setToolTip(_translate("editRackDialog", "Pick colour for linked contacts"))
        self.pickColour_button.setText(_translate("editRackDialog", "Pick..."))
        self.clearColour_button.setText(_translate("editRackDialog", "Clear"))
        self.label_2.setText(_translate("editRackDialog", "Delete rack:"))
        self.deleteRack_button.setText(_translate("editRackDialog", "X"))

