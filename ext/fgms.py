
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from os import path
from struct import pack, unpack
from math import radians, pi, cos, sin, acos
from datetime import timedelta
from PyQt5.QtCore import QThread

from util import some
from settings import settings
from game.env import env
from game.manager import GameType # FUTURE[fgms_xpdr]: remove GameType import
from ext.fgfs import ICAO_aircraft_type
from GUI.misc import signals

from data.UTC import read_stopwatch, now
from data.coords import cartesian_metres_to_WGS84_geodetic, WGS84_geodetic_to_cartesian_metres
from data.params import StdPressureAlt
from data.aircraft import Aircraft
from data.chatMsg import ChatMessage


# ---------- Constants ----------

encoding = 'utf-8'
maximum_packet_size = 2048
dodgy_character_substitute = '_'

FGMS_live_ACFT_time = timedelta(seconds=2) # after which ACFT is considered disconnected (to appear as "?" on next sweep)
FGMS_connection_timeout = timedelta(seconds=60) # after which ACFT is considered a disconnected zombie (to be removed)
timestamp_ignore_maxdiff = 10 # s (as specified in FGMS packets)

ATCpie_aircraft_model = 'ATC-pie'
known_ATC_models = [ATCpie_aircraft_model, 'OpenRadar', 'ATC-ML', 'atc-tower']
FGMS_XPDR_mode_spec = { i:c for i, c in enumerate('0GACS') }

fgms_listen_timeout = 1 # seconds

# FGMS packet type codes
position_message_type_code = 7

# -------------------------------




class PacketData:
	'''
	Data packer/unpacker for FGFS stuff
	Includes funny FGFS behaviour like little endian ints and big endian doubles,
	"buggy strings" (encoded with int sequences), etc.
	'''
	def __init__(self, data=None):
		self.data = some(data, bytes(0))
		
	def allData(self):
		return self.data
	def __len__(self):
		return len(self.data)
	
	def pad(self, block_multiple):
		pad = block_multiple - (len(self) % block_multiple)
		self.append_bytes(bytes(pad % block_multiple))

	def pack_bool(self, b):
		self.pack_int(int(b))
	def pack_int(self, i):
		self.data += pack('!i', i)
	def pack_float(self, f):
		self.data += pack('!f', f)
	def pack_double(self, d):
		self.data += pack('!d', d)
	def pack_string(self, size, string): # For padded null-terminated string
		self.data += pack('%ds' % size, bytes(string, encoding)[:size-1])
	def append_bytes(self, raw_data):
		self.data += raw_data
	def append_packed(self, data):
		self.data += data.allData()
	def append_hexbytes(self, data): # Data is a string of hex-represented bytes
		self.data += bytes.fromhex(data)
	def pack_FGFS_buggy_string(self, string):
		strbuf = PacketData()
		for c in string:
			strbuf.pack_int(ord(c))
		strbuf.pad(16)
		self.pack_int(len(string))
		self.append_packed(strbuf)

	def unpack_bytes(self, nbytes):
		popped = self.data[:nbytes]
		self.data = self.data[nbytes:]
		if len(popped) < nbytes:
			print('WARNING: Truncated packet detected. Expected %d bytes; only %d could be read.' % (nbytes, len(popped)))
			return bytes(nbytes)
		return popped
	def unpack_int(self):
		return unpack('!i', self.unpack_bytes(4))[0]
	def unpack_float(self):
		return unpack('!f', self.unpack_bytes(4))[0]
	def unpack_double(self):
		return unpack('!d', self.unpack_bytes(8))[0]
	def unpack_string(self, size):
		return self.unpack_bytes(size).split(b'\x00', 1)[0].decode()
	def unpack_bool(self):
		return self.unpack_int()
	def unpack_FGFS_buggy_string(self):
		nchars = self.unpack_int()
		intbytes = PacketData(self.unpack_bytes((((4 * nchars - 1) // 16) + 1) * 16))
		chrlst = []
		for i in range(nchars):
			try: chrlst.append(chr(intbytes.unpack_int()))
			except ValueError: chrlst.append(dodgy_character_substitute)
		return ''.join(chrlst)








# ----------------

#    ENCODING

# ----------------


def make_fgms_packet(sender_callsign, packet_type, content_data):
	packet = PacketData()
	# Header first (32 bytes)
	packet.append_bytes(b'FGFS') # Magic
	packet.append_hexbytes('00 01 00 01') # Protocol version 1.1
	packet.pack_int(packet_type) # Msg type: position message
	packet.pack_int(32 + len(content_data)) # Length of data
	#packet.append_hexbytes('00 00 00 00') # ReplyAddress: replaced with line below in case FGMS uses it before protocol change
	packet.pack_int(settings.radar_range) # Visibility range (see message 35687340 on the FG devel list)
	packet.append_hexbytes('00 00 00 00') # ReplyPort: ignored
	packet.pack_string(8, sender_callsign) # Callsign
	# Append the data
	packet.append_packed(content_data)
	return packet


def position_data(aircraft_model, pos_coords, pos_amsl, hdg=0, pitch=0, roll=0):
	'''
	pos_coords: EarthCoords
	pos_amsl should be geometric alt in feet
	'''
	buf = PacketData()
	buf.pack_string(96, aircraft_model) # Aircraft model
	buf.pack_double(read_stopwatch()) # Time
	buf.pack_double(.1) # Lag # WARNING zero value can make some FG clients crash (see SF tickets 1927 and 1942)
	posX, posY, posZ = WGS84_geodetic_to_cartesian_metres(pos_coords, pos_amsl)
	buf.pack_double(posX) # PosX
	buf.pack_double(posY) # PosY
	buf.pack_double(posZ) # PosZ
	oriX, oriY, oriZ = FG_orientation_XYZ(pos_coords, hdg, pitch, roll)
	buf.pack_float(oriX) # OriX
	buf.pack_float(oriY) # OriY
	buf.pack_float(oriZ) # OriZ
	buf.pack_float(0) # VelX
	buf.pack_float(0) # VelY
	buf.pack_float(0) # VelZ
	buf.pack_float(0) # AV1
	buf.pack_float(0) # AV2
	buf.pack_float(0) # AV3
	buf.pack_float(0) # LA1
	buf.pack_float(0) # LA2
	buf.pack_float(0) # LA3
	buf.pack_float(0) # AA1
	buf.pack_float(0) # AA2
	buf.pack_float(0) # AA3
	buf.append_hexbytes('00 00 00 00') # 4-byte padding
	return buf








# ----------------

#    DECODING

# ----------------


def decode_FGMS_position_message(packet):
	'''
	Returns a tuple of 7 values decoded from the argument FGMS packet:
	- callsign of the aircraft (FGMS unique identifier)
	- FGMS header time stamp contained
	- model string as recognised by current game setting
	- aircraft position received in header: (EarthCoords, geometric altitude AMSL in ft)
	- the transponder status tuple: (char mode, int code, StdPressureAlt, bool ident) with possible None values
	- the packaged text chat message if any
	- the packaged comm frequency if any
	'''
	buf = PacketData(packet)
	# Header
	got_magic = buf.unpack_bytes(4)
	if got_magic != b'FGFS':
		raise ValueError('Bad magic byte sequence: %s' % got_magic)
	got_protocol_version = buf.unpack_bytes(4)
	if got_protocol_version != bytes.fromhex('00 01 00 01'):
		raise ValueError('Bad protocol version: %s' % got_protocol_version)
	got_msg_type = buf.unpack_bytes(4)
	if got_msg_type != bytes.fromhex('00 00 00 07'):
		raise ValueError('Bad message type: %s' % got_msg_type)
	got_packet_size = buf.unpack_int()
	ignored = buf.unpack_int()
	ignored = buf.unpack_int()
	got_callsign = buf.unpack_string(8)
	
	# Done header. Now obligatory data
	got_model = buf.unpack_string(96)
	got_time = buf.unpack_double()
	got_lag = buf.unpack_double()
	got_posX = buf.unpack_double()
	got_posY = buf.unpack_double()
	got_posZ = buf.unpack_double()
	for i in range(15): # Ori, Vel, AV, LA, AA triplets
		ignored = buf.unpack_bytes(4)
	# NB (cf. FG source file MultiPlayer/multiplaymgr.cxx):
	# FGMS spec says "up to 8 bytes for padding the data to a multiple of 8 bytes",
	# which should make us read 4 bytes of padding at this point. But we do not,
	# for backward compatibility reasons; padding will be ignored anyway.
	#ignored = buf.unpack_bytes(4) # Discard padding
	
	# Done obligatory data, now optional data:
	got_xpdr_mode = got_xpdr_code = got_xpdr_alt = got_xpdr_ident = got_chat_msg = got_comm_frq = None
	while len(buf) >= 4:
		try:
			prop_code = buf.unpack_int()
			read_function = FGMS_properties[prop_code][1]
			prop_value = read_function(buf)
			if prop_code == FGMS_prop_XPDR_mode:
				got_xpdr_mode = prop_value
			elif prop_code == FGMS_prop_XPDR_code:
				got_xpdr_code = prop_value
			elif prop_code == FGMS_prop_XPDR_alt:
				got_xpdr_alt = prop_value
			elif prop_code == FGMS_prop_XPDR_ident:
				got_xpdr_ident = prop_value
			elif prop_code == FGMS_prop_chat_msg:
				got_chat_msg = prop_value
			elif prop_code == FGMS_prop_comm_frq:
				got_comm_frq = prop_value[0:3] + '.' + prop_value[3:6]
		except KeyError:
			pass #print('Skipping unknown property code %d in packet from %s; searching forward...' % (prop_code, got_callsign))
	if len(buf) != 0:
		print('Bytes left over in packet from %s: %s' % (got_callsign, buf.data))
	fgfs_model = path.basename(got_model)
	if fgfs_model.endswith('.xml'):
		fgfs_model = fgfs_model[:-4]
	res_model = fgfs_model if fgfs_model in known_ATC_models else ICAO_aircraft_type(fgfs_model)
	position = cartesian_metres_to_WGS84_geodetic(got_posX, got_posY, got_posZ)
	# decode transponder status
	res_xpdr_mode = FGMS_XPDR_mode_spec.get(got_xpdr_mode, None)
	res_xpdr_code = got_xpdr_code if got_xpdr_code != None and got_xpdr_code >= 0 else None
	res_xpdr_alt = StdPressureAlt.fromAMSL(got_xpdr_alt, 1013.25) if got_xpdr_alt != None and got_xpdr_alt > -999 else None
	res_xpdr_ident = bool(got_xpdr_ident)
	if settings.FGMS_XPDR_mode_guess and settings.game_manager.game_type == GameType.FLIGHTGEAR_MP: # FUTURE[fgms_xpdr]: remove whole "if"
		if got_xpdr_mode == None:
			res_xpdr_mode = None # No XPDR
		elif chr(got_xpdr_mode) in '0GACS': # Recognise ATC-pie "AI aircraft" packets (int 0..4 prop overloaded with ASCII codes)
			res_xpdr_mode = chr(got_xpdr_mode)
		else:
			# FlightGear XPDR mode prop spec very badly followed by aircraft models: many always set to 1 or 2.
			# Relying on other XPDR-related props in packets...
			# NB: modes G and S not identifiable until aircraft models' mode values comply with spec.
			if res_xpdr_code == None:
				res_xpdr_mode = '0'
			elif res_xpdr_alt == None:
				res_xpdr_mode = 'A'
			else:
				res_xpdr_mode = 'C'
	xpdr_status = res_xpdr_mode, res_xpdr_code, res_xpdr_alt, res_xpdr_ident
	# yield the horrible tuple
	return got_callsign, got_time, res_model, position, xpdr_status, got_chat_msg, got_comm_frq
	









# --------------------------------------------------------------------------------------------------



class FgmsAircraft(Aircraft):
	def __init__(self, identifier, acft_type, init_time_stamp, coords, g_alt):
		Aircraft.__init__(self, identifier, acft_type, coords, g_alt)
		self.latest_time_stamp = init_time_stamp
		self.radio_comm_frequency = None
		self.last_chat_message_read = None
	
	def isZombie(self):
		return now() - self.lastLiveUpdateTime() > FGMS_connection_timeout
	
	def isRadarVisible(self): # overriding method
		return Aircraft.isRadarVisible(self) and now() - self.lastLiveUpdateTime() <= FGMS_live_ACFT_time
	
	def receiveChatMessage(self, msg):
		if msg != self.last_chat_message_read:
			self.last_chat_message_read = msg
			signals.incomingTextChat.emit(ChatMessage(self.identifier, msg))




def update_FgmsAircraft_list(ACFT_lst, udp_packet):
	try:
		fgms_identifier, time_stamp, model, position, xpdr_status, chat_msg, pub_frq = decode_FGMS_position_message(udp_packet)
		pos_coords, pos_alt = position
	except ValueError as err:
		print('Ignoring packet: %s' % err)
		return
	try: # Try finding connected aircraft
		fgms_acft = next(acft for acft in ACFT_lst if acft.identifier == fgms_identifier)
	except StopIteration: # Aircraft not found; create it
		fgms_acft = FgmsAircraft(fgms_identifier, model, time_stamp, pos_coords, pos_alt)
		ACFT_lst.append(fgms_acft)
	else: # ACFT was found and needs updating
		# Time stamp
		if fgms_acft.latest_time_stamp - timestamp_ignore_maxdiff < time_stamp < fgms_acft.latest_time_stamp:
			return # Drop unordered UDP packet
		else:
			fgms_acft.latest_time_stamp = time_stamp
		# Aircraft model (change of model string is unlikely, but "filter out unrecognised model" may have been toggled)
		fgms_acft.aircraft_type = model
	fgms_acft.radio_comm_frequency = pub_frq
	fgms_acft.updateLiveStatus(pos_coords, pos_alt, *xpdr_status)
	if chat_msg != None and chat_msg != '':
		fgms_acft.receiveChatMessage(chat_msg)







class FGMShandshaker(QThread):
	def __init__(self, gui, socket, srv_address, callsign):
		QThread.__init__(self, parent=gui)
		self.socket = socket
		self.server_address = srv_address
		self.callsign = callsign
		self.current_chat_msg = ''
	
	def currentChatMessage(self):
		return self.current_chat_msg
	
	def setChatMessage(self, msg):
		self.current_chat_msg = msg
		
	def run(self):
		coords = env.radarPos()
		pos_data = position_data(ATCpie_aircraft_model, coords, env.elevation(coords))
		pos_data.pack_int(FGMS_prop_chat_msg)
		pos_data.pack_FGFS_buggy_string(self.current_chat_msg)
		if settings.publicised_frequency != None:
			pos_data.pack_int(FGMS_prop_comm_frq)
			pos_data.pack_FGFS_buggy_string(settings.publicised_frequency)
		packet = make_fgms_packet(self.callsign, position_message_type_code, pos_data)
		#print('Sending packet with size %d=0x%x bytes. Optional data is: %s' % (len(packet), len(packet), packet.data[228:])) # DEBUG
		try:
			self.socket.sendto(packet.allData(), self.server_address)
		except OSError as error:
			print('Could not send FGMS packet to server. System says: %s' % error)



class FGMSlistener(QThread):
	def __init__(self, parent, socket, callback):
		'''
		The last argument is a function called on every FGMS packet reception.
		'''
		QThread.__init__(self, parent)
		self.socket = socket
		self.process_packet = callback
		
	def run(self):
		self.socket.settimeout(fgms_listen_timeout)
		self.listening = True
		while self.listening:
			try:
				received_packet = self.socket.recv(maximum_packet_size)
				#print('Received packet of %d bytes.' % len(received_packet)) # DEBUG
			except OSError: # this includes the timeout exception from socket.recv
				pass
			else:
				self.process_packet(received_packet)
		self.socket.settimeout(None)
	
	def stop(self):
		self.listening = False









# --------------------------------------------------------------------------------------------------

FGMS_properties = {
  100: ('surface-positions/left-aileron-pos-norm',  PacketData.unpack_float),
  101: ('surface-positions/right-aileron-pos-norm', PacketData.unpack_float),
  102: ('surface-positions/elevator-pos-norm',      PacketData.unpack_float),
  103: ('surface-positions/rudder-pos-norm',        PacketData.unpack_float),
  104: ('surface-positions/flap-pos-norm',          PacketData.unpack_float),
  105: ('surface-positions/speedbrake-pos-norm',    PacketData.unpack_float),
  106: ('gear/tailhook/position-norm',              PacketData.unpack_float),
  107: ('gear/launchbar/position-norm',             PacketData.unpack_float),
  108: ('gear/launchbar/state',                     PacketData.unpack_FGFS_buggy_string),
  109: ('gear/launchbar/holdback-position-norm',    PacketData.unpack_float),
  110: ('canopy/position-norm',                     PacketData.unpack_float),
  111: ('surface-positions/wing-pos-norm',          PacketData.unpack_float),
  112: ('surface-positions/wing-fold-pos-norm',     PacketData.unpack_float),

  200: ('gear/gear[0]/compression-norm',           PacketData.unpack_float),
  201: ('gear/gear[0]/position-norm',              PacketData.unpack_float),
  210: ('gear/gear[1]/compression-norm',           PacketData.unpack_float),
  211: ('gear/gear[1]/position-norm',              PacketData.unpack_float),
  220: ('gear/gear[2]/compression-norm',           PacketData.unpack_float),
  221: ('gear/gear[2]/position-norm',              PacketData.unpack_float),
  230: ('gear/gear[3]/compression-norm',           PacketData.unpack_float),
  231: ('gear/gear[3]/position-norm',              PacketData.unpack_float),
  240: ('gear/gear[4]/compression-norm',           PacketData.unpack_float),
  241: ('gear/gear[4]/position-norm',              PacketData.unpack_float),

  300: ('engines/engine[0]/n1',  PacketData.unpack_float),
  301: ('engines/engine[0]/n2',  PacketData.unpack_float),
  302: ('engines/engine[0]/rpm', PacketData.unpack_float),
  310: ('engines/engine[1]/n1',  PacketData.unpack_float),
  311: ('engines/engine[1]/n2',  PacketData.unpack_float),
  312: ('engines/engine[1]/rpm', PacketData.unpack_float),
  320: ('engines/engine[2]/n1',  PacketData.unpack_float),
  321: ('engines/engine[2]/n2',  PacketData.unpack_float),
  322: ('engines/engine[2]/rpm', PacketData.unpack_float),
  330: ('engines/engine[3]/n1',  PacketData.unpack_float),
  331: ('engines/engine[3]/n2',  PacketData.unpack_float),
  332: ('engines/engine[3]/rpm', PacketData.unpack_float),
  340: ('engines/engine[4]/n1',  PacketData.unpack_float),
  341: ('engines/engine[4]/n2',  PacketData.unpack_float),
  342: ('engines/engine[4]/rpm', PacketData.unpack_float),
  350: ('engines/engine[5]/n1',  PacketData.unpack_float),
  351: ('engines/engine[5]/n2',  PacketData.unpack_float),
  352: ('engines/engine[5]/rpm', PacketData.unpack_float),
  360: ('engines/engine[6]/n1',  PacketData.unpack_float),
  361: ('engines/engine[6]/n2',  PacketData.unpack_float),
  362: ('engines/engine[6]/rpm', PacketData.unpack_float),
  370: ('engines/engine[7]/n1',  PacketData.unpack_float),
  371: ('engines/engine[7]/n2',  PacketData.unpack_float),
  372: ('engines/engine[7]/rpm', PacketData.unpack_float),
  380: ('engines/engine[8]/n1',  PacketData.unpack_float),
  381: ('engines/engine[8]/n2',  PacketData.unpack_float),
  382: ('engines/engine[8]/rpm', PacketData.unpack_float),
  390: ('engines/engine[9]/n1',  PacketData.unpack_float),
  391: ('engines/engine[9]/n2',  PacketData.unpack_float),
  392: ('engines/engine[9]/rpm', PacketData.unpack_float),

  800: ('rotors/main/rpm', PacketData.unpack_float),
  801: ('rotors/tail/rpm', PacketData.unpack_float),
  810: ('rotors/main/blade[0]/position-deg',  PacketData.unpack_float),
  811: ('rotors/main/blade[1]/position-deg',  PacketData.unpack_float),
  812: ('rotors/main/blade[2]/position-deg',  PacketData.unpack_float),
  813: ('rotors/main/blade[3]/position-deg',  PacketData.unpack_float),
  820: ('rotors/main/blade[0]/flap-deg',  PacketData.unpack_float),
  821: ('rotors/main/blade[1]/flap-deg',  PacketData.unpack_float),
  822: ('rotors/main/blade[2]/flap-deg',  PacketData.unpack_float),
  823: ('rotors/main/blade[3]/flap-deg',  PacketData.unpack_float),
  830: ('rotors/tail/blade[0]/position-deg',  PacketData.unpack_float),
  831: ('rotors/tail/blade[1]/position-deg',  PacketData.unpack_float),

  900: ('sim/hitches/aerotow/tow/length',                       PacketData.unpack_float),
  901: ('sim/hitches/aerotow/tow/elastic-constant',             PacketData.unpack_float),
  902: ('sim/hitches/aerotow/tow/weight-per-m-kg-m',            PacketData.unpack_float),
  903: ('sim/hitches/aerotow/tow/dist',                         PacketData.unpack_float),
  904: ('sim/hitches/aerotow/tow/connected-to-property-node',   PacketData.unpack_bool),
  905: ('sim/hitches/aerotow/tow/connected-to-ai-or-mp-callsign',   PacketData.unpack_FGFS_buggy_string),
  906: ('sim/hitches/aerotow/tow/brake-force',                  PacketData.unpack_float),
  907: ('sim/hitches/aerotow/tow/end-force-x',                  PacketData.unpack_float),
  908: ('sim/hitches/aerotow/tow/end-force-y',                  PacketData.unpack_float),
  909: ('sim/hitches/aerotow/tow/end-force-z',                  PacketData.unpack_float),
  930: ('sim/hitches/aerotow/is-slave',                         PacketData.unpack_bool),
  931: ('sim/hitches/aerotow/speed-in-tow-direction',           PacketData.unpack_float),
  932: ('sim/hitches/aerotow/open',                             PacketData.unpack_bool),
  933: ('sim/hitches/aerotow/local-pos-x',                      PacketData.unpack_float),
  934: ('sim/hitches/aerotow/local-pos-y',                      PacketData.unpack_float),
  935: ('sim/hitches/aerotow/local-pos-z',                      PacketData.unpack_float),

  1001: ('controls/flight/slats',  PacketData.unpack_float),
  1002: ('controls/flight/speedbrake',  PacketData.unpack_float),
  1003: ('controls/flight/spoilers',  PacketData.unpack_float),
  1004: ('controls/gear/gear-down',  PacketData.unpack_float),
  1005: ('controls/lighting/nav-lights',  PacketData.unpack_float),
  1006: ('controls/armament/station[0]/jettison-all',  PacketData.unpack_bool),

  1100: ('sim/model/variant', PacketData.unpack_int),
  1101: ('sim/model/livery/file', PacketData.unpack_FGFS_buggy_string),

  1200: ('environment/wildfire/data', PacketData.unpack_FGFS_buggy_string),
  1201: ('environment/contrail', PacketData.unpack_int),

  1300: ('tanker', PacketData.unpack_int),

  1400: ('scenery/events', PacketData.unpack_FGFS_buggy_string),

  1500: ('instrumentation/transponder/transmitted-id', PacketData.unpack_int),
  1501: ('instrumentation/transponder/altitude', PacketData.unpack_int),
  1502: ('instrumentation/transponder/ident', PacketData.unpack_bool),
  1503: ('instrumentation/transponder/inputs/mode', PacketData.unpack_int),

  10001: ('sim/multiplay/transmission-freq-hz',  PacketData.unpack_FGFS_buggy_string),
  10002: ('sim/multiplay/chat',  PacketData.unpack_FGFS_buggy_string),

  10100: ('sim/multiplay/generic/string[0]', PacketData.unpack_FGFS_buggy_string),
  10101: ('sim/multiplay/generic/string[1]', PacketData.unpack_FGFS_buggy_string),
  10102: ('sim/multiplay/generic/string[2]', PacketData.unpack_FGFS_buggy_string),
  10103: ('sim/multiplay/generic/string[3]', PacketData.unpack_FGFS_buggy_string),
  10104: ('sim/multiplay/generic/string[4]', PacketData.unpack_FGFS_buggy_string),
  10105: ('sim/multiplay/generic/string[5]', PacketData.unpack_FGFS_buggy_string),
  10106: ('sim/multiplay/generic/string[6]', PacketData.unpack_FGFS_buggy_string),
  10107: ('sim/multiplay/generic/string[7]', PacketData.unpack_FGFS_buggy_string),
  10108: ('sim/multiplay/generic/string[8]', PacketData.unpack_FGFS_buggy_string),
  10109: ('sim/multiplay/generic/string[9]', PacketData.unpack_FGFS_buggy_string),
  10110: ('sim/multiplay/generic/string[10]', PacketData.unpack_FGFS_buggy_string),
  10111: ('sim/multiplay/generic/string[11]', PacketData.unpack_FGFS_buggy_string),
  10112: ('sim/multiplay/generic/string[12]', PacketData.unpack_FGFS_buggy_string),
  10113: ('sim/multiplay/generic/string[13]', PacketData.unpack_FGFS_buggy_string),
  10114: ('sim/multiplay/generic/string[14]', PacketData.unpack_FGFS_buggy_string),
  10115: ('sim/multiplay/generic/string[15]', PacketData.unpack_FGFS_buggy_string),
  10116: ('sim/multiplay/generic/string[16]', PacketData.unpack_FGFS_buggy_string),
  10117: ('sim/multiplay/generic/string[17]', PacketData.unpack_FGFS_buggy_string),
  10118: ('sim/multiplay/generic/string[18]', PacketData.unpack_FGFS_buggy_string),
  10119: ('sim/multiplay/generic/string[19]', PacketData.unpack_FGFS_buggy_string),

  10200: ('sim/multiplay/generic/float[0]', PacketData.unpack_float),
  10201: ('sim/multiplay/generic/float[1]', PacketData.unpack_float),
  10202: ('sim/multiplay/generic/float[2]', PacketData.unpack_float),
  10203: ('sim/multiplay/generic/float[3]', PacketData.unpack_float),
  10204: ('sim/multiplay/generic/float[4]', PacketData.unpack_float),
  10205: ('sim/multiplay/generic/float[5]', PacketData.unpack_float),
  10206: ('sim/multiplay/generic/float[6]', PacketData.unpack_float),
  10207: ('sim/multiplay/generic/float[7]', PacketData.unpack_float),
  10208: ('sim/multiplay/generic/float[8]', PacketData.unpack_float),
  10209: ('sim/multiplay/generic/float[9]', PacketData.unpack_float),
  10210: ('sim/multiplay/generic/float[10]', PacketData.unpack_float),
  10211: ('sim/multiplay/generic/float[11]', PacketData.unpack_float),
  10212: ('sim/multiplay/generic/float[12]', PacketData.unpack_float),
  10213: ('sim/multiplay/generic/float[13]', PacketData.unpack_float),
  10214: ('sim/multiplay/generic/float[14]', PacketData.unpack_float),
  10215: ('sim/multiplay/generic/float[15]', PacketData.unpack_float),
  10216: ('sim/multiplay/generic/float[16]', PacketData.unpack_float),
  10217: ('sim/multiplay/generic/float[17]', PacketData.unpack_float),
  10218: ('sim/multiplay/generic/float[18]', PacketData.unpack_float),
  10219: ('sim/multiplay/generic/float[19]', PacketData.unpack_float),

  10300: ('sim/multiplay/generic/int[0]', PacketData.unpack_int),
  10301: ('sim/multiplay/generic/int[1]', PacketData.unpack_int),
  10302: ('sim/multiplay/generic/int[2]', PacketData.unpack_int),
  10303: ('sim/multiplay/generic/int[3]', PacketData.unpack_int),
  10304: ('sim/multiplay/generic/int[4]', PacketData.unpack_int),
  10305: ('sim/multiplay/generic/int[5]', PacketData.unpack_int),
  10306: ('sim/multiplay/generic/int[6]', PacketData.unpack_int),
  10307: ('sim/multiplay/generic/int[7]', PacketData.unpack_int),
  10308: ('sim/multiplay/generic/int[8]', PacketData.unpack_int),
  10309: ('sim/multiplay/generic/int[9]', PacketData.unpack_int),
  10310: ('sim/multiplay/generic/int[10]', PacketData.unpack_int),
  10311: ('sim/multiplay/generic/int[11]', PacketData.unpack_int),
  10312: ('sim/multiplay/generic/int[12]', PacketData.unpack_int),
  10313: ('sim/multiplay/generic/int[13]', PacketData.unpack_int),
  10314: ('sim/multiplay/generic/int[14]', PacketData.unpack_int),
  10315: ('sim/multiplay/generic/int[15]', PacketData.unpack_int),
  10316: ('sim/multiplay/generic/int[16]', PacketData.unpack_int),
  10317: ('sim/multiplay/generic/int[17]', PacketData.unpack_int),
  10318: ('sim/multiplay/generic/int[18]', PacketData.unpack_int),
  10319: ('sim/multiplay/generic/int[19]', PacketData.unpack_int)
}

def FGMS_prop_code_by_name(name):
	return next(code for code, data in FGMS_properties.items() if data[0] == name)


FGMS_prop_comm_frq = FGMS_prop_code_by_name('sim/multiplay/transmission-freq-hz') # STRING
FGMS_prop_chat_msg = FGMS_prop_code_by_name('sim/multiplay/chat') # STRING
FGMS_prop_XPDR_code = FGMS_prop_code_by_name('instrumentation/transponder/transmitted-id') # INT
FGMS_prop_XPDR_alt = FGMS_prop_code_by_name('instrumentation/transponder/altitude') # INT
FGMS_prop_XPDR_ident = FGMS_prop_code_by_name('instrumentation/transponder/ident') # BOOL
FGMS_prop_XPDR_mode = FGMS_prop_code_by_name('instrumentation/transponder/inputs/mode') # INT





## ======= FGFS orientation conversions =======

epsilon = 1e-8

def wxyz_quat_mult(q1, q2):
	w1, x1, y1, z1 = q1
	w2, x2, y2, z2 = q2
	w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
	x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
	y = w1 * y2 - x1 * z2 + y1 * w2 + z1 * x2
	z = w1 * z2 + x1 * y2 - y1 * x2 + z1 * w2
	return w, x, y, z




def earth2quat(coords):
	zd2 = radians(coords.lon) / 2
	yd2 = -pi / 4 - radians(coords.lat) / 2
	Szd2 = sin(zd2)
	Syd2 = sin(yd2)
	Czd2 = cos(zd2)
	Cyd2 = cos(yd2)
	w = Czd2 * Cyd2
	x = -Szd2 * Syd2
	y = Czd2 * Syd2
	z = Szd2 * Cyd2
	return w, x, y, z


def euler2quat(z, y, x):
	zd2 = z / 2
	yd2 = y / 2
	xd2 = x / 2
	Szd2 = sin(zd2)
	Syd2 = sin(yd2)
	Sxd2 = sin(xd2)
	Czd2 = cos(zd2)
	Cyd2 = cos(yd2)
	Cxd2 = cos(xd2)
	Cxd2Czd2 = Cxd2 * Czd2
	Cxd2Szd2 = Cxd2 * Szd2
	Sxd2Szd2 = Sxd2 * Szd2
	Sxd2Czd2 = Sxd2 * Czd2
	w = Cxd2Czd2 * Cyd2 + Sxd2Szd2 * Syd2
	x = Sxd2Czd2 * Cyd2 - Cxd2Szd2 * Syd2
	y = Cxd2Czd2 * Syd2 + Sxd2Szd2 * Cyd2
	z = Cxd2Szd2 * Cyd2 - Sxd2Czd2 * Syd2
	return w, x, y, z




def FG_orientation_XYZ(coords, hdg, pitch, roll):
	local_rot = euler2quat(radians(hdg), radians(pitch), radians(roll))
	qw, qx, qy, qz = wxyz_quat_mult(earth2quat(coords), local_rot)
	acw = acos(qw)
	sa = sin(acw)
	if abs(sa) < epsilon:
		return 1, 0, 0 # no rotation
	else:
		angle = 2 * acw
		k = angle / sa
		return k*qx, k*qy, k*qz
