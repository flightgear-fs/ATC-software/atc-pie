
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from socket import timeout

from urllib.request import urlopen
from urllib.error import URLError
from urllib.parse import urlencode
from xml.etree import ElementTree

from data.UTC import now


# ---------- Constants ----------

METAR_base_location = 'http://tgftp.nws.noaa.gov/data/observations/metar/stations'
decl_base_location = 'http://www.ngdc.noaa.gov/geomag-web/calculators/calculateDeclination'

# -------------------------------


def get_METAR(icao):
	try:
		info_string = urlopen('%s/%s.TXT' % (METAR_base_location, icao)).read().decode().split('\n')[1]
		info_string += '='
		return info_string
	except URLError:
		print('Could not download METAR for station %s' % icao)
	except timeout:
		print('NOAA METAR request timed out.')




def get_declination(earth_location):
	today = now()
	try:
		q_items = {
			'startYear': today.year,
			'startMonth': today.month,
			'startDay': today.day,
			'resultFormat': 'xml',
			'lon1Hemisphere': 'EW'[earth_location.lon < 0],
			'lon1': abs(earth_location.lon),
			'lat1Hemisphere': 'NS'[earth_location.lat < 0],
			'lat1': abs(earth_location.lat),
			'browserRequest': 'false'
		}
		url = '%s?%s' % (decl_base_location, urlencode(q_items))
		response = ElementTree.fromstring(urlopen(url, timeout=10).read())
		if response.tag == 'maggridresult':
			res_elt = response.find('result')
			if res_elt != None:
				decl = float(res_elt.find('declination').text)
				return decl
	except URLError:
		print('Could not obtain declination from NOAA website.')
	except timeout:
		print('NOAA declination request timed out.')
	except ElementTree.ParseError:
		print('Parse error while reading NOAA data.')


