
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re

from os import path

from data.airport import AirportData
from data.coords import EarthCoords
from data.params import Heading, Speed
from data.airport import DirRunway, Helipad
from data.nav import NavpointError
from data.db import acft_db
from data.elevation import ElevationMap

from PyQt5.QtGui import QPixmap, QColor


# ---------- Constants ----------

route_presets_file = 'resources/nav/route-presets'
aircraft_db_spec_file = 'resources/acft/acft-db'
background_images_dir = 'resources/bg-img'
elev_map_file_fmt = 'resources/elev/%s.elev'

pixmap_corner_sep = ':'
livery_spec_line_token = ':'
unavailable_acft_info_token = '-'

# -------------------------------


def read_point_spec(specstr, db):
	mvlst = specstr.split('>')
	pbase = mvlst.pop(0)
	try:
		result = EarthCoords.fromString(pbase) if ',' in pbase else navpointFromSpec(pbase, db).coordinates
	except NavpointError:
		raise ValueError('No navpoint for "%s" or navpoint not unique (consider using \'~\' operator)' % pbase)
	else:
		while mvlst != []:
			mv = mvlst.pop(0).split(',')
			if len(mv) == 2:
				radial = Heading(float(mv[0]), True)
				distance = float(mv[1])
				result = result.moved(radial, distance)
			else:
				raise ValueError('Bad use of \'>\' in point spec "%s"' % specstr)
		return result


	
def navpointFromSpec(specstr, db):
	if '~' in specstr: # name closest to point spec
		name, near = specstr.split('~', maxsplit=1)
		return db.findClosest(read_point_spec(near, db), code=name)
	else:
		return db.findUnique(specstr)



##------------------------------------##
##                                    ##
##          BACKGROUND IMAGES         ##
##                                    ##
##------------------------------------##

def read_hand_drawing(file_name, db):
	try:
		with open(file_name) as f:
			draw_sections = []
			line = f.readline()
			while line != '':
				line = line.strip()
				if line != '':
					colour = QColor(line)
					if not colour.isValid():
						raise ValueError('Not a colour: ' + line)
					points = []
					tokens = f.readline().split(maxsplit=1)
					while len(tokens) != 0:
						text = tokens[1].strip() if len(tokens) == 2 else None
						points.append((read_point_spec(tokens[0], db), text))
						tokens = f.readline().split(maxsplit=1)
					if len(points) == 0:
						raise ValueError('No points in sequence')
					draw_sections.append((colour, points))
				line = f.readline()
		return draw_sections
	except FileNotFoundError:
		raise ValueError('Drawing file not found')


def read_bg_img(icao, db):
	try:
		with open(path.join(background_images_dir, '%s.lst' % icao)) as f:
			radar_background_layers = []
			loose_strip_bay_backgrounds = []
			for line in f:
				tokens = line.strip().split(maxsplit=2)
				if len(tokens) == 3:
					try:
						image_file = path.join(background_images_dir, tokens[0])
						if tokens[1] == 'DRAW': # DRAWING SPEC, no corners given
							radar_background_layers.append((False, tokens[0], tokens[2], read_hand_drawing(image_file, db)))
						else:
							pixmap = QPixmap(image_file)
							if pixmap.isNull():
								raise ValueError('Not found or unrecognised format')
							if tokens[1] == 'LOOSE': # LOOSE STRIP BAY BACKGROUND
								new_tokens = tokens[2].split(maxsplit=1)
								if len(new_tokens) != 2:
									raise ValueError('Bad LOOSE spec (missing scale or title)')
								loose_strip_bay_backgrounds.append((tokens[0], pixmap, float(new_tokens[0]), new_tokens[1]))
							elif pixmap_corner_sep in tokens[1]: # PIXMAP, two corners given
								nw, se = tokens[1].split(pixmap_corner_sep, maxsplit=1)
								nw_coords = read_point_spec(nw, db)
								se_coords = read_point_spec(se, db)
								radar_background_layers.append((True, tokens[0], tokens[2], (pixmap, nw_coords, se_coords)))
							else:
								raise ValueError('Bad image spec (should be NW:SE or "DRAW")' + tokens[1])
					except ValueError as error:
						print('%s: %s' % (tokens[0], error))
				elif tokens != []:
					print('Bad syntax in background drawing spec line: ' + line)
		return radar_background_layers, loose_strip_bay_backgrounds
	except FileNotFoundError:
		print('No background image list found.')
		return [], []




##-----------------------------------##
##                                   ##
##       GROUND ELEVATION MAPS       ##
##                                   ##
##-----------------------------------##


def get_ground_elevation_map(location_code):
	try:
		with open(elev_map_file_fmt % location_code) as f:
			nw = se = None
			line = f.readline()
			while nw == None and line != '':
				tokens = line.split('#', maxsplit=1)[0].split()
				if tokens == []:
					line = f.readline()
				elif len(tokens) == 2:
					nw = EarthCoords.fromString(tokens[0])
					se = EarthCoords.fromString(tokens[1])
				else:
					raise ValueError('invalid header line')
			if nw == None:
				raise ValueError('missing header line')
			matrix = []
			xprec = None
			line = f.readline()
			while line.strip() != '':
				values = [float(token) for token in line.split('#', maxsplit=1)[0].split()]
				if xprec == None:
					xprec = len(values)
				elif len(values) != xprec:
					raise ValueError('expected %d values in row %d' % (xprec, len(matrix) + 1))
				matrix.append(values) # add row
				line = f.readline()
		# Finished reading file.
		result = ElevationMap(nw.toRadarCoords(), se.toRadarCoords(), len(matrix), xprec)
		for i, row in enumerate(matrix):
			for j, elev in enumerate(row):
				result.setElevation(i, j, elev)
		return result
	except ValueError as err:
		print('Error in elevation map: %s' % err)





##-----------------------------------------##
##                                         ##
##   AIRCRAFT DATA BASE + FGFS RENDERING   ##
##                                         ##
##-----------------------------------------##


def load_aircraft_db():
	'''
	returns a dict: ICAO desig -> (category, WTC, cruise speed)
	where category is either of those used in X-plane, e.g. for parking positions
	any of tuple elements can use the "unavailable_acft_info_token" to signify unkown info
	'''
	try:
		with open(aircraft_db_spec_file) as f:
			for line in f:
				tokens = line.split('#', maxsplit=1)[0].split()
				if len(tokens) == 4:
					desig, xplane_cat, wtc, cruise = tokens
					if xplane_cat == unavailable_acft_info_token:
						xplane_cat = None
					if wtc == unavailable_acft_info_token:
						wtc = None
					acft_db[desig] = xplane_cat, wtc, (Speed(float(cruise)) if cruise != unavailable_acft_info_token else None)
				elif tokens != []:
					print('Error on ACFT spec line: %s' % line.strip())
	except FileNotFoundError:
		print('Aircraft data base file not found: %s' % aircraft_db_spec_file)





def make_FGFS_model_recognisers(spec_file):
	result = []
	try:
		with open(spec_file) as f:
			for line in f:
				tokens = line.split('#', maxsplit=1)[0].split(maxsplit=2)
				if len(tokens) == 2: # new model recogniser
					regexp, model = tokens
					try:
						result.append((re.compile(regexp, flags=re.IGNORECASE), model))
					except Exception as err: # CAUTION: greedy catch, but can only come from exceptions in re.compile
						print('Error in regexp for model %s: %s' % (model, err))
				elif tokens != []:
					print('Error on FGFS model recognising spec line: %s' % line)
	except FileNotFoundError:
		print('FG model recognising spec file not found: %s' % spec_file)
	return result



def make_FGFS_model_chooser(spec_file):
	models = {}
	liveries = {}
	last_dez = None
	try:
		with open(spec_file) as f:
			for line in f:
				tokens = line.split('#', maxsplit=1)[0].split(maxsplit=3)
				if len(tokens) == 2: # new model chooser
					dez, model = tokens
					models[dez] = model
					last_dez = dez
				elif len(tokens) == 3 and tokens[0] == livery_spec_line_token and last_dez != None:
					airline, livery = tokens[1:]
					if last_dez not in liveries: # first key for this ACFT type
						liveries[last_dez] = {}
					liveries[last_dez][airline] = livery
				elif tokens != []:
					print('Error on FGFS model choice line: %s' % line)
					last_dez = None
	except FileNotFoundError:
		print('FG model chooser spec file not found: %s' % spec_file)
	return models, liveries




##---------------------------------##
##                                 ##
##          PRESET ROUTES          ##
##                                 ##
##---------------------------------##


def read_route_presets():
	result = {}
	try:
		with open(route_presets_file) as f:
			for line in f:
				spl = line.strip().split(maxsplit=2)
				if spl == [] or line.startswith('#'):
					pass # Ignore empty or comment lines
				elif len(spl) == 3:
					end_points = spl[0], spl[1]
					try:
						result[end_points].append(spl[2])
					except KeyError:
						result[end_points] = [spl[2]]
				else:
					print('Error on preset route line: %s' % line)
	except FileNotFoundError:
		pass
	return result


