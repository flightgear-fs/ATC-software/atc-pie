
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from telnetlib import Telnet
from PyQt5.QtCore import QProcess, QThread

from settings import settings
from game.env import env
from ext.resources import make_FGFS_model_recognisers, make_FGFS_model_chooser
from GUI.misc import signals


# ---------- Constants ----------

fgfs_viewing_acft_model = 'ufo'
dummy_viewer_callsign = 'ATC-pie'
telnet_read_timeout = 5 # seconds (or None)
initial_FOV = 55 # degrees of horizontal angle covered

FGFS_models_to_ICAO_types_file = 'resources/acft/fgfs2icao'
ICAO_types_to_FGFS_models_file = 'resources/acft/icao2fgfs'

# -------------------------------


FGFS_model_recognisers = make_FGFS_model_recognisers(FGFS_models_to_ICAO_types_file)
FGFS_model_chooser, FGFS_liveries = make_FGFS_model_chooser(ICAO_types_to_FGFS_models_file)



# FGFS model -> ICAO type identification (used when reading FGMS packets in multi-player games)
def ICAO_aircraft_type(model):
	try:
		return next(icao for recog, icao in FGFS_model_recognisers if recog.match(model))
	except StopIteration:
		return None if settings.strict_FGFS_ATDs else model


# ICAO ACFT type -> FGFS model string (used to include in FGMS packets for solo game tower viewing)
def FGFS_model(icao_type):
	try:
		return FGFS_model_chooser[icao_type]
	except KeyError:
		return icao_type






def fgTwrPositioningOptions():
	assert env.airport_data != None
	pos, alt = env.viewpoint()
	options = []
	options.append('--lat=%s' % pos.lat)
	options.append('--lon=%s' % pos.lon)
	options.append('--altitude=%g' % alt)
	options.append('--heading=360')
	return options



class FlightGearTowerViewer:
	def __init__(self, gui):
		self.internal_process = QProcess(gui)
		self.internal_process.setStandardErrorFile(settings.outputFileName('fgfs-stderr', ext='log'))
		self.internal_process.stateChanged.connect(lambda state: self.notifyStartStop(state == QProcess.Running))
		self.running = False
	
	def start(self):
		if settings.external_tower_viewer_process:
			self.notifyStartStop(True)
		else:
			fgfs_options = fgTwrPositioningOptions()
			# More positioning options
			fgfs_options.append('--roll=0')
			fgfs_options.append('--pitch=0')
			fgfs_options.append('--vc=0')
			# Local directory options
			if settings.FGFS_root_dir != '':
				fgfs_options.append('--fg-root=%s' % settings.FGFS_root_dir)
			if settings.FGFS_scenery_dir != '':
				fgfs_options.append('--fg-scenery=%s' % settings.FGFS_scenery_dir)
			# Connection options
			fgfs_options.append('--callsign=%s' % dummy_viewer_callsign)
			fgfs_options.append('--multiplay=out,100,localhost,%d' % settings.FGFS_views_send_port)
			fgfs_options.append('--multiplay=in,100,localhost,%d' % settings.tower_viewer_UDP_port)
			fgfs_options.append('--telnet=,,100,,%d,' % settings.tower_viewer_telnet_port)
			# View options
			fgfs_options.append('--aircraft=%s' % fgfs_viewing_acft_model)
			fgfs_options.append('--fdm=null')
			fgfs_options.append('--fov=%g' % initial_FOV)
			fgfs_options.append('--enable-real-weather-fetch')
			fgfs_options.append('--time-match-real')
			# Options for lightweight (interface and CPU load)
			fgfs_options.append('--disable-ai-traffic')
			fgfs_options.append('--disable-panel')
			fgfs_options.append('--disable-sound')
			fgfs_options.append('--disable-hud')
			fgfs_options.append('--disable-fullscreen')
			fgfs_options.append('--prop:/sim/menubar/visibility=false')
			# Now run
			self.internal_process.setProgram(settings.FGFS_executable)
			self.internal_process.setArguments(fgfs_options)
			#print('Running: %s %s' % (settings.FGFS_executable, ' '.join(fgfs_options)))
			self.internal_process.start()
		
	def stop(self, wait=False):
		if self.running:
			if settings.external_tower_viewer_process:
				self.notifyStartStop(False)
			else:
				self.internal_process.terminate()
				if wait:
					self.internal_process.waitForFinished() # default time-out
	
	def notifyStartStop(self, b):
			self.running = b
			signals.towerViewProcessToggled.emit(b)








def send_packet_to_views(udp_packet):
	if settings.controlled_tower_viewer.running:
		tower_viewer_host = settings.external_tower_viewer_host if settings.external_tower_viewer_process else 'localhost'
		send_packet_to_view(udp_packet, (tower_viewer_host, settings.tower_viewer_UDP_port))
		#print('Sent packet to %s:%d' % (tower_viewer_host, settings.tower_viewer_UDP_port))
	if settings.additional_views_active:
		for address in settings.additional_views:
			send_packet_to_view(udp_packet, address)


def send_packet_to_view(packet, addr):
	try:
		settings.FGFS_views_send_socket.sendto(packet, addr)
	except OSError as err:
		pass









class TelnetSessionThreader(QThread):
	def __init__(self, gui, commands, loopInterval=None):
		QThread.__init__(self, gui)
		self.connection = None
		self.loop_interval = loopInterval
		self.generate_commands = (lambda: commands) if isinstance(commands, list) else commands
	
	def run(self):
		try:
			tower_viewer_host = settings.external_tower_viewer_host if settings.external_tower_viewer_process else 'localhost'
			#print('Creating Telnet connection to %s:%d' % (tower_viewer_host, settings.tower_viewer_telnet_port)) # DEBUG
			self.connection = Telnet(tower_viewer_host, port=settings.tower_viewer_telnet_port)
			self.loop = self.loop_interval != None
			self._runCommandsOnce()
			if self.loop:
				QThread.msleep(self.loop_interval)
			while self.loop:
				self._runCommandsOnce()
				QThread.msleep(self.loop_interval)
			self.connection.close()
		except ConnectionRefusedError:
			print('Telnet connection error. Is tower view window open?')
	
	def stop(self):
		self.loop = False
	
	def _runCommandsOnce(self):
		for cmd in self.generate_commands():
			self.sendCommand(cmd)
		
	def sendCommand(self, line, answer=False):
		#print('Sending command: %s' % line) # DEBUG
		self.connection.write(line.encode() + b'\r\n')
		if answer:
			read = self.connection.read_until(b'\n', timeout=telnet_read_timeout)
			#print('Received bytes: %s' % read) # DEBUG
			return read.decode().strip()

