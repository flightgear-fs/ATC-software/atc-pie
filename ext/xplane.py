
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

import re
from os import path

from settings import version_string
from data.airport import AirportData, GroundNetwork
from data.coords import EarthCoords
from data.params import Heading
from data.nav import world_nav_data, Fix, NDB, VOR, Airfield, Rnav
from data.airport import DirRunway, Helipad

from PyQt5.QtGui import QPainterPath


# ---------- Constants ----------

xplane_data_dir = 'resources/xplane'
extracted_airport_fmt = 'resources/apt.extract/%s'
custom_airport_fmt = 'resources/apt/%s.dat'
custom_navaid_file = 'resources/nav/custom_navaid'
custom_navfix_file = 'resources/nav/custom_fix'

extracted_apt_file_line2 = 'Extracted by ATC-pie version %s from the full world "apt.dat" file' % version_string

# -------------------------------



# ============================================== #

#            X-PLANE HELPER FUNCTIONS            #

# ============================================== #


def line_code(line):
	try:
		return int(line.split(maxsplit=1)[0])
	except (IndexError, ValueError):
		return None




def is_xplane_airport_header(line, icao=None):
	return line_code(line) in [1, 16, 17] and (icao == None or line.split(maxsplit=5)[4] == icao)

def is_xplane_node_line(line):
	return line_code(line) in range(111, 117)

def is_paved(surface_code):
	return surface_code in ['1', '2']


def decimalise_freq(s):
	# we have len(s) == 5 with no decimal point
	frq = '%s.%s' % (s[:3], s[3:])
	if frq[5] in '27':
		return frq + '5'
	else: # normally in '05'
		return frq + '0'



def open_airport_file(ad_code):
	try:
		return open(custom_airport_fmt % ad_code)
	except FileNotFoundError: # No custom airport file found; fall back on packaged X-plane data.
		extracted_airport_file_name = extracted_airport_fmt % ad_code
		try:
			return open(extracted_airport_file_name)
		except FileNotFoundError: # Airport never extracted yet; build simple file first.
			with open(path.join(xplane_data_dir, 'apt.dat'), encoding='iso-8859-1') as f:
				line = line1 = f.readline()
				while line != '' and not is_xplane_airport_header(line, icao=ad_code):
					line = f.readline()
				if line != '': # not EOF
					with open(extracted_airport_file_name, 'w') as out:
						out.write(line1)
						out.write(extracted_apt_file_line2 + '\n\n')
						out.write(line)
						line = f.readline()
						while line != '' and not is_xplane_airport_header(line):
							out.write(line)
							line = f.readline()
			# Now file should exist
			return open(extracted_airport_file_name)





def extend_path(path, end_point, prev_bezier, this_bezier):
	if this_bezier == None:
		if prev_bezier == None: # straight line
			path.lineTo(end_point)
		else:                   # bezier curve from last ctrl point
			path.quadTo(prev_bezier, end_point)
	else:
		mirror_ctrl = 2 * end_point - this_bezier
		if prev_bezier == None: # bezier curve to point with ctrl point
			path.quadTo(mirror_ctrl, end_point)
		else:                   # cubic bezier curve using ctrl points on either side
			path.cubicTo(prev_bezier, mirror_ctrl, end_point)
	



def parse_xplane_node_line(line):
	'''
	returns a 5-value tuple:
	- node position
	- bezier ctrl point if not None
	- int paint type
	- int light type
	- bool if ending node (True if closes path), or None if path goes on
	'''
	if not is_xplane_node_line(line):
		raise ValueError('Not a node line: %s' % line)
	tokens = line.split()
	row_code = tokens[0]
	node_coords = EarthCoords(float(tokens[1]), float(tokens[2])).toQPointF()
	bezier_ctrl = EarthCoords(float(tokens[3]), float(tokens[4])).toQPointF() if row_code in ['112', '114', '116'] else None
	ending_spec = None if row_code in ['111', '112'] else row_code in ['113', '114']
	paint_type = int(tokens[5]) if len(tokens) >= 6 else None
	light_type = int(tokens[6]) if len(tokens) >= 7 else None
	return node_coords, bezier_ctrl, ending_spec, paint_type, light_type
	






def read_xplane_node_sequence(f, first_line=None):
	'''
	Node sequence.
	returns (QPainterPath, bool if closed, int lines read)
	'''
	path = QPainterPath()
	lines_read = 0
	line = first_line
	if line == None:
		line = f.readline()
		lines_read += 1
	node, bez, end_mode, paint, light = parse_xplane_node_line(line.strip())
	first_node = node
	first_bezier = bez
	got_last = end_mode != None
	path.moveTo(node)
	prev_bezier = bez
	while line != '' and not got_last:
		node, bez, end_mode, paint, light = parse_xplane_node_line(line.strip())
		got_last = end_mode != None
		extend_path(path, node, prev_bezier, bez)
		prev_bezier = bez
		if not got_last:
			line = f.readline()
			lines_read += 1
	if got_last:
		if end_mode: # if this is "referenced before def", node sequence has length 1 (legal?!)
			# last node implies a closed loop
			extend_path(path, first_node, bez, first_bezier)
		return path, end_mode, lines_read
	else:
		print('End of file: missing ending node')










# =============================================== #

#                  AIRPORT DATA                   #

# =============================================== #


def get_airport_data(icao):
	result = AirportData()
	result.navpoint = world_nav_data.findAirfield(icao)
	for width, paved, rwy1, rwy2 in get_runways(icao):
		result.addPhysicalRunway(width, paved, rwy1, rwy2)
	result.ground_net = get_ground_network(icao)
	result.viewpoints = get_viewpoints(icao)
	result.helipads = get_helipads(icao)
	result.windsocks = get_windsocks(icao)
	return result




# X-PLANE runway line example:
# 100 29.87 1 1 0.00 0 2 1 07L 48.75115000 002.09846100 0.00 178.61 2 0 0 1 25R 48.75439400 002.11289900 0.00 0.00 2 1 0 0

# In order:
# 0: "100" for land RWY
# 1: width-metres
# 2: surface type
# 3-7: (ignore)
# 8-16 (RWY 1): name lat-end lon-end disp-thr-metres (ignore) (ignore) (ignore) (ignore) (ignore)
# 17-25 (RWY 2): (idem 8-16)

def get_runways(icao):
	with open_airport_file(icao) as f:
		result = []
		for line in f:
			tokens = line.split()
			if tokens != [] and tokens[0] == '100':
				width = float(tokens[1])
				paved = is_paved(tokens[2])
				name, lat, lon, disp_thr = tokens[8:12]
				rwy1 = DirRunway(name, EarthCoords(float(lat), float(lon)), float(disp_thr))
				name, lat, lon, disp_thr = tokens[17:21]
				rwy2 = DirRunway(name, EarthCoords(float(lat), float(lon)), float(disp_thr))
				result.append((width, paved, rwy1, rwy2))
		return result


# X-PLANE helipad line example:
# 102 H1 47.53918248 -122.30722302 2.00 10.06 10.06 1 0 0 0.25 0

# In order:
# 0: "102" for helipad
# 1: name/designator
# 2-3: lat-lon of centre
# 4: true heading orientation
# 5-6: length-width (metres)
# 7: surface type
# 8-11: (ignore)

def get_helipads(ICAO):
	with open_airport_file(ICAO) as f:
		result = []
		for line in f:
			tokens = line.split()
			if tokens != [] and tokens[0] == '102':
				row_code, name, lat, lon, ori, l, w, surface = tokens[:8]
				centre = EarthCoords(float(lat), float(lon))
				result.append(Helipad(name, centre, is_paved(tokens[7]), float(l), float(w), Heading(float(ori), True)))
		return result


# X-PLANE frequency line example:
# 50 11885 ATIS

# In order:
# 0: freq type: 50=recorded (e.g. ATIS), 51=unicom, 52=DEL, 53=GND, 54=TWR, 55=APP, 56=DEP
# 1: integer frequency in 100*Hz
# 2: description

def get_frequencies(icao):
	with open_airport_file(icao) as f:
		result = []
		for line in f:
			tokens = line.split(maxsplit=2)
			if tokens != [] and tokens[0] in [str(i) for i in range(50, 57)]:
				freq_type = { '50':'recorded', '51':'A/A', '52':'DEL', '53':'GND', '54':'TWR', '55':'APP', '56':'DEP' }[tokens[0]]
				result.append((decimalise_freq(tokens[1]), tokens[2].strip(), freq_type))
	return sorted(result, key=(lambda f: f[0]))



# X-PLANE viewpoint line example:
# 14   37.61714303 -122.38327660  200 0 Tower Viewpoint

# In order:
# 0: "14" for viewpoint
# 1-2: lat-lon coordinates
# 3: viewpoint height in ft
# 4: (ignored)
# 5: name

def get_viewpoints(icao):
	result = []
	with open_airport_file(icao) as f:
		for line in f:
			if line_code(line) == 14:
				row_code, lat, lon, height, ignore, name = line.split(maxsplit=5)
				result.append((EarthCoords(float(lat), float(lon)), float(height), name.strip()))
	return result


# X-PLANE windsock line example:
# 19  48.71901305  002.37906976 1 New Windsock 02

# In order:
# 0: "19" for windsock
# 1-2: lat-lon coordinates
# 3: has lighting
# 4: name

def get_windsocks(ICAO):
	with open_airport_file(ICAO) as f:
		result = []
		for line in f:
			if line_code(line) == 19:
				row_code, lat, lon, ignore_rest_of_line = line.split(maxsplit=3)
				result.append(EarthCoords(float(lat), float(lon)))
	return result




def get_airport_boundary(ICAO):
	with open_airport_file(ICAO) as f:
		result = []
		line = f.readline()
		line_number = 1
		while line != '' and not line_code(line) == 130: # Airport boundary section
			line = f.readline()
			line_number += 1
		if line != '':
			path, closed, lines_read = read_xplane_node_sequence(f)
			line_number += lines_read
			if not closed:
				print('Line %d: Boundary should end with a closing node' % line_number)
			return path


def get_taxiways(ICAO):
	'''
	returns a list of (descr_str, paved_bool, QPainterPath) tuples
	Each tuple defines a taxiway, to be drawn with the QPainterPath.
	'''
	with open_airport_file(ICAO) as f:
		result = []
		line = f.readline()
		line_number = 1
		while line != '': # not EOF
			if line_code(line) == 110: # TWY section
				header_tokens = line.strip().split(maxsplit=4)
				#Replaced because of dodgy data found at VHXX: row_code, surface, ignore1, ignore2, descr = header_tokens
				surface = header_tokens[1]
				descr = header_tokens[4] if len(header_tokens) == 5 else ''
				#print('Reading TWY section: %s' % descr)
				twy_path, closed, lines_read = read_xplane_node_sequence(f)
				line_number += lines_read
				if not closed:
					print('Line %d: X-plane taxiway should end with a closing node' % line_number)
				# Read holes on this TWY:
				line = f.readline()
				line_number += 1
				while is_xplane_node_line(line):
					hole_path, closed, lines_read = read_xplane_node_sequence(f, first_line=line)
					line_number += lines_read
					if not closed:
						print('Line %d: TWY hole should end with a closing node' % line_number)
					twy_path.addPath(hole_path)
					line = f.readline() # for new loop (more holes)
					line_number += 1
				result.append((descr, is_paved(surface), twy_path))
			else:
				line = f.readline() # for new loop (more TWYs)
				line_number += 1
	return result
	



def get_airport_linear_objects(icao):
	with open_airport_file(icao) as f:
		result = []
		line = f.readline()
		line_number = 1
		while line != '': # not EOF
			if line_code(line) == 120: # Linear feature; header can contain a name (ignored here)
				object_name = line.split(maxsplit=1)[1] if not line.strip() == '120' else None
				path, closed, lines_read = read_xplane_node_sequence(f)
				line_number += lines_read
				result.append((object_name, path))
			line = f.readline() # for more linear objects
			line_number += 1
	return result



# Example of TAXIWAY NODE spec line
#   1201 47.53752190 -122.30826710 both 5416 A_start
# Columns:
#   1-2: lat-lon
#   4: ID

# Example of TAXIWAY EDGE spec line
#   1202 5416 5417 twoway taxiway A
# Columns:
#   1-2: vertices
#   4: "taxiway" or "runway" if on runway
#   5: TWY name

# Example of PARKING POSITION spec line
#   1300 47.43931757 -122.29806851 88.78 gate jets|turboprops A2
# Columns:
#   1-2: lat-lon
#   3: true heading when ACFT is parked
#   4: "gate", "hangar", "misc" or "tie-down" ("misc" not considered as parking)
#   5: pipe-deparated list heavy|jets|turboprops|props|helos or "all"
#   6: unique name of position


def get_ground_network(icao):
	with open_airport_file(icao) as f:
		ground_net = GroundNetwork()
		line = f.readline()
		line_number = 1
		while line != '': # not EOF
			if line_code(line) == 1201: # TWY node
				tokens = line.strip().split(maxsplit=5)
				lat, lon, ignore, nid = tokens[1:5]
				ground_net.addNode(nid, EarthCoords(float(lat), float(lon)))
			elif line_code(line) == 1202: # TWY edge
				tokens = line.strip().split(maxsplit=5)
				v1, v2 = tokens[1:3]
				if ground_net.nodeExists(v1) and ground_net.nodeExists(v2):
					rwy_spec = tokens[5] if len(tokens) == 6 and tokens[4] == 'runway' else None
					twy_name = tokens[5] if len(tokens) == 6 and tokens[4] == 'taxiway' else None
					ground_net.addEdge(v1, v2, rwy=rwy_spec, twy=twy_name)
				else:
					print('Line %d: Invalid node for taxiway edge spec' % line_number)
			elif line_code(line) == 1300: # parking_position
				tokens = line.strip().split(maxsplit=6)
				if len(tokens) == 7:
					lat, lon, hdg, typ, who, pkid = tokens[1:7]
					if typ in ['gate', 'hangar', 'tie-down']:
						pos = EarthCoords(float(lat), float(lon))
						cats = [] if who == 'all' else who.split('|')
						ground_net.addParkingPosition(pkid, pos, Heading(float(hdg), True), typ, cats)
				else:
					print('Line %d: Invalid parking position spec' % line_number)
			line = f.readline() # for new loop (more TWYs)
			line_number += 1
		return ground_net










# ================================================ #

#                 NAVIGATION DATA                  #

# ================================================ #


def _seek_airfield_coords(f):
	'''
	we are inside the airport section looking for its coordinates
	return the coordinates found, and the line that made you stop (overshoot), or empty string if EOF
	Best score is tower view point (line code 14)
	'''
	coords = None
	score = 0
	line = f.readline()
	while line != '' and not is_xplane_airport_header(line):
		if line_code(line) == 14: # X-Plane view point
			row_code, lat, lon, ignore_rest_of_line = line.split(maxsplit=3)
			coords = EarthCoords(float(lat), float(lon))
		elif coords == None and line_code(line) == 100: # fall back on RWY if no viewpoint found yet
			tokens = line.split()
			coords = EarthCoords(float(tokens[9]), float(tokens[10])).moved(Heading(360, True), .15)
		line = f.readline()
	return coords, line


def import_airfield_data(file_name):
	with open(file_name, encoding='iso-8859-1') as f:
		line = f.readline()
		while line != '': # not EOF
			if is_xplane_airport_header(line):
				row_code, elevation, ignore1, ignore2, code, name = line.split(maxsplit=5)
				if code.isalpha(): # Ignoring airports with numbers in them---to many of them, hardly ever useful
					coords, line = _seek_airfield_coords(f)
					if coords != None: # Airfields with unknown world coordinates are ignored
						world_nav_data.add(Airfield(code, coords, float(elevation), name.strip()))
				else:
					line = f.readline()
			else:
				line = f.readline()


def import_navaid_data(file_name):
	with open(file_name, encoding='iso-8859-1') as f:
		for line in f:
			if line_code(line) in [2, 3]: # NDB or VOR
				row_code, lat, lon, ignore1, frq, ignore2, ignore3, short_name, xplane_name = line.split(maxsplit=8)
				long_name = xplane_name.strip()
				coords = EarthCoords(float(lat), float(lon))
				if row_code == '2': # NDB
					world_nav_data.add(NDB(short_name, coords, frq, long_name, dme=('DME' in long_name)))
				else: # VOR/VORTAC
					world_nav_data.add(VOR(short_name, coords, decimalise_freq(frq), long_name, \
						dme=('DME' in long_name), tacan=('VORTAC' in long_name)))


# navfix_data_file example line:
# 49.137500  004.049167 DIKOL
#   lat        lon      name
navfix_line_regexp = re.compile('([0-9.-]+) +([0-9.-]+) +([A-Za-z0-9]+)')

def import_navfix_data(file_name):
	with open(file_name, encoding='iso-8859-1') as f:
		for line in f:
			match = navfix_line_regexp.search(line)
			if match: # Fix spec line
				lat = float(match.group(1))
				lon = float(match.group(2))
				name = match.group(3)
				coordinates = EarthCoords(lat, lon)
				if name.isalpha() and len(name) == 5:
					world_nav_data.add(Fix(name, coordinates))
				else:
					world_nav_data.add(Rnav(name, coordinates))



def import_world_navigation_data():
	import_airfield_data(path.join(xplane_data_dir, 'apt.dat'))
	try:
		import_navaid_data(custom_navaid_file)
	except FileNotFoundError:
		import_navaid_data(path.join(xplane_data_dir, 'earth_nav.dat'))
	try:
		import_navfix_data(custom_navfix_file)
	except FileNotFoundError:
		import_navfix_data(path.join(xplane_data_dir, 'earth_fix.dat'))


