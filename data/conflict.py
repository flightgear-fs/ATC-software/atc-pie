
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from util import some, ordered_pair, intervals_intersect
from settings import settings

from game.env import env
from data.coords import breakUpLine
from data.params import StdPressureAlt, time_to_fly, distance_flown, Speed
from data.strip import assigned_heading_detail, parsed_route_detail


# ---------- Constants ----------

default_aircraft_speed = Speed(350)

# -------------------------------



class NoPath(Exception):
	pass


def shapes_intersect(sh1, sh2):
	for r1 in sh1:
		for r2 in sh2:
			r1a = r1[0].toRadarCoords()
			r1b = r1[1].toRadarCoords()
			r2a = r2[0].toRadarCoords()
			r2b = r2[1].toRadarCoords()
			if intervals_intersect(ordered_pair(r1a.x(), r1b.x()), ordered_pair(r2a.x(), r2b.x())) \
					and intervals_intersect(ordered_pair(r1a.y(), r1b.y()), ordered_pair(r2a.y(), r2b.y())):
				return True
	return False




def rectFrom(start, heading, distance):
	return start, start.moved(heading, distance)







def route_shape(pos, waypoints, distance_limit):
	if waypoints == [] or distance_limit != None and distance_limit <= 0:
		return []
	else:
		wp = waypoints[0].coordinates
		wp_dist = pos.distanceTo(wp)
		if env.pointOnMap(wp) and (distance_limit == None or distance_limit >= wp_dist):
			leg = (pos, wp)
		else: # waypoint is out of range
			wp_hdg = pos.headingTo(wp)
			# limit distance because it could be the end of the world
			leg = rectFrom(pos, wp_hdg, some(distance_limit, min(wp_dist, 1.5 * settings.map_range)))
		result = breakUpLine(*leg)
		if env.pointOnMap(leg[1]): # more legs in shape to add
			result += route_shape(wp, waypoints[1:], (None if distance_limit == None else distance_limit - wp_dist))
		return result





def horizontal_path(acft, limit=None):
	'''
	with a time-to-fly limit: returns lines following assigned heading or route until first point out of radar range or TTF elapsed
	without: ignores any assigned heading and returns full route until first point out of radar range regarless of distance
	'''
	pos = acft.coords()
	strip = env.linkedStrip(acft)
	if strip != None:
		assHdg = strip.lookup(assigned_heading_detail)
		speed = some(acft.groundSpeed(), default_aircraft_speed)
		if assHdg != None and limit != None:
			return breakUpLine(*rectFrom(pos, assHdg, distance_flown(limit, speed)))
		route = strip.lookup(parsed_route_detail)
		if route != None:
			waypoints = [route.waypoint(i) for i in range(route.currentLegIndex(pos), route.legCount())]
			return route_shape(pos, waypoints, (None if limit == None else distance_flown(limit, speed)))
	raise NoPath()



def vertical_assignment(acft):
	strip = env.linkedStrip(acft)
	if strip == None:
		return None
	else:
		alt = strip.assignedPressureAlt(env.QNH())
		return None if alt == None else alt.ft1013()





class Conflict:
	NO_CONFLICT, DEPENDS_ON_ALT, PATH_CONFLICT, NEAR_MISS = range(4) # CAUTION: Order is used for comparison



def conflict_test(acft1, acft2):
	pos1 = acft1.coords()
	pos2 = acft2.coords()
	alt1 = acft1.xpdrAlt()
	alt2 = acft2.xpdrAlt()
	if alt1 != None:
		if alt1.FL() < settings.conflict_warning_floor_FL:
			return Conflict.NO_CONFLICT
		alt1 = alt1.ft1013()
	if alt2 != None:
		if alt2.FL() < settings.conflict_warning_floor_FL:
			return Conflict.NO_CONFLICT
		alt2 = alt2.ft1013()
	## Checking for loss of separation first (most serious alert)
	if pos1.distanceTo(pos2) < settings.horizontal_separation:
		if alt1 != None and alt2 != None and abs(alt1 - alt2) < settings.vertical_separation:
			return Conflict.NEAR_MISS
	if not settings.route_conflict_warnings:
		return Conflict.NO_CONFLICT
	## Check for an anticipated route/hdg assignment conflict
	try:
		hs1 = horizontal_path(acft1, settings.conflict_warning_anticipation)
		hs2 = horizontal_path(acft2, settings.conflict_warning_anticipation)
		if not shapes_intersect(hs1, hs2):
			return Conflict.NO_CONFLICT
	except NoPath:
		return Conflict.NO_CONFLICT
	## At this point, we know: horizontal heading or route assigned and in conflict. Check for altitudes.
	ass1 = vertical_assignment(acft1)
	ass2 = vertical_assignment(acft2)
	if ass1 == None or ass2 == None:
		return Conflict.DEPENDS_ON_ALT
	else: # both ACFT have an assigned alt.
		vi1 = (ass1, ass1) if alt1 == None else ordered_pair(alt1, ass1)
		vi2 = (ass2, ass2) if alt2 == None else ordered_pair(alt2, ass2)
		return Conflict.PATH_CONFLICT if intervals_intersect(vi1, vi2) else Conflict.NO_CONFLICT

