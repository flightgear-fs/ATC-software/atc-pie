
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from util import some
from settings import settings
from game.env import env
from data.UTC import now
from data.db import known_aircraft_types
from data.params import Speed, StdPressureAlt
from data.conflict import Conflict


# ---------- Constants ----------

snapshot_history_size = 120 # number of radar snapshots
snapshot_diff_time = 2 # seconds (how long to look back in history for snapshot diffs)
min_taxiing_speed = Speed(5)
max_ground_height = 100 # ft

# -------------------------------




class RadarSnapshot:
	def __init__(self, time_stamp, coords, geom_alt):
		# Obligatory data
		self.time_stamp = time_stamp
		self.coords = coords
		self.geometric_alt = geom_alt
		# XPDR readings
		self.xpdrMode = '0'
		self.xpdrSqCode = None
		self.xpdrAlt = None
		self.xpdrCallsign = None
		self.xpdrAcftType = None
		self.xpdrIdent = None
		# Inferred values
		self.heading = None
		self.groundSpeed = None
		self.verticalSpeed = None









class Aircraft:
	'''
	This class represents a live aircraft, whether visible or invisible.
	'''
	
	def __init__(self, identifier, acft_type, init_position, init_geom_alt):
		self.identifier = identifier # assumed unique
		self.aircraft_type = acft_type
		# "REAL TIME" VALUES
		self.live_update_time = now()
		self.live_position = init_position, init_geom_alt # EarthCoords, geom AMSL
		self.live_XPDR_status = None, None, None, None # mode, code, alt, ident
		# UPDATED DATA
		init_snapshot = RadarSnapshot(self.live_update_time, init_position, init_geom_alt)
		self.radar_snapshots = [init_snapshot] # snapshot history; list must not be empty
		self.conflict = Conflict.NO_CONFLICT
		# USER OPTIONS
		self.ignored = False
		self.XPDR_cheat = False
	
	def __str__(self):
		return ('%s' if self.isRadarVisible() else '(%s)') % self.identifier
	
	def geomAlt(self, realTime=False):
		if realTime:
			return self.live_position[1]
		else:
			return self.lastSnapshot().geometric_alt
	
	
	## CONNECTION AND DATA UPDATE
	
	def updateLiveStatus(self, pos, galt, xm, xsq, xalt, xid):
		self.live_update_time = now()
		self.live_position = pos, galt
		self.live_XPDR_status = xm, xsq, xalt, xid
	
	def moveHistoryTimesForward(self, delay):
		self.live_update_time += delay
		for snap in self.radar_snapshots:
			snap.time_stamp += delay
	
	def lastLiveUpdateTime(self):
		return self.live_update_time
	
	
	## RADAR DETAIL ACCESS
	
	def xpdrEquipped(self):
		return self.live_XPDR_status[0] != None
	
	def coords(self, realTime=False):
		if realTime:
			return self.live_position[0]
		else:
			return self.lastSnapshot().coords
	
	def isRadarVisible(self):
		'''
		a radar can draw a spot (possibly helped by a cheat)
		'''
		return env.pointInRadarRange(self.coords(realTime=True)) \
			and (self.xpdrMode(realTime=True) != '0' or settings.primary_radar_active) \
			and (settings.radar_signal_floor_height == 0 or settings.radar_cheat and settings.ignore_floor_on_radar_cheat \
						or self.geomAlt(realTime=True) >= settings.radar_signal_floor_height + env.elevation(env.radarPos()))
	
	def considerOnGround(self):
		return self.xpdrMode() == 'G' or self.xpdrAlt() != None and self.geomAlt() - env.elevation(self.coords()) <= max_ground_height
	
	
	## TRANSPONDER READING
	
	def xpdrMode(self, realTime=False):
		'''
		Possible return values (single char): '0':off/standby, 'G':ground, 'A', 'C', 'S'
		'''
		if self.XPDR_cheat or settings.radar_cheat:
			result = 'S'
		elif realTime:
			result = self.live_XPDR_status[0]
		else:
			result = self.lastSnapshot().xpdrMode
		return some(result, settings.XPDR_mode_if_not_equipped)
	
	def xpdrSqCode(self, realTime=False):
		if self.xpdrMode(realTime) in '0G':
			return None
		else:
			return self.live_XPDR_status[1] if realTime else self.lastSnapshot().xpdrSqCode
	
	def xpdrAlt(self, realTime=False):
		if self.xpdrMode(realTime) not in 'CS':
			return None
		elif realTime:
			if self.live_XPDR_status[2] != None:
				return self.live_XPDR_status[2]
			elif self.live_position[1] != None: # no XPDR data; fallback on geometric alt position value
				return StdPressureAlt.fromAMSL(self.live_position[1], env.QNH())
			else:
				return None
		else:
			return self.lastSnapshot().xpdrAlt
	
	def xpdrIdent(self, realTime=False):
		if self.xpdrMode(realTime) == '0':
			return None
		else:
			return self.live_XPDR_status[3] if realTime else self.lastSnapshot().xpdrIdent
	
	def xpdrCallsign(self, realTime=False):
		if self.xpdrMode(realTime) == 'S':
			return self.identifier if realTime else self.lastSnapshot().xpdrCallsign
		else:
			return None
	
	def xpdrAcftType(self, realTime=False):
		if self.xpdrMode(realTime) == 'S':
			return self.aircraft_type if realTime else self.lastSnapshot().xpdrAcftType
		else:
			return None
	
	
	## RADAR SNAPSHOTS
	
	def lastSnapshot(self):
		return self.radar_snapshots[-1]
	
	def positionHistory(self, hist):
		'''
		returns the history of snapshot coordinates for the given delay since last live update, in chronological order.
		'''
		try:
			i_start = next(i for i, snap in enumerate(self.radar_snapshots) if self.live_update_time - snap.time_stamp <= hist)
			return [snap.coords for snap in self.radar_snapshots[i_start:]]
		except StopIteration: # if no live update in the time frame requested (can happen if app freezes for a while)
			return []
	
	def radarSnapshot(self):
		prev = self.lastSnapshot() # always exists
		if prev.time_stamp == self.live_update_time:
			return # No point saving values again: they were not updated since last snapshot
		else: # otherwise create a new snapshot
			snapshot = RadarSnapshot(self.live_update_time, self.coords(realTime=True), self.geomAlt(realTime=True))
		
		# Fill snapshot instant XPDR readings
		snapshot.xpdrMode = self.xpdrMode(realTime=True)
		snapshot.xpdrSqCode = self.xpdrSqCode(realTime=True)
		snapshot.xpdrAlt = self.xpdrAlt(realTime=True)
		snapshot.xpdrCallsign = self.xpdrCallsign(realTime=True)
		snapshot.xpdrAcftType = self.xpdrAcftType(realTime=True)
		snapshot.xpdrIdent = self.xpdrIdent(realTime=True)
		
		# Search history for best snapshot to use for diff
		diff_seconds = (snapshot.time_stamp - prev.time_stamp).total_seconds()
		i = 1 # index of currently selected prev
		while i < len(self.radar_snapshots) and diff_seconds < snapshot_diff_time:
			i += 1
			prev = self.radar_snapshots[-i]
			diff_seconds = (snapshot.time_stamp - prev.time_stamp).total_seconds()
		
		# Fill snapshot diffs
		if prev.coords != None and snapshot.coords != None:
			# ground speed
			snapshot.groundSpeed = Speed(prev.coords.distanceTo(snapshot.coords) * 3600 / diff_seconds)
			# heading
			if snapshot.groundSpeed != None and snapshot.groundSpeed.diff(min_taxiing_speed) > 0: # acft moving
				try: snapshot.heading = snapshot.coords.headingFrom(prev.coords)
				except ValueError: snapshot.heading = prev.heading # stopped: keep prev. hdg
			else:
				snapshot.heading = prev.heading
		# vertical speed
		if prev.xpdrAlt != None and snapshot.xpdrAlt != None:
			snapshot.verticalSpeed = (snapshot.xpdrAlt.diff(prev.xpdrAlt)) * 60 / diff_seconds
		# Append snapshot to history
		self.radar_snapshots.append(snapshot)
		if len(self.radar_snapshots) > snapshot_history_size:
			del self.radar_snapshots[0]
	
	def heading(self):
		return self.lastSnapshot().heading
	
	def groundSpeed(self):
		return self.lastSnapshot().groundSpeed
	
	def verticalSpeed(self):
		return self.lastSnapshot().verticalSpeed
	
	def estimatedIAS(self):
		'''
		Considers TAS = ground speed (no wind correction since wind is not known here).
		'''
		gs = self.lastSnapshot().groundSpeed
		if gs == None:
			return None
		else:
			alt = self.xpdrAlt()
			return None if alt == None else gs.tas2ias(alt)


