
#
# This file is part of the ATC-pie project,
# an air traffic control simulation program.
# 
# Copyright (C) 2015  Michael Filhol <mickybadia@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#

from data.coords import EarthCoords


# ---------- Constants ----------

# -------------------------------



class Navpoint:
	## STATIC:
	types = AD, VOR, NDB, FIX, ILS, RNAV = range(6)
	
	def tstr(t):
		return { \
			Navpoint.AD: 'AD', \
			Navpoint.VOR: 'VOR', \
			Navpoint.NDB: 'NDB', \
			Navpoint.FIX: 'Fix', \
			Navpoint.ILS: 'ILS', \
			Navpoint.RNAV: 'RNAV'
		}[t]
	
	def __init__(self, t, code, coords):
		'''
		code should be upper case
		'''
		self.type = t
		self.code = code
		self.coordinates = coords
		self.long_name = '' # to be overridden if applicable
	
	def __str__(self):
		return self.code
	




class Airfield(Navpoint):
	def __init__(self, icao, coords, alt, airport_name):
		Navpoint.__init__(self, Navpoint.AD, icao, coords)
		self.altitude = alt
		self.long_name = airport_name


class VOR(Navpoint):
	def __init__(self, identifier, coords, frq, long_name, dme=False, tacan=False):
		Navpoint.__init__(self, Navpoint.VOR, identifier, coords)
		self.long_name = long_name
		self.frequency = frq
		self.dme = dme
		self.tacan = tacan


class NDB(Navpoint):
	def __init__(self, identifier, coords, frq, long_name, dme=False):
		Navpoint.__init__(self, Navpoint.NDB, identifier, coords)
		self.long_name = long_name
		self.frequency = frq
		self.dme = dme


class Fix(Navpoint):
	def __init__(self, name, coords):
		Navpoint.__init__(self, Navpoint.FIX, name, coords)


class Rnav(Navpoint):
	def __init__(self, name, coords):
		Navpoint.__init__(self, Navpoint.RNAV, name, coords)














class NavpointError(Exception):
	pass








class NavDB:
	def __init__(self):
		self.by_type = { t:[] for t in Navpoint.types } # type -> navpoint list (KeyError safe)
		self.by_code = {} # code -> navpoint list (KeyError is possible)
	
	def add(self, p):
		self.by_type[p.type].append(p)
		try:
			self.by_code[p.code].append(p)
		except KeyError:
			self.by_code[p.code] = [p]
	
	def clear(self):
		for key in self.by_type:
			self.by_type[key] = []
		self.by_code.clear()
	
	def byType(self, t): # WARNING: do not alter result
		return self.by_type[t]
	
	def findAll(self, code=None, types=Navpoint.types): # WARNING: do not alter result
		if code == None:
			result = []
			for t in types:
				result += self.byType(t)
			return result
		else:
			key = code.upper()
			return [p for p in self.by_code.get(key, []) if p.type in types]
	
	def findUnique(self, code, types=Navpoint.types):
		'''
		raises NavpointError if zero or more than one navpoint is found with given code and whose type is in "types" list
		'''
		candidates = self.findAll(code, types)
		if len(candidates) != 1:
			raise NavpointError(str(code) if code != None else '')
		return candidates[0]
	
	def findClosest(self, ref, code=None, types=Navpoint.types):
		'''
		raises NavpointError if no navpoint is found with given code and type in "types" list
		'''
		candidates = self.findAll(code, types)
		if candidates == []:
			raise NavpointError(str(code) if code != None else '')
		return min(candidates, key=(lambda p: ref.distanceTo(p.coordinates)))
	
	def findAirfield(self, icao):
		'''
		raises ValueError if None is given as argument
		raises NavpointError if no airfield is found with given code
		'''
		if icao == None:
			raise ValueError('NavDB.findAirfield: no airport given')
		return self.findUnique(icao, types=[Navpoint.AD]) # can raise NavpointError
	
	def subDB(self, pred):
		result = NavDB()
		result.by_type = { t: [p for p in plst if pred(p)] for t, plst in self.by_type.items() }
		result.by_code = { c: [p for p in plst if pred(p)] for c, plst in self.by_code.items() if plst != [] }
		return result




world_nav_data = NavDB()



